name="M&T Common Sense Content Pack DLC Support"
path="mod/MEIOUandTaxes_cs_unit_DLC_support"
dependencies={
	"MEIOU and Taxes 2.02"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesCS.jpg"
supported_version="1.24.*.*"
