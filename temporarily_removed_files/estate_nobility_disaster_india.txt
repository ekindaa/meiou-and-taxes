#estate_nobility_disaster_india = {
#	potential = {
#		has_dlc = "The Cossacks"
#		has_estate = estate_nobles
#		estate_influence = {
#			estate = estate_nobles
#			influence = 60
#		}
#		capital_scope = {
#			province_group = india_charter
#		}
#	}
#
#
#	can_start = {
#		has_any_disaster = no
#		estate_influence = {
#			estate = estate_nobles
#			influence = 80
#		}
#	}
#	
#	can_stop = {
#		NOT = {
#			estate_influence = {
#				estate = estate_nobles
#				influence = 80
#			}		
#		}
#	}
#	
#	progress = {
#		modifier = {
#			factor = 1
#			estate_influence = {
#				estate = estate_nobles
#				influence = 80
#			}
#			hidden_trigger = {
#				NOT = {
#					estate_influence = {
#						estate = estate_nobles
#						influence = 85
#					}			
#				}			
#			}
#		}
#		modifier = {
#			factor = 2
#			estate_influence = {
#				estate = estate_nobles
#				influence = 85
#			}
#			hidden_trigger = {
#				NOT = {
#					estate_influence = {
#						estate = estate_nobles
#						influence = 90
#					}			
#				}			
#			}
#		}
#		modifier = {
#			factor = 3
#			estate_influence = {
#				estate = estate_nobles
#				influence = 90
#			}
#			hidden_trigger = {
#				NOT = {
#					estate_influence = {
#						estate = estate_nobles
#						influence = 95
#					}			
#				}			
#			}
#		}	
#		modifier = {
#			factor = 4
#			estate_influence = {
#				estate = estate_nobles
#				influence = 95
#			}
#			hidden_trigger = {
#				NOT = {
#					estate_influence = {
#						estate = estate_nobles
#						influence = 100
#					}			
#				}			
#			}
#		}	
#		modifier = {
#			factor = 5
#			estate_influence = {
#				estate = estate_nobles
#				influence = 100
#			}
#		}
#		modifier = {
#			factor = 1
#			overextension_percentage = 0.05
#			hidden_trigger = {
#				NOT = {
#					overextension_percentage = 0.1	
#				}			
#			}
#		}	
#		modifier = {
#			factor = 2
#			overextension_percentage = 0.1
#			hidden_trigger = {
#				NOT = {
#					overextension_percentage = 0.15	
#				}			
#			}
#		}		
#		modifier = {
#			factor = 3
#			overextension_percentage = 0.15
#			hidden_trigger = {
#				NOT = {
#					overextension_percentage = 0.2	
#				}			
#			}
#		}		
#		modifier = {
#			factor = 4
#			overextension_percentage = 0.2
#			hidden_trigger = {
#				NOT = {
#					overextension_percentage = 0.25	
#				}			
#			}
#		}		
#		modifier = {
#			factor = 5
#			overextension_percentage = 0.25
#			hidden_trigger = {
#				NOT = {
#					overextension_percentage = 0.3	
#				}			
#			}
#		}	
#		modifier = {
#			factor = 6
#			overextension_percentage = 0.3
#			hidden_trigger = {
#				NOT = {
#					overextension_percentage = 0.35	
#				}			
#			}
#		}	
#		modifier = {
#			factor = 7
#			overextension_percentage = 0.35
#			hidden_trigger = {
#				NOT = {
#					overextension_percentage = 0.4	
#				}			
#			}
#		}	
#		modifier = {
#			factor = 8
#			overextension_percentage = 0.4
#			hidden_trigger = {
#				NOT = {
#					overextension_percentage = 0.45
#				}			
#			}
#		}	
#		modifier = {
#			factor = 9
#			overextension_percentage = 0.45
#			hidden_trigger = {
#				NOT = {
#					overextension_percentage = 0.5
#				}			
#			}
#		}	
#		modifier = {
#			factor = 10
#			overextension_percentage = 0.5
#			hidden_trigger = {
#				NOT = {
#					overextension_percentage = 0.55
#				}			
#			}
#		}	
#		modifier = {
#			factor = 1
#			NOT = { legitimacy = 90 }
#			hidden_trigger = {
#				NOT = {
#					legitimacy = 80
#				}			
#			}
#		}	
#		modifier = {
#			factor = 2
#			NOT = { legitimacy = 80 }
#			hidden_trigger = {
#				NOT = {
#					legitimacy = 70 
#				}			
#			}
#		}
#		modifier = {
#			factor = 3
#			NOT = { legitimacy = 70 }
#			hidden_trigger = {
#				NOT = {
#					legitimacy = 60 
#				}			
#			}
#		}
#		modifier = {
#			factor = 4
#			NOT = { legitimacy = 60 }
#			hidden_trigger = {
#				NOT = {
#					legitimacy = 50 
#				}			
#			}
#		}
#		modifier = {
#			factor = 5
#			NOT = { legitimacy = 50 }
#		}
#		modifier = {
#			factor = 1
#			war_exhaustion = 1
#			hidden_trigger = {
#				NOT = {
#					war_exhaustion = 2
#				}			
#			}
#		}
#		modifier = {
#			factor = 2
#			war_exhaustion = 2
#			hidden_trigger = {
#				NOT = {
#					war_exhaustion = 3
#				}			
#			}
#		}
#		modifier = {
#			factor = 3
#			war_exhaustion = 3
#			hidden_trigger = {
#				NOT = {
#					war_exhaustion = 4
#				}			
#			}
#		}
#		modifier = {
#			factor = 4
#			war_exhaustion = 4
#			hidden_trigger = {
#				NOT = {
#					war_exhaustion = 5
#				}			
#			}
#		}
#		modifier = {
#			factor = 5
#			war_exhaustion = 5
#			hidden_trigger = {
#				NOT = {
#					war_exhaustion = 6
#				}			
#			}
#		}
#		modifier = {
#			factor = 6
#			war_exhaustion = 6
#			hidden_trigger = {
#				NOT = {
#					war_exhaustion = 7
#				}			
#			}
#		}
#		modifier = {
#			factor = 7
#			war_exhaustion = 7
#			hidden_trigger = {
#				NOT = {
#					war_exhaustion = 8
#				}			
#			}
#		}
#		modifier = {
#			factor = 8
#			war_exhaustion = 8
#			hidden_trigger = {
#				NOT = {
#					war_exhaustion = 9
#				}			
#			}
#		}
#		modifier = {
#			factor = 9
#			war_exhaustion = 9
#			hidden_trigger = {
#				NOT = {
#					war_exhaustion = 10
#				}			
#			}
#		}
#		modifier = {
#			factor = 10
#			war_exhaustion = 10
#		}
#		modifier = {
#			factor = 15
#			NOT = {
#				capital_scope = { controlled_by = ROOT }				
#			}
#		}
#	}
#	
#	can_end = {
#		always = yes
#	}
#	
#	modifier = {
#		global_tax_modifier = -0.25
#		global_trade_power = -0.25
#		trade_efficiency = -0.25
#		land_morale = -0.25
#		discipline = -0.05
#	}
#
#	on_start = indian_estate_disaster.1
#	on_end = indian_estate_disaster.2
#	
#	on_monthly = {
#		events = {
#	
#		}
#		random_events = { 
#			#500 = 0
#			#500 = indian_estate_disaster.3
#			#100 = indian_estate_disaster.4
#			#50 = indian_estate_disaster.5
#			1000 = 0
#			10 = centralisation.99
#		}
#	}
#}
