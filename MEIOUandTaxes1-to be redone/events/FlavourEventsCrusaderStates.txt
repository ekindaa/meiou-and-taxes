########################################
# Events to help the Crusader States devolve
# Based on the life of Nerio I Acciaioli, 
# the Tuscan Noble who took Athens from the 
# Catalans.
########################################
# Latin Lord Rises
country_event = {
	id = flavor_crusaders.001
	title = "flavor_crusaders.001.t"
	desc = "flavor_crusaders.001.d"
	picture = ADVISOR_eventPicture

	trigger = {
		has_country_flag = greek_crusader_state	
		NOT = { is_year = 1650 }
		is_lesser_in_union = yes
	}

	mean_time_to_happen = {
		months = 480
		modifier = {
			factor = 0.5
			overlord = {
				is_at_war = yes
			}	
		}
		modifier = {
			factor = 0.25
			NOT = { legitimacy = 80 }	
		}
	}

	option = {			# Damn!
		name = "flavor_crusaders.001.a"
		capital_scope = {
			pretender_rebels = 2
		}
		overlord = { country_event = { id = flavor_crusaders.002 days = 1 } }
		change_religion = catholic
		change_primary_culture = lombard
	}
}
country_event = {
	id = flavor_crusaders.002
	title = "flavor_crusaders.002.t"
	desc = "flavor_crusaders.002.d"
	picture = ADVISOR_eventPicture

	is_triggered_only = yes

	option = {			# Damn!
		name = "flavor_crusaders.002.a"
	}
	option = {			# Let them go!
		name = "flavor_crusaders.002.b"
		break_union = FROM
	}
}