bookmark =
{
	name = "SERENISSIMA_ITALIA_NAME"
	desc = "SERENISSIMA_ITALIA_DESC"
	date = 1444.11.11

	country = FIR
	country = VEN
	country = SIE
	country = FRK

	effect = {
		1 = { set_global_flag = f_game_start }
	}
}
