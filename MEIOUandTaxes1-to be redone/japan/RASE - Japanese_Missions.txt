# Japanese Missions

convert_ezochi_to_shintoism = {
	
	type = country

	category = ADM
	
	allow = {
		culture_group = japanese				# tag = JAP
		religion = shinto
		owns = 2300
		2300 = { NOT = { religion = shinto } }
		num_of_missionaries = 1
		NOT = { stability = 3 }
		NOT = { has_country_modifier = force_converted_province_mission }
	}
	abort = {
		OR = {
			NOT = { religion = shinto }
			NOT = { owns = 2300 }
			stability = 3
		}
	}
	success = {
		2300 = { religion = shinto }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			dip = 4
		}
	}
	effect = {
		add_stability_1 = yes
		add_country_modifier = {
			name = "force_converted_province_mission"
			duration = 3650
		}
	}
}


conquer_southern_korea = {

	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		area = south_korea_area
	}
	allow = {
		culture_group = japanese				# tag = JAP
		has_country_flag = united_japan
		mil = 4
		is_free_or_tributary_trigger = yes
		num_of_ports = 1
		NOT = { has_country_modifier = military_victory }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
		}
	}
	success = {
		all_target_province = {
			OR = { 
				owned_by = ROOT
				owner = { is_subject_of = ROOT }
			}
		}
	}
	chance = {
		factor = 1000
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		add_country_modifier = { name = decreased_morale duration = 1000 }
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		FROM = {
			add_army_tradition = 5
			add_country_modifier = {
				name = "military_victory"
				duration = 5000
			}
		}
		every_target_province = {
			if = {
				limit = { is_core = ROOT }
				add_local_autonomy = -10
				}
			if = {
			limit = { NOT = { is_core = ROOT } }
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
				}
			}
		}
	}
}


colonize_the_northern_islands = {
	
	type = country

	category = DIP
	
	allow = {
		culture_group = japanese				# tag = JAP
		2302 = { is_empty = yes base_tax = 1 }
		2744 = { is_empty = yes base_tax = 1 }
		num_of_colonists = 1
		num_of_ports = 1
		NOT = { has_country_modifier = colonial_enthusiasm }
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			AND = {
				NOT = { owns = 2302 }
				NOT = { owns = 2744 }
				2302 = { is_empty = no }
				2744 = { is_empty = no }
			}
		}
	}
	success = {
		owns = 2302
		owns = 2744
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 3650
		}
	}
}


monopolize_japanese_cot = {
	
	type = country

	category = DIP
	
	allow = {
		tag = JAP
		NOT = {
			2293 = {
				is_strongest_trade_power = JAP
			}
		}
		NOT = { last_mission = monopolize_japanese_cot }
	}
	abort = {
	}
	success = {
		2293 = {
			is_strongest_trade_power = JAP
		}	
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea = national_trade_policy
		}	
	}
	effect = {
		add_treasury = 40
		add_dip_power = 40
		set_country_flag = monopolize_japanese_cot
	}
}


establish_footholding_in_manchuria = {

	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		region = manchuria_region
	}

	allow = {
		culture_group = japanese
		has_country_flag = united_japan
		has_global_flag = westernized_japan
		is_free_or_tributary_trigger = yes
		NOT = { has_country_modifier = military_victory }
		NOT = { manchuria_region = { owned_by = ROOT } }
		
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		all_target_province = {
			OR = { 
				owned_by = ROOT
				owner = { is_subject_of = ROOT }
			}
		}
	}
	chance = {
		factor = 1000
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		add_country_modifier = { name = decreased_morale duration = 1000 }
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		FROM = {
			add_army_tradition = 5
			add_country_modifier = {
				name = "military_victory"
				duration = 5000
			}
		}
		every_target_province = {
			if = {
				limit = { is_core = ROOT }
				add_local_autonomy = -10
				}
			if = {
			limit = { NOT = { is_core = ROOT } }
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
				}
			}
		}
	}
}


conquer_okinawa = {

	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		area = ryukyu_area
	}
	
	allow = {
		culture_group = japanese				# tag = JAP
		has_country_flag = united_japan
		is_free_or_tributary_trigger = yes
		is_at_war = no
		num_of_ports = 1
		NOT = { has_country_modifier = military_victory }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { num_of_ports = 1 }
		}
	}
	success = {
		all_target_province = {
			OR = { 
				owned_by = ROOT
				owner = { is_subject_of = ROOT }
			}
		}
	}
	chance = {
		factor = 1000
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		add_country_modifier = { name = decreased_morale duration = 1000 }
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		FROM = {
			add_army_tradition = 5
			add_country_modifier = {
				name = "military_victory"
				duration = 5000
			}
		}
		every_target_province = {
			if = {
				limit = { is_core = ROOT }
				add_local_autonomy = -10
				}
			if = {
			limit = { NOT = { is_core = ROOT } }
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
				}
			}
		}
	}
}


jap_colonize_taiwan = {

	type = country
	
	category = DIP
	
	allow = {
		culture_group = japanese				# tag = JAP
		has_country_flag = united_japan
		has_global_flag = westernized_japan
		is_free_or_tributary_trigger = yes
		is_at_war = no
		num_of_cities = 8
		num_of_ports = 1
		num_of_colonists = 1
		NOT = { has_country_modifier = colonial_enthusiasm }
		NOT = { has_country_flag = colony_in_taiwan }
		2303 = {
			has_discovered = ROOT
			is_empty = yes
			base_tax = 1
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { num_of_ports = 1 }
		}
	}
	success = {
		owns = 2303
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			dip = 4
		}
	}
	effect = {
		add_prestige = 5
		set_country_flag = colony_in_taiwan
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 3650
		}
	}
}
