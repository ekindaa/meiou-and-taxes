# No previous file for Erieehronon

culture = erielhonan
religion = totemism
capital = "Erieehronon"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 35
native_ferocity = 8
native_hostileness = 6

1650.1.1  = { owner = HUR
		controller = HUR
		add_core = HUR
		trade_goods = wheat } #Hurons escape to Erie territory, ally with them
1656.1.1  = { 	owner = IRO
		controller = IRO
		add_core = IRO
		culture = iroquois } #Taken by Iroquois in Beaver Wars.
1679.1.1  = {  }
1707.5.12 = {  }
1753.8.3  = {	owner = FRA
		controller = FRA
		citysize = 330
		culture = francien
	    	religion = catholic
			trade_goods = fur
	    } # Construction of Fort Presque Isle
1760.1.1  = { controller = GBR } # Occupied by the British
1763.2.10 = {	owner = GBR
		culture = english
		religion = protestant
	    } # Treaty of Paris - ceded to Britain, France gave up its claim
1763.10.9 = {	owner = LEN
		controller = LEN
		add_core = LEN
		culture = lenape
		religion = totemism
	    } # Royal proclamation, Britan recognize native lands (as protectorates)
1795.8.3  = { owner = USA
		controller = USA
		culture = american
		religion = protestant
	    } # Treaty of Greenville, much of Ohio ceded by Natives.
1796.7.22 = { capital = "Cleveland" citysize = 150 }
1800.1.1  = { citysize = 4900 }
