# No previous file for Chippewaa

owner = FOX
controller = FOX
add_core = FOX
is_city = yes
culture = potawatomi
religion = totemism
capital = "Chippewa"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 45
native_ferocity = 4
native_hostileness = 8

1634.1.1  = {  } # Jean Nicolet
1650.1.1  = { 	owner = POT
		controller = POT
		add_core = POT
		culture = anishinabe } #Fleeing from the Beaver Wars
1707.5.12 = {  }
1716.1.1  = { add_core = FRA }
1763.2.10 = {
	owner = GBR
	controller = GBR
	citysize = 155
	culture = english
	religion = protestant
	remove_core = FRA
} # Treaty of Paris, ceded to the British
1763.10.9 = {	owner = POT
		controller = POT
		add_core = POT
		is_city = yes
		culture = huron
		religion = totemism
	    } # Royal proclamation, Britan recognize native lands (as protectorates)
1813.10.5 = {
	owner = USA
	controller = USA
	add_core = USA
	is_city = yes
	culture = american
	religion = protestant
} #The death of Tecumseh mark the end of organized native resistance 
