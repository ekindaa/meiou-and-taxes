# No previous file for Pah� S�pa

culture = cheyenne
religion = totemism
capital = "Paha Sapa"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 15
native_ferocity = 3
native_hostileness = 6

1760.1.1  = {	owner = CHY
		controller = CHY
		add_core = CHY
		trade_goods = fur
		culture = cheyenne
		is_city = yes } #Horses allow plain tribes to expand
