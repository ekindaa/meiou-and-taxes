# No previous file for Kitsay

culture = comanche
religion = totemism
capital = "Kitsay"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 15
native_ferocity = 2
native_hostileness = 5

1760.1.1  = {	owner = COM
		controller = COM
		add_core = COM
		is_city = yes } #Great Plain tribes spread over vast territories
