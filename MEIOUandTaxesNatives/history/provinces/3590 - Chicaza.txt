# No previous file for Chicaza

culture = chickasaw
religion = totemism
capital = "Chicaza"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 20
native_ferocity = 2
native_hostileness = 5

1540.1.1   = {  } # Hernando de Soto
1650.1.1   = {
	owner = CHI
	controller = CHI
	add_core = CHI
	trade_goods = cotton
	is_city = yes
} #Natives disruptiosn of the Beaver wars
1786.1.3= {
	owner = USA
	controller = USA
	culture = american
	trade_goods = cotton
	is_city = yes
	religion = protestant
} #Treaty of Hopewell (with the Choctaw), come under US authority
