# Conquest Missions

conquest_of_abenaki = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = ABE
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = ABE
		ABE = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = ABE }
	}
	success = {
		NOT = { exists = ABE }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_apache = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = APA
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = APA
		APA = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = APA }
	}
	success = {
		NOT = { exists = APA }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_assiniboine = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = ASI
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = ASI
		ASI = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = ASI }
	}
	success = {
		NOT = { exists = ASI }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_blackfoot = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = BLA
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = BLA
		BLA = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = BLA }
	}
	success = {
		NOT = { exists = BLA }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_caddo = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = CAD
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = CAD
		CAD = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = CAD }
	}
	success = {
		NOT = { exists = CAD }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_cherokee = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = CHE
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = CHE
		CHE = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = CHE }
	}
	success = {
		NOT = { exists = CHE }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_cheyenne = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = CHY
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = CHY
		CHY = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = CHY }
	}
	success = {
		NOT = { exists = CHY }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_comanche = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = COM
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = COM
		COM = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = COM }
	}
	success = {
		NOT = { exists = COM }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_creek = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = CRE
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = CRE
		CRE = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = CRE }
	}
	success = {
		NOT = { exists = CRE }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_huron = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = HUR
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = HUR
		HUR = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = HUR }
	}
	success = {
		NOT = { exists = HUR }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_illinewek = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = ILN
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = ILN
		ILN = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = ILN }
	}
	success = {
		NOT = { exists = ILN }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_iroquois = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = IRO
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = IRO
		IRO = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = IRO }
	}
	success = {
		NOT = { exists = IRO }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_lenape = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = LEN
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = LEN
		LEN = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = LEN }
	}
	success = {
		NOT = { exists = LEN }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_mahican = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = MAH
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = MAH
		MAH = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = MAH }
	}
	success = {
		NOT = { exists = MAH }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_mikmaq = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = MIK
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = MIK
		MIK = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = MIK }
	}
	success = {
		NOT = { exists = MIK }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_navajo = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = NJO
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = NJO
		NJO = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = NJO }
	}
	success = {
		NOT = { exists = NJO }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_ojibwa = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = OJI
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = OJI
		OJI = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = OJI }
	}
	success = {
		NOT = { exists = OJI }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_osage = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = OSA
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = OSA
		OSA = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = OSA }
	}
	success = {
		NOT = { exists = OSA }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_pawnee = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = PAW
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = PAW
		PAW = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = PAW }
	}
	success = {
		NOT = { exists = PAW }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_pequot = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = PEQ
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = PEQ
		PEQ = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = PEQ }
	}
	success = {
		NOT = { exists = PEQ }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_powhatan = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = POW
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = POW
		POW = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = POW }
	}
	success = {
		NOT = { exists = POW }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_pueblo = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = PUE
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = PUE
		PUE = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = PUE }
	}
	success = {
		NOT = { exists = PUE }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_shawnee = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = SHA
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = SHA
		SHA = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = SHA }
	}
	success = {
		NOT = { exists = SHA }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_shoshone = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = SHO
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = SHO
		SHO = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = SHO }
	}
	success = {
		NOT = { exists = SHO }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_sioux = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = SIO
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = SIO
		SIO = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = SIO }
	}
	success = {
		NOT = { exists = SIO }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

conquest_of_cree = {

	type = country

	category = MIL
	
	target_provinces = {
		owned_by = WCR
	}
	allow = {
		is_free_or_tributary_trigger = yes
		is_at_war = no
		is_neighbor_of = WCR
		WCR = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = WCR }
	}
	success = {
		NOT = { exists = WCR }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
	}
}

