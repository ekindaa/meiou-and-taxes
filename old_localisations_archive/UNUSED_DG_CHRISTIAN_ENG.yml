l_english:
 dg_catholic.011.t: "Sileni Alcibiadis"
 dg_catholic.011.d: "By $YEAR$ it was clear that the Catholic Church needed reform, and also clear that the pace of reform was too slow.  Noted theologian $THEOLOGIAN$ wrote a book in which he criticized the church by making allusions to Greek history to avoid accusations of heresy.  The message was that priests and popes, who were supposed to be pure, were not - yet nobody was able to criticize them.  Thanks to the invention of the printing press, it became far more popular than expected."
 dg_catholic.012.t: "The Praise Of Folly"
 dg_catholic.012.d: "In $YEAR$ noted author and humanist $THEOLOGIAN$ wrote The Praise Of Folly, a satire of the church and especially of the Papal Curia.  It was hugely popular, even with the pope, and considered to be hilarious - yet its message, that the church was in need of reform, was not lost on many."
 dg_catholic.051.t: "Church Reform"
 dg_catholic.051.d: "The Catholic church was ordained by God but made up of mortal men, expected to be perfect but sometimes falling short of heaven.  Pious Catholics understood this, but occasionally the church could go too far.  A particularly odious scandal or blatant corruption could lead to calls for reform in the church.  When even $MONARCHTITLE$s began demanding the pope call a council, it was time to re-evaluate where things stood."
 dg_catholic.051.a: "Ask the pope to call a church council"
 dg_catholic.051.b: "It is not our place to criticize the church"
 dg_catholic.056.i: "Debate the use of the Vernacular (Counter Reformation)"
 dg_catholic.056.j: "Debate the meaning of the Host (Counter Reformation)"
 dg_catholic.056.k: "Debate the importance of Scripture (Counter Reformation)"
 dg_catholic.056.l: "Debate the importance of Good Works (Counter Reformation)"
 dg_catholic.061.b: "Send lavish gifts to the pope"
 dg_catholic.112.a: "Go and spread the good news"
 dg_catholic.113.a: "We can only wonder what might have been"
 dg_orthodox.013.t: "Usurper claims the throne"
 dg_orthodox.013.d: "$MONARCH$'s claim to the throne has been challenged!  A usurper is raising his banner against the $MONARCHTITLE$, declaring that God shall decide the victor, proving his right to rule over $COUNTRY$.  \n\nThe Roman traditions of $COUNTRY$ meant that no single dynasty could claim a right to the throne.  Instead, there were dozens of precedents for a powerful noble or charismatic general to take the throne from a weak or untested $MONARCHTITLE$.  This was made even worse by the knowledge of this tradition - new $MONARCHTITLE$s saw charismatic leaders as rivals to power and often acted to eliminate them, forcing this kind of confrontation.  If it could not be dealt with quickly, it ran the risk of creating an opportunity for foreign powers to take advantage of the confusion.\n\nAlthough several $MONARCHTITLE$s attempted to reform the laws of inheritance, gaining the acceptance of such a change among all stakeholders was a challenging undertaking, one that demanded a systematic change (improve your form of government)."
 dg_orthodox.023.b: "Appoint a new patriarch"
 dg_orthodox.037.t: "[From.GetName] declares autocephaly"
 dg_orthodox.037.d: "Our Exarch in [From.GetName] has returned to us, having been forced to leave most suddenly when their church declared its independence from us.  They have publicly appointed a patriarch of their own and refuse to acknowledge the authority of ours.  This is a drastic step that represents a break between our two churches, although not a permanent one.  We may still be able to salvage something from this disaster."
 dg_orthodox.037.a: "Refuse to recognize their independence"
 dg_orthodox.037.b: "Recognize this fait accomplit"
 dg_orthodox.037.c: "The patriarch is fast losing his value to us"
 dg_orthodox.038.t: "[From.GetName] recognizes our independence"
 dg_orthodox.038.d: "Surprisingly, the Patriarch has agreed to recognize our church's autocephaly, placing our church back in communion with the rest of the $COUNTRY_RELIGION$ Church.  While we regret having had to stoop to such measures, it is clear now that we made a wise decision."
 dg_orthodox.038.a: "We knew they could be reasonable"
 dg_orthodox.039.t: "[From.GetName] severs their ties with our Patriarch"
 dg_orthodox.039.d: "The Patriarch was the leader of Orthodox and Miaphysite Christians.  However, living under the rule of a $COUNTRY_RELIGION$ $MONARCHTITLE$, he lost much of his authority outside $COUNTRY$.  The nation of [From.GetName] has decided to cut its ties to him and declare itself autocephalous.  This does not have any significant effects for us, but it is a significant setback for the captive Patriarch." 
 dg_protestant.001.t: "A New Translation"
 dg_protestant.001.d: "Prominent humanist scholars made many interesting discoveries while translating Greek and Roman texts into Latin.  Among their most notable achievements was a new translation of the Bible using the original Greek sources, then comparing it, page by page, with the official church translation (the Vulgate Bible).  Readers could see for themselves that the official version differed - and so contained errors.  This in turn meant that its translators were not divinely inspired, and that the church could make mistakes."
 dg_protestant.001.a: "This has profound implications"
 dg_protestant.002.t: "The Ninety-Five Theses"
 dg_protestant.002.d: "The corruption of the Catholic church sparked a number of reform movements.  Although several of these met with some success, none were anything more than regional rebellions that could be suppressed by the church.  The slow pace of reform frustrated reformers within the church, until a lone monk decided to post his many criticisms of the church publicly on the church door of his town.  His simple, easy to understand  $PRIMARY_CULTURE$, and the speed at which the printing press copied his words, meant that his ideas spread at an unprecedented rate.  What had begun as a theological argument had become a rallying cry for disaffected Catholics everywhere."
 dg_protestant.002.a: "What has he begun?"
 dg_protestant.003.d: "The corruption of the Catholic church sparked a number of reform movements.  Although several of these met with some success, none were anything more than regional rebellions that could be suppressed by the church.  The slow pace of reform frustrated reformers within the church, until a lone monk decided to post his many criticisms of the church publicly on the church door of his town.  His simple words written in the local language, and the speed at which the printing press copied his words, meant that his ideas spread at an unprecedented rate.  What had begun as a theological argument had become a rallying cry for disaffected Catholics everywhere." 
 dg_protestant.004.t: "Reaction to heresy"
 dg_protestant.004.d: "A new protest movement has begun to make waves in Christendom.  Like previous movements, this new one presents a rational series of much-needed changes to church practise and doctrine to modernize the church.  Those previous attempts, however, turned into heresy: John Wycliffe was forced to recant his beliefs, the Lollards were converted by force, and Jan Hus was burned at the stake!  Their crime was in going too far, too quickly, forcing the church into a confrontation that could have only one ending...and then rejecting the church's authority when they were condemned.  The church changes slowly because it is carrying centuries of tradition on its shoulders.  But is it too slow?  \n\nThis event gives you one last chance to decide whether $COUNTRY$ will remain Catholic or turn away from the church."
 dg_protestant.004.a: "Remain true to the church"
 dg_protestant.004.b: "His words have the ring of truth"
 dg_protestant.005.t: "Populist Reformation in $PROVINCENAME$"
 dg_protestant.005.d: "The ideas of the Protestant Reformation found fertile ground among the common people of many countries.  Many among the newly literate middle classes were eager to read the Bible themselves, and officially or underground, translations of the Bible were made available to them.  Discovering they could now debate theology and finding new grounds to criticize the church, they were eager recipients of anti-Catholic tracts and cartoons that spread quickly via the printing press.  While the Catholic church considered how to respond, thousands of ordinary commoners took up Bibles and proclaimed themselves missionaries of the Protestant faith."
 dg_protestant.005.a: "Excellent"
 dg_protestant.006.t: "Protestant heresy spreads to $PROVINCENAME$"
 dg_protestant.006.d: "The theology of the Reformation spread rapidly across Europe.  In some lands, Machiavellian princes converted their people to the new faith, willingly or not.  $MONARCHTITLE$ $MONARCH$ held firm, punishing reformers and banning their subversive literature from $COUNTRY$.  Despite these measures, Protestant missionaries continued to spread the new faith even at the risk of their lives." 
 dg_protestant.006.a: "Condemn these sinners and their lies"
 dg_protestant.006.b: "Burn them at the stake!"
 dg_protestant.007.a: "Root out this heresy"
 dg_protestant.007.c: "Build support for Reformation"
 dg_protestant.008.t: "Reformation Doctrine"
 dg_protestant.008.d: "The excesses of the Catholic church, highlighted by humanist thinkers and spread by the new printing press, created widespread dissatisfaction across Europe.  When one theologian spoke out against the church directly to the people a powerful movement arose to return the church to its apostolic beginnings...or create a new church in its place.  This new doctrine spread quickly through pamphlet and missionary, but it left conflict and bloodshed in its path as the existing church realized the threat and responded to it.  How should we get involved?"
 dg_protestant.023.t: "The Danger of Reformation"
 dg_protestant.023.d: "When princes and priests debated Reformation doctrines, peasants and burghers listened.  They heard that the church was corrupt and needed to be overthrown in order to restore Christ's kingdom on Earth - and they believed.  It was only a small step from there to suggest that other institutions were corrupt and needed to be overthrown.  Overnight a revolution sprang up as peasants demanded everything from communion in both forms to an end to serfdom."
 dg_protestant.023.a: "Crush the revolt"
 dg_protestant.023.b: "Compromise with the rebels"
 dg_protestant.024.t: "Reprisals"
 dg_protestant.024.d: "Although peasants inspired by the Reformation had revolted in great numbers, they were disorganized and lacked outside support.  After some early successes the movement was crushed and the feudal order restored.  The question then remained: what to do about the rebels?  Some princes reacted harshly by executing rebel leaders and imposing strict new rules on the peasants, while others - more lenient or perhaps more scared - decided not to make changes."
 dg_protestant.024.a: "Punish them harshly"
 dg_protestant.024.b: "Treat them leniently"
 dg_protestant.025.t: "Exsurge Domine"
 dg_protestant.025.d: "The new ideas of the Reformation were anathema to the church.  The pope reacted quickly by excommunicating the spiritual head of the Reformation, then asked nearby rulers to publicly post the papal bull (decree) excommunicating him so that good Catholics would be alert against this threat.  Much to his displeasure, some princes refused to do so, giving the Reformation a foothold in their lands."
 dg_protestant.025.a: "Post the bill in public places"
 dg_protestant.025.b: "Refuse to comply with the pope"
 dg_protestant.027.t: "Arrest of the Heretic"
 dg_protestant.027.d: "Having lost his protection in $PROVINCENAME$, the reformer and his followers were soon taken prisoner.  This gives us an opportunity to send him to [the_vatican.GetCapitalName] for trial, where he will no doubt face the same fate that Jan Hus did - the just reward of those who would cause a schism.  We could also skip the trial and execute him ourselves, to be sure his heretic tongue will stop wagging."
 dg_protestant.027.a: "Send him to [the_vatican.GetCapitalName] for trial"
 dg_protestant.027.b: "Execute him"
 dg_protestant.027.c: "Quietly let him escape"
 dg_protestant.028.t: "Reformer on Trial"
 dg_protestant.028.d: "The monk whose works have caused such a fuss has been arrested and brought to [the_vatican.GetCapitalName] for trial.  Like others before him, he sought to reform the church, and like others before him, when his ideas spiraled out of control he remained unrepentant and unwilling to repent.  What shall we do with him?"
 dg_protestant.028.a: "Sentence him to death"
 dg_protestant.028.b: "Sentence him to imprisonment"
 dg_protestant.028.c: "Find him innocent of heresy"
 dg_reformed.004.a: "The new doctrine is spreading"
 dg_papacy.003.t: "The Captivity"
 dg_papacy.003.d: "Rome was important to Catholics far out of proportion to its population.  The pope's official title was Bishop of Rome, and as such Rome was the center of the church.  When it was seized from the papacy by force, Catholic rulers saw it as an attempt to control the church itself, not just its land.  At the same time, the pope yearned to return to his bishopric and charged faithful Catholics with the task of restoring him to his throne." 
 dg_papacy.003.a: "Promise to regain Rome for the pope"
 dg_papacy.003.b: "Now is not the time"
 dg_papacy.003.c: "The age of crusades is long over"
 dg_papacy.004.t: "Successful Crusade"
 dg_papacy.004.d: "In $YEAR$, $MONARCH$'s crusade to free the Holy See from unbelievers came to fruition.  Rome, the Eternal City, was now in $COUNTRY$'s hands, and the pope asked $MONARCH$ to remember $RULER_ADJ$ oath and return it to his rightful rule.  The riches of the city could tempt a lesser man to sin, but $MONARCH$ had sworn a holy oath to return the pope to power in Rome."
 dg_papacy.004.a: "Restore the papal state to the pope"
 dg_papacy.004.b: "Break our vow and keep Rome for $COUNTRY$"
 dg_papacy.005.t: "Crusade for Rome declared"
 dg_papacy.005.d: "Not only was there theological reasons for the pope to rule Rome, Catholic rulers often had a vested interest in restoring him to power in Rome, far from the courts of rivals.  A Catholic ruler has made a vow to take the province from us and return it to the pope.  Are we ready for a war?"
 dg_papacy.005.a: "It is mine!"
 dg_papacy.005.b: "Back down and return Rome to the pope"
 dg_papacy.021.t: "The corruption of pope $MONARCH$"
 dg_papacy.021.d: "During the middle ages the papacy evolved from an advisory position into a powerful, centralized authority which competed with local rulers for control over the church.  Although there was a great deal of respect for the pope as the head of the Catholic church, his position was not completely sacrosanct - especially when he acted as a rival for power.  As the papal states grew from a small fief into a regional power, secular princes began to wonder whether or not the pope had lost his way, and Catholics began talking about apostolic poverty, church councils, and other limitations on the papacy.  It may be time to back down from our aggressive ways."
 dg_papacy.021.a: "Submit to the emperor's judgement"
 dg_papacy.021.b: "Preach obedience to the Vatican"
 dg_papacy.042.a: "Shower them with riches"
 dg_papacy.042.b: "Show them some favour"
 dg_papacy.042.c: "The church is our family now"
 dg_papacy.043.t: "The Papal Court"
 dg_papacy.043.d: "Throughout the middle ages there was fierce debate over the supremacy or church or state.  The pope, as the representative of God on Earth, had to impress upon foreign kings and dignitaries his position above them - no small feat when it came to rulers of much wealthier states.  Unfortunately, some popes saw luxury and wealth as their due and began to behave like royalty, only worse: popes could not pass their lands to their sons, and so had no reason to save their wealth instead of spending it." 
 dg_papacy.043.a: "The pope must be exalted above kings"
 dg_papacy.043.b: "The pope must maintain a proper court"
 dg_papacy.043.c: "The pope is but a humble shepherd"
 dg_papacy.047.d: "Although the pope claimed to be superior to secular rulers, in reality it was difficult and costly to keep up with the great courts of Europe.  At times the papacy was driven into bankruptcy.  Several popes who began their reign with dreams of introducing reforms or rooting out heresy found themselves mired in tax codes and financial records.  In an attempt to rebuild state finances, some popes came up with creative ideas.  One possibility was to hold a state lottery in which ordinary subjects could buy a chance to win riches.  This idea had been tried in other countries with great success, but it smacked of gambling and hence sin."
 dg_papacy.048.t: "St. Peter's Basilica"
 dg_papacy.048.d: "Built over a period of 120 years, Saint Peter's Basilica in $CAPITAL_CITY$ stands as one of the most beautiful churches in history.  The basilica was built to honor Saint Peter, one of the twelve apostles of Jesus Christ and, according to Catholic tradition, the first pope.  Replacing an earlier church, the basilica is one of the largest churches in the city.  Filled with the interior designs of several Renaissance artists, it attracts thousands of pilgrims eager to see its wonders."  
 dg_papacy.048.a: "It stands in unearthly glory" 
 dg_papacy.067.t: "Vatican power struggle"
 dg_papacy.067.d: "As the head of the Catholic church worldwide, the pope held a unique position - one that was coveted by many Christian rulers.  As the great powers of Europe intervened in the politics of the papacy, more than one pope was elected only through the support of a foreign power.  Some of these popes were strong, independent leaders, while others seemed to be little more than puppets.  Occasionally, though, even a puppet could be dangerous.  When a falling-out between a pope and his patron happened, it could lead not only to war, but to attempts to reform the papal curia in order to curtail the influence of a papal controller.  During times like this, all countries supporting a papabile would feel the effects as the Vatican began passing laws aimed at restoring the independence of future popes."
 dg_papacy.067.a: "This is an outrage!"
 dg_papacy.067.b: "Perhaps we can compromise"
 dg_papacy.067.c: "Support his holiness in this struggle"
 dg_papacy.067.e: "Stay on the sidelines"
 dg_pope.008.b: "We cannot claim to be surprised"
 dg_pope.019.t: "The pope appoints new cardinals"
 dg_pope.019.d: "The pope alone has the power to appoint new cardinals, and by tradition most popes did so early in their papacies, to replace cardinals who had died, to acknowledge particularly devout theologians, or to reward supporters and family members.  Popes would call a consistory and announce the names of the new cardinals, who would receive the cap and ring of their new rank in a public ceremony.  Although canon law set the number of cardinals to 20, this limit was ignored by popes who saw the need to appoint like-minded bishops, and was later raised to 70.  The new cardinals swelling the ranks of the college meant that each cardinal was relatively less important - at least until their number dwindled once more.\n\nWe have lost some influence with the curia."
 dg_pope.019.a: "The game begins anew"
 dg_leagues.023.t: "Freedom of the Bishops"
 dg_setup.004.d: "$MONARCHTITLE$ $MONARCH$ was summoned to the Vatican recently.  There the pope discussed church history with him, glad to be able to share his love of history with a fellow Catholic."
 dg_setup.004.a: "I do love history!"
 convert_to_catholic_title: "Accept the Pope's authority"
 convert_to_hussite_title: "Adopt the Articles of Prague"
 convert_to_hussite_desc: "The Hussite religion met in Prague to hammer out a unified confession of faith. These 'four articles of Prague' became the principal document outlining the Christian faith of the Hussites, but it also clearly spelled out their differences with the Catholic faith.  Any ruler that accepted the articles without exception would be knowingly breaking from [the_vatican.GetCapitalName]."
 declare_autocephaly_title: "Declare our theological independence"
 declare_autocephaly_desc: "Our state church is an arm of the patriarchate, and as a result our religious leaders must be confirmed by the patriarch rather than by our ruler.  This state of affairs is untenable for a state destined for greatness; we are powerful enough that we could simply declare our state church to be autocephalous, or independent of the patriarchate, and appoint a new patriarch ourselves.  The Patriarch will not appreciate this, but it will bring prestige to our nation."
 carmelite_order: "Carmelite Order"
 desc_carmelite_order: "This province is home to an important chapter of the Carmelite order.  \n\nBeginning in the early thirteenth century, Carmelite monks and nuns sought perfection through work and contemplation in splendid isolation. Their monasteries and abbeys often produced fine ales, cheeses and other goods which the monks traded for goods they could not produce.  Many women were attracted to the 'White Friars' and became sister nuns in the Order along with many laity."
 catholic_theocracy: "Theocracy"
 desc_catholic_theocracy: "As a Catholic theocracy, we have some influence over the curia."
 miaphysite_patriarchate: "Ecumenical Catholicate"
 desc_miaphysite_patriarchate: "A Miaphysite Patriarch was the head of his religion within the borders of his nation, for vassals, and sometimes for smaller Miaphysite countries that lacked autocephaly.  Without a patriarch, a country would be forced to seek the approval of a foreign leader for its bishops and metropolitans."
 anchorite_order: "Hermits"
 EVTOPTA55863: "Speak out against this heresy"
 EVTOPTB55863: "There is merit in their demands"
 protestant_spread: "Countries that share our culture are more likely to embrace Protestantism, even if they previously chose to support the Renaissance."
 patriarch_weak: "Either we must be larger than our patriarch's country, or there must not be an independent Orthodox patriarch for us (Constantinople for Europe, Alexandria for Africa, Antioch for Asia)."
 cardinals_disempowered: "This will reduce the influence of other nations with cardinals slightly"
 cardinals_greatly_disempowered: "This will reduce the influence of other nations with cardinals and make them resent us slightly"
 st_peter_basilica_explained: "Such a grand basilica will take generations to build"
 emperor_wins_league_war: "\nNevertheless, the Emperor has won a victory for the Catholic religion.  It is now guaranteed that the imperial throne will remain in Catholic hands...for now."
 league_wins_league_war: "\nThe upstart Schmalkaldic League has achieved its purpose and defeated the emperor.  It is now a certainty that the imperial throne will pass to the Protestants...for now."
 league_war_stalemate: "\nBoth sides are exhausted, with neither able to claim a decisive victory over the other...for now."
 preferred_aspect_guided_interpretation: "\nOur theologians are in favor of declaring the Church to be the official interpreter of Scripture."
 preferred_aspect_free_membership: "\nOur theologians are in favor of allowing people to voluntarily leave the Church without consequences."
 