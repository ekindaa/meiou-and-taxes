name="--MEIOU and Taxes Alternate Flags Module"
path="mod/MEIOUandTaxesFlags"
dependencies={
	"MEIOU and Taxes 1.25"
}
tags={
	"MEIOU and Taxes"
	"Flags"
}
supported_version="1.17.*.*"
