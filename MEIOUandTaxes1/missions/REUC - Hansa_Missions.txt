# Hansa Missions

end_sound_toll = {
	
	type = country
	
	category = MIL
	
	allow = {
		tag = FRL
		government = merchant_republic
		is_free_or_tributary_trigger = yes
		#NOT = {
		#	45 = { has_province_modifier = free_shipping_through_the_sound }
		#}
		12 = {
			has_province_modifier = sound_toll
			NOT = { owned_by = FRL }
		}
	}
	abort = {
		OR = {
			NOT = { government = merchant_republic }
			is_subject_other_than_tributary_trigger = yes
		}
	}
	success = {
		OR = {
			controls = 6			# Skane
			controls = 12			# Sjaelland
		}
	}
	chance = {
		factor = 1000
	}
	effect = {
		#45 = {							# Lubeck
		#	add_province_modifier = {
		#		name = "free_shipping_through_the_sound"
		#		duration = -1
		#	}
		#}
		add_country_modifier = {
			name = "merchant_society"
			duration = 3650
		}
	}
}
