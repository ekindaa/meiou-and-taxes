# Danish Missions

control_osel = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = DEN
		is_free_or_tributary_trigger = yes
		NOT = { owns = 1354 } # Saaremaa
		35 = { NOT = { owner = { alliance_with = DEN } } }
		NOT = { has_country_modifier = baltic_ambition }
		NOT = { has_country_flag = controlled_osel }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 1354 # Saaremaa
	}
	chance = {
		factor = 1500
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		add_claim = 1354 # Saaremaa
	}
	abort_effect = {
		remove_claim = 1354 # Saaremaa
	}
	effect = {
		add_prestige = 5
		add_navy_tradition = 5
		set_country_flag = controlled_osel
		add_country_modifier = {
			name = "baltic_ambition"
			duration = 1500
		}
		1354 = {
			add_territorial_core_effect = yes
		}
	}
}

colonize_iceland = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = DEN
		370 = { is_empty = yes base_tax = 1 }
		371 = { is_empty = yes base_tax = 1 }
		num_of_colonists = 1
		num_of_ports = 1
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			AND = {
				370 = { is_empty = no }
				NOT = { owns = 370 } # Skalholt
			}
			AND = {
				371 = { is_empty = no }
				NOT = { owns = 371 } # Holar
			}
		}
	}
	success = {
		owns = 370 # Skalholt
		owns = 371 # Holar
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 4
		}
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 3650
		}
	}
}

retake_scania = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = DEN
		is_free_or_tributary_trigger = yes
		NOT = { has_country_modifier = baltic_ambition }
		mil = 4
		SWE = { owns = 6 } # Skane
	}
	abort = {
		OR = {
			is_free_or_tributary_trigger = yes
			AND = {
				SWE = { NOT = { owns = 6 } } # Skane
				NOT = { owns = 6 } # Skane
			}
		}
	}
	success = {
		owns = 6 # Skane
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			is_monarch_leader = yes
		}
	}
	immediate = {
		add_claim = 6 # Skane
	}
	abort_effect = {
		remove_claim = 6 # Skane
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "baltic_ambition"
			duration = 1500
		}
		6 = {
			add_territorial_core_effect = yes
		}
	}
}

retake_halland = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = DEN
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { has_country_modifier = military_victory }
		SWE = { owns = 26 } # Halland
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			AND = {
				SWE = { NOT = { owns = 26 } } # Halland
				NOT = { owns = 26 } # Halland
			}
		}
	}
	success = {
		owns = 26 # Halland
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			is_monarch_leader = yes
		}
	}
	immediate = {
		add_claim = 26 # Halland
	}
	abort_effect = {
		remove_claim = 26 # Halland
	}
	effect = {
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
		26 = {
			add_territorial_core_effect = yes
		}
	}
}

denmark_slesvig_holstein_relations = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = DEN
		exists = SHL
		is_free_or_tributary_trigger = yes
		government = monarchy
		NOT = { has_country_modifier = foreign_contacts }
		NOT = { has_opinion = { who = SHL value = 50 } }
		NOT = { war_with = SHL }
		NOT = { marriage_with = SHL }
		SHL = {
			is_free_or_tributary_trigger = yes
			government = monarchy
		}
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { government = monarchy }
			war_with = SHL
			SHL = {
				OR = {
					is_subject_other_than_tributary_trigger = yes
					NOT = { government = monarchy }
				}
			}
		}
	}
	success = {
		SHL = { has_opinion = { who = DEN value = 100 } }
		marriage_with = SHL
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = SHL value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = SHL value = -100 } }
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "foreign_contacts"
			duration = 3650
		}
	}
}

denmark_oldenburg_relations = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = DEN
		exists = OLD
		is_free_or_tributary_trigger = yes
		government = monarchy
		NOT = { has_country_modifier = foreign_contacts }
		NOT = { has_opinion = { who = OLD value = 50 } }
		NOT = { war_with = OLD }
		NOT = { marriage_with = OLD }
		OLD = {
			is_free_or_tributary_trigger = yes
			government = monarchy
		}
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { government = monarchy }
			war_with = OLD
			OLD = {
				OR = {
					is_subject_other_than_tributary_trigger = yes
					NOT = { government = monarchy }
				}
			}
		}
	}
	success = {
		OLD = { has_opinion = { who = DEN value = 100 } }
		marriage_with = OLD
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = OLD value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = OLD value = -100 } }
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "foreign_contacts"
			duration = 3650
		}
	}
}

vassalize_norway = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = DEN
		exists = NOR
		is_free_or_tributary_trigger = yes
		NOT = { has_country_modifier = military_vassalization }
		NOT = { war_with = NOR }
		NOR = {
			is_free_or_tributary_trigger = yes
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = NOR }
			NOR = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		NOR = { vassal_of = DEN }
	}
	chance = {
		factor = 1500
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = NOR value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = NOR
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = NOR
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "military_vassalization"
			duration = 3650
		}
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = NOR
			}
		}
	}
}

annex_norway = {
	
	type = country
	
	category = DIP
	
	target_provinces = {
		owned_by = NOR
	}
	allow = {
		tag = DEN
		exists = NOR
		is_free_or_tributary_trigger = yes
		dip = 4
		NOR = {
			vassal_of = DEN
			religion_group = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = NOR }
			NOR = { NOT = { religion_group = ROOT } }
		}
	}
	success = {
		NOT = { exists = NOR }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1500
		modifier = {
			factor = 2
			has_opinion = { who = NOR value = 100 }
		}
		modifier = {
			factor = 2
			has_opinion = { who = NOR value = 200 }
		}
	}
	effect = {
		add_prestige = 20
		add_adm_power = 50
		add_dip_power = 50
	}
}

defend_denmark = {
	
	type = country
	
	category = MIL
	
	allow = {
		tag = DEN
		is_free_or_tributary_trigger = yes
		NOT = { has_country_flag = defended_denmark_flag }
		danish_region = { owned_by = SWE }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		NOT = { war_with = SWE }
		NOT = { danish_region = { owned_by = SWE } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	effect = {
		add_prestige = 10
		add_war_exhaustion = -5
		set_country_flag = defended_denmark_flag
		add_ruler_modifier = {
			name = defended_denmark
		}
	}
}

danish_controlled_cot = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces_list = {
		45
		2355
		34
		38
	}
	
	target_provinces = {
		NOT = { owned_by = ROOT }
	}

	allow = {
		tag = DEN
		is_free_or_tributary_trigger = yes
		NOT = { has_country_modifier = baltic_ambition }
		OR = {
			45 = {
				owned_by = DEN
				NOT = { has_province_flag = danish_controlled_mission }
				NOT = { owner = { alliance_with = DEN } }
			}
			2355 = {
				owned_by = DEN
				NOT = { has_province_flag = danish_controlled_mission }
				NOT = { owner = { alliance_with = DEN } }
			}
			34 = {
				owned_by = DEN
				NOT = { has_province_flag = danish_controlled_mission }
				NOT = { owner = { alliance_with = DEN } }
			}
			38 = {
				owned_by = DEN
				NOT = { has_province_flag = danish_controlled_mission }
				NOT = { owner = { alliance_with = DEN } }
			}
		}
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		OR = {
			owns = 45 # Lubeck
			owns = 2355 # Danzig
			owns = 34 # Ingria
			owns = 38 # Riga
		}
	}
	chance = {
		factor = 1500
		modifier = {
			factor = 2
			has_idea = national_trade_policy
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		if = {
			limit = { owns = 45 } # Lubeck
			45 = { set_province_flag = danish_controlled_mission }
		}
		if = {
			limit = { owns = 2355 } # Danzig
			2355 = { set_province_flag = danish_controlled_mission }
		}
		if = {
			limit = { owns = 34 } # Ingria
			34 = { set_province_flag = danish_controlled_mission }
		}
		if = {
			limit = { owns = 38 } # Riga
			38 = { set_province_flag = danish_controlled_mission }
		}
		add_country_modifier = {
			name = "baltic_ambition"
			duration = 1000
		}
	}
}

denmark_control_gotland = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = DEN
		is_free_or_tributary_trigger = yes
		NOT = { owns = 25 } # Gotland
		NOT = { has_country_modifier = baltic_ambition }
		25 = {
			owner = {
				NOT = { alliance_with = ROOT }
				NOT = { war_with = ROOT }
			}
		}
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 25 # Gotland
	}
	chance = {
		factor = 1500
		modifier = {
			factor = 5
			mil = 4
		}
		modifier = {
			factor = 10
			GOT = { owns = 25 } # Gotland
		}
	}
	immediate = {
		add_claim = 25 # Gotland
	}
	abort_effect = {
		remove_claim = 25 # Gotland
	}
	effect = {
		add_prestige = 5
		add_country_modifier = {
			name = "baltic_ambition"
			duration = 1500
		}
		25 = {
			add_territorial_core_effect = yes
		}
	}
}
