# Austrian Missions

unite_austria = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces = {
		OR = {
			owned_by = STY
			owned_by = TIR
		}
	}
	allow = {
		tag = HAB
		is_free_or_tributary_trigger = yes
		NOT = { has_country_flag = austria_reunited_flag }
		OR = {
			exists = STY
			exists = TIR
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			AND = {
				NOT = { exists = STY }
				NOT = { exists = TIR }
			}
		}
	}
	success = {
		NOT = { exists = STY }
		NOT = { exists = TIR }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			TIR = { has_opinion = { who = HAB value = 100 } }
		}
		modifier = {
			factor = 2
			STY = { has_opinion = { who = HAB value = 100 } }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		set_country_flag = austria_reunited_flag
		add_country_modifier = {
			name = "austria_reunited"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}


recover_silesia = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = HAB
		is_free_or_tributary_trigger = yes
		NOT = { owns = 1359 } # Wroclaw
		1359 = { is_core = ROOT } # Wroclaw
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 1359 # Wroclaw
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	effect = {
		add_prestige = 10
		1359 = {
			add_province_modifier = {
				name = "faster_integration"
				duration = 3650
			}
		}
	}
}


reclaim_the_empire = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = HAB
		is_emperor = no
		is_female = no
		has_regency = no
		num_of_cities = 6
		is_lesser_in_union = no
		NOT = { government = republic }
		OR = {
			NOT = { has_country_flag = empire_reclaimed }
			had_country_flag = { flag = empire_reclaimed days = 7300 }
		}
		capital_scope = {
			is_part_of_hre = yes
		}
	}
	abort = {
		OR = {
			has_regency = yes
			is_female = yes
			government = republic
			is_lesser_in_union = yes
			capital_scope = {
				is_part_of_hre = no
			}
		}
	}
	success = {
		is_emperor = yes
	}
	chance = {
		factor = 1500
	}
	effect = {
		add_prestige = 25
		add_country_modifier = {
			name = "succesful_bid_for_imperial_crown"
			duration = 3650
		}
	}
}


subjugate_bohemia = {
	
	type = country
	ai_mission = yes
	
	category = MIL
	
	allow = {
		tag = HAB
		exists = BOH
		is_year = 1500
		is_free_or_tributary_trigger = yes
		BOH = {
			is_free_or_tributary_trigger = yes
			is_emperor = no
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = BOH }
			BOH = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		senior_union_with = BOH
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = BOH value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_restore_personal_union
			months = 300
			target = BOH
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_restore_personal_union
			target = BOH
		}
	}
	effect = {
		add_prestige = 10
		add_adm_power = 50
		add_dip_power = 50
		hidden_effect = {
			remove_casus_belli = {
				type = cb_restore_personal_union
				target = BOH
			}
		}
	}
}


subjugate_hungary = {
	
	type = country
	ai_mission = yes
	
	category = MIL
	
	allow = {
		tag = HAB
		exists = HUN
		is_year = 1500
		is_free_or_tributary_trigger = yes
		HUN = {
			is_free_or_tributary_trigger = yes
			is_emperor = no
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = HUN }
			HUN = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		senior_union_with = HUN
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = HUN value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_restore_personal_union
			months = 300
			target = HUN
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_restore_personal_union
			target = HUN
		}
	}
	effect = {
		add_prestige = 10
		add_adm_power = 50
		add_dip_power = 50
		hidden_effect = {
			remove_casus_belli = {
				type = cb_restore_personal_union
				target = HUN
			}
		}
	}
}


subjugate_burgundy = {
	
	type = country
	ai_mission = yes
	
	category = MIL
	
	allow = {
		tag = HAB
		exists = BUR
		is_free_or_tributary_trigger = yes
		BUR = {
			is_free_or_tributary_trigger = yes
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = BUR }
			BUR = { is_subject_other_than_tributary_trigger = yes }
		}
	}
	success = {
		senior_union_with = BUR
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = BUR value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_restore_personal_union
			months = 180
			target = BUR
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_restore_personal_union
			target = BUR
		}
	}
	effect = {
		add_prestige = 10
		add_adm_power = 50
		add_dip_power = 50
		hidden_effect = {
			remove_casus_belli = {
				type = cb_restore_personal_union
				target = BUR
			}
		}
	}
}


no_ottomans_in_eastern_balkans = {
	
	type = country
	
	category = MIL
	
	allow = {
		tag = HAB
		exists = TUR
		is_free_or_tributary_trigger = yes
		TUR = {
			num_of_cities = 15
			is_neighbor_of = ROOT
			NOT = { alliance_with = HAB }
		}
		east_balkan_region = { owned_by = TUR }
		NOT = { has_country_modifier = drove_back_the_turk }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { east_balkan_region = { owned_by = TUR } }
		}
	}
	success = {
		NOT = { east_balkan_region = { owned_by = TUR } }
		east_balkan_region = { owned_by = ROOT }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = TUR value = 0 } }
		}
		modifier = {
			factor = 2
			TUR = { NOT = { num_of_cities = ROOT } }
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "drove_back_the_turk"
			duration = 3650
		}
	}
}


no_ottomans_in_western_balkans = {
	
	type = country
	
	category = MIL
	
	allow = {
		tag = HAB
		exists = TUR
		is_free_or_tributary_trigger = yes
		TUR = {
			num_of_cities = 15
			is_neighbor_of = ROOT
			NOT = { alliance_with = HAB }
		}
		west_balkan_region = { owned_by = TUR }
		NOT = { has_country_modifier = drove_back_the_turk }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { west_balkan_region = { owned_by = TUR } }
		}
	}
	success = {
		NOT = { west_balkan_region = { owned_by = TUR } }
		west_balkan_region = { owned_by = HAB }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = TUR value = 0 } }
		}
		modifier = {
			factor = 2
			TUR = { NOT = { num_of_cities = ROOT } }
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "drove_back_the_turk"
			duration = 3650
		}
	}
}

partition_poland = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = HAB
		is_year = 1700
		exists = POL
		exists = PRU
		exists = RUS
		NOT = { has_country_modifier = polish_partitions }
		NOT = { has_country_flag = partitioned_poland }
		is_neighbor_of = POL
		POL = {
			owns = 259 # Lubelskie
			owns = 262 # Krakowskie
			owns = 1279 # Sandomierskie
			is_neighbor_of = PRU
			is_neighbor_of = RUS
		}
		OR = {
			is_core = 259 # Lubelskie
			is_core = 262 # Krakowskie
			is_core = 1279 # Sandomierskie
		}
		has_opinion = { who = RUS value = 0 }
		has_opinion = { who = PRU value = 0 }
	}
	abort = {
		OR = {
			NOT = { exists = POL }
			NOT = { is_neighbor_of = POL }
		}
	}
	success = {
		OR = {
			owns = 259 # Lubelskie
			owns = 262 # Krakowskie
			owns = 1279 # Sandomierskie
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			POL = { NOT = { num_of_cities = ROOT } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = POL value = 0 } }
		}
	}
	effect = {
		add_prestige = 10
		set_country_flag = partitioned_poland
		add_country_modifier = {
			name = "polish_partitions"
			duration = 3650
		}
	}
}


austria_spain_relations = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = HAB
		exists = SPA
		is_free_or_tributary_trigger = yes
		government = monarchy
		NOT = { has_country_modifier = foreign_contacts }
		NOT = { war_with = SPA }
		NOT = { marriage_with = SPA }
		SPA = {
			is_free_or_tributary_trigger = yes
			government = monarchy
		}
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { government = monarchy }
			NOT = { exists = SPA }
			war_with = SPA
			SPA = {
				OR = {
					is_subject_other_than_tributary_trigger = yes
					NOT = { government = monarchy }
				}
			}
		}
	}
	success = {
		marriage_with = SPA
		SPA = { has_opinion = { who = HAB value = 100 } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = SPA value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = SPA value = -100 } }
		}
	}
	effect = {
		add_dip_power = 50
		add_country_modifier = {
			name = "foreign_contacts"
			duration = 500
		}
	}
}


austria_burgundy_relations = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = HAB
		exists = BUR
		is_free_or_tributary_trigger = yes
		government = monarchy
		NOT = { has_country_modifier = foreign_contacts }
		NOT = { war_with = BUR }
		NOT = { marriage_with = BUR }
		BUR = {
			is_free_or_tributary_trigger = yes
			government = monarchy
			num_of_cities = 4
			is_neighbor_of = FRA
		}
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { government = monarchy }
			NOT = { exists = BUR }
			war_with = BUR
			BUR = {
				OR = {
					is_subject_other_than_tributary_trigger = yes
					NOT = { government = monarchy }
				}
			}
		}
	}
	success = {
		marriage_with = BUR
		BUR = { has_opinion = { who = HAB value = 100 } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = BUR value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = BUR value = -100 } }
		}
	}
	effect = {
		add_adm_power = 50
		add_country_modifier = {
			name = "foreign_contacts"
			duration = 500
		}
	}
}


austria_bavaria_relations = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = HAB
		exists = BAV
		is_free_or_tributary_trigger = yes
		government = monarchy
		NOT = { has_country_modifier = foreign_contacts }
		NOT = { war_with = BAV }
		NOT = { marriage_with = BAV }
		BAV = {
			is_free_or_tributary_trigger = yes
			government = monarchy
		}
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { government = monarchy }
			NOT = { exists = BAV }
			war_with = BAV
			BAV = {
				OR = {
					is_subject_other_than_tributary_trigger = yes
					NOT = { government = monarchy }
				}
			}
		}
	}
	success = {
		marriage_with = BAV
		BAV = { has_opinion = { who = HAB value = 100 } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = BAV value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = BAV value = -100 } }
		}
	}
	effect = {
		add_dip_power = 50
		add_country_modifier = {
			name = "foreign_contacts"
			duration = 500
		}
	}
}


no_territory_to_the_ottomans = {
	
	type = country
	
	category = MIL
	
	allow = {
		tag = HAB
		exists = TUR
		is_free_or_tributary_trigger = yes
		is_neighbor_of = TUR
		austrian_circle_region = { owned_by = TUR }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		NOT = { war_with = TUR }
		NOT = { austrian_circle_region = { owned_by = TUR } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	effect = {
		add_prestige = 10
		add_war_exhaustion = -5
		add_country_modifier = {
			name = "drove_back_the_turk"
			duration = 3650
		}
	}
}


hegemony_over_north_italy = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	target_provinces_list = {
		108 # Verona
		1345 # Brescia
		2372 # Cremona
	}

	target_provinces = {
		NOT = { owned_by = ROOT }
	}

	allow = {
		tag = HAB
		is_free_or_tributary_trigger = yes
		mil = 4
		is_year = 1500
		NOT = { has_country_flag = had_italian_ambition_mission }
		NOT = { owns = 108 }	# Verona
		NOT = { owns = 1345 }	# Brescia
		NOT = { owns = 2372 }	# Cremona
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 5
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 5
		set_country_flag = had_italian_ambition_mission
		add_country_modifier = {
			name = "italian_ambition_modifier"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}


annex_hungary = {
	
	type = country
	
	category = DIP
	
	target_provinces = {
		owned_by = HUN
	}
	allow = {
		tag = HAB
		exists = HUN
		is_free_or_tributary_trigger = yes
		dip = 4
		HUN = {
			vassal_of = HAB
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
			owns = 1273 # Szolnok
			religion_group = ROOT
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = HUN }
			HUN = { NOT = { religion_group = ROOT } }
		}
	}
	success = {
		NOT = { exists = HUN }
		all_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_opinion = { who = HUN value = 100 }
		}
		modifier = {
			factor = 2
			has_opinion = { who = HUN value = 200 }
		}
	}
	effect = {
		add_prestige = 10
		add_adm_power = 50
		add_dip_power = 50
	}
}


protect_belgium_from_the_french = {
	
	type = country
	
	category = MIL
	
	allow = {
		tag = HAB
		exists = FRA
		is_free_or_tributary_trigger = yes
		FRA = { is_neighbor_of = ROOT }
		low_countries_region = { owned_by = ROOT }
		belgii_region = { owned_by = FRA }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { belgii_region = { owned_by = ROOT } }
		}
	}
	success = {
		NOT = { war_with = FRA }
		NOT = { belgii_region = { owned_by = FRA } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	effect = {
		add_war_exhaustion = -5
		every_owned_province = {
			limit = { OR = { region = belgii_region region = low_countries_region } }
			add_province_modifier = {
				name = "protected_austrian_low_countries"
				duration = 7300
			}
		}
	}
}

protect_hungary_from_the_ottomans = {
	
	type = country
	
	category = MIL
	
	target_areas_list = {
		transdanubia_area
		alfold_area
		kisalfold_area
	}
	
	target_provinces = {
		owned_by = TUR
	}
	allow = {
		tag = HAB
		exists = TUR
		is_free_or_tributary_trigger = yes
		NOT = { has_country_modifier = drove_back_the_turk }
		TUR = { is_neighbor_of = ROOT }
		magyar_plains_region = { owned_by = ROOT }
		magyar_plains_region = { owned_by = TUR }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { magyar_plains_region = { owned_by = ROOT } }
		}
	}
	success = {
		NOT = { war_with = TUR }
		NOT = { magyar_plains_region = { owned_by = TUR } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		add_war_exhaustion = -5
		add_country_modifier = {
			name = "drove_back_the_turk"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

protect_north_hungary_from_the_ottomans = {
	
	type = country
	
	category = MIL
	
	target_areas_list = {
		east_slovakia_area
		west_slovakia_area
		transylvania_area
	}
	
	target_provinces = {
		owned_by = TUR
	}
	allow = {
		tag = HAB
		exists = TUR
		is_free_or_tributary_trigger = yes
		NOT = { has_country_modifier = drove_back_the_turk }
		TUR = { is_neighbor_of = ROOT }
		north_carpathia_region = { owned_by = ROOT }
		north_carpathia_region = { owned_by = TUR }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { north_carpathia_region = { owned_by = ROOT } }
		}
	}
	success = {
		NOT = { war_with = TUR }
		NOT = { north_carpathia_region = { owned_by = TUR } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		add_war_exhaustion = -5
		add_country_modifier = {
			name = "drove_back_the_turk"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}

protect_italy_from_the_french = {
	
	type = country
	
	category = MIL
	
	target_areas_list = {
		lombardy_area
		piedmont_area
		liguria_area
		padan_area
		tuscany_area
		siena_area
		lazio_area
		venetia_area
		emilia_romagna_area
		marche_area
	}
	
	target_provinces = {
		owned_by = FRA
	}
	allow = {
		tag = HAB
		exists = FRA
		is_free_or_tributary_trigger = yes
		always = no #Removed, replaced by generic keep rival out of Italy mission
		mil = 4
		FRA = { is_neighbor_of = ROOT }
		italy_superregion = { owned_by = FRA }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		NOT = { war_with = FRA }
		NOT = { italy_superregion = { owned_by = FRA } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}


protect_italy_from_the_french_republic = {
	
	type = country
	
	category = MIL
	
	target_areas_list = {
		lombardy_area
		piedmont_area
		liguria_area
		padan_area
		tuscany_area
		siena_area
		lazio_area
		venetia_area
		emilia_romagna_area
		marche_area
	}
	
	target_provinces = {
		owned_by = FRA
		FRA = { is_revolution_target = yes }
	}
	allow = {
		tag = HAB
		exists = FRA
		is_free_or_tributary_trigger = yes
		always = no #Removed, replaced by generic keep rival out of Italy mission
		mil = 4
		FRA = { is_neighbor_of = ROOT }
		italy_superregion = { owned_by = FRA }
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		NOT = { war_with = FRA }
		NOT = { italy_superregion = { owned_by = FRA } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}


free_transylvania_from_the_ottomans = {
	
	type = country
	
	category = MIL
	
	target_areas_list = {
		transylvania_area
	}
	
	target_provinces = {
		owned_by = TUR
	}
	allow = {
		tag = HAB
		exists = TUR
		is_free_or_tributary_trigger = yes
		mil = 4
		NOT = { has_country_modifier = drove_back_the_turk }
		transylvania_area = { owned_by = TUR }
		TUR = { is_neighbor_of = ROOT }
		NOT = { war_with = TUR }
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = TUR }
			NOT = { TUR = { is_neighbor_of = ROOT } }
		}
	}
	success = {
		NOT = { war_with = TUR }
		NOT = { transylvania_area = { owned_by = TUR } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = TUR value = 0 } }
		}
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = TUR value = -100 } }
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "drove_back_the_turk"
			duration = 3650
		}
		every_target_province = {
			add_territorial_core_effect = yes
		}
	}
}
