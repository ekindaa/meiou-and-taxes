vassalize_shirvan = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = JAI
		NOT = { has_country_modifier = military_vassalization }
		exists = SHI
		is_free_or_tributary_trigger = yes
		SHI = {
			is_free_or_tributary_trigger = yes
			is_neighbor_of = ROOT
			NOT = { num_of_cities = ROOT }
		}
	}
	abort = {
		OR = {
			is_subject_other_than_tributary_trigger = yes
			NOT = { exists = SHI }
			SHI = {
				is_subject_other_than_tributary_trigger = yes
			}
		}
	}
	success = {
		SHI = { vassal_of = JAI }
	}
	chance = {
		factor = 5000
		modifier = {
			factor = 2
			NOT = { has_opinion = { who = SHI value = 0 } }
		}
	}
	immediate = {
		add_casus_belli = {
			type = cb_vassalize_mission
			months = 120
			target = SHI
		}
	}
	abort_effect = {
		remove_casus_belli = {
			type = cb_vassalize_mission
			target = SHI
		}
	}
	effect = {
		add_country_modifier = {
			name = military_vassalization
			duration = 3650
		}
		hidden_effect = {
			remove_casus_belli = {
				type = cb_vassalize_mission
				target = SHI
			}
		}
	}
}

acquire_tabriz = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		tag = JAI
		is_free_or_tributary_trigger = yes
		NOT = { owns = 416 } # Tabriz
	}
	abort = {
		is_subject_other_than_tributary_trigger = yes
	}
	success = {
		owns = 416 # Tabriz
	}
	chance = {
		factor = 5000
		modifier = {
			factor = 2
			416 = { # Tabriz
				owner = {
					NOT = { reverse_has_opinion = { who = PREV value = 0 } }
				}
			}
		}
	}
	effect = {
		add_prestige = 10
		add_adm_power = 25
	}
}