bohemia_marriage_england = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = BOH
		NOT = { is_year = 1400 }
		exists = ENG
		NOT = { has_country_modifier = foreign_contacts }
		ENG = {
			is_free_or_tributary_trigger = yes
			government = monarchy
			religion = ROOT
		}
		is_free_or_tributary_trigger = yes
		government = monarchy
		NOT = { war_with = ENG }
		NOT = { marriage_with = ENG }
	}
	abort = {
		OR = {
			NOT = { exists = ENG }
			ENG = { is_subject_other_than_tributary_trigger = yes }
			ENG = { NOT = { government = monarchy } }
			ENG = { NOT = { religion = ROOT } }
			war_with = ENG
			is_subject_other_than_tributary_trigger = yes
			NOT = { government = monarchy }
		}
	}
	success = {
		marriage_with = ENG
	}
	chance = {
		factor = 1000
	}
	effect = {
		add_adm_power = 50
		add_country_modifier = {
			name = "foreign_contacts"
			duration = 3650
		}
	}
}

england_marriage_bohemia = {
	
	type = country
	
	category = DIP
	
	allow = {
		tag = ENG
		NOT = { is_year = 1400 }
		exists = BOH
		NOT = { has_country_modifier = foreign_contacts }
		BOH = {
			is_free_or_tributary_trigger = yes
			government = monarchy
			religion = ROOT
		}
		is_free_or_tributary_trigger = yes
		government = monarchy
		NOT = { war_with = BOH }
		NOT = { marriage_with = BOH }
	}
	abort = {
		OR = {
			NOT = { exists = BOH }
			BOH = { is_subject_other_than_tributary_trigger = yes }
			BOH = { NOT = { government = monarchy } }
			BOH = { NOT = { religion = ROOT } }
			war_with = BOH
			is_subject_other_than_tributary_trigger = yes
			NOT = { government = monarchy }
		}
	}
	success = {
		marriage_with = BOH
	}
	chance = {
		factor = 1000
	}
	effect = {
		add_adm_power = 50
		add_country_modifier = {
			name = "foreign_contacts"
			duration = 3650
		}
	}
}
