conquer_bombay = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		normal_or_historical_nations = yes
		num_of_ports = 3
		is_free_or_tributary_trigger = yes
		religion_group = christian
		OR = {
			technology_group = western
			ai = no
		}
		OR = {
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		NOT = {
			indian_coast_group = {
				owned_by = ROOT
			}
		}
		NOT = { has_country_flag = western_india_mission_completed }
		NOT = { has_country_modifier = east_india_trade_rush }
		3144 = {
			range = ROOT
			has_discovered = ROOT
			NOT = { owned_by = ROOT }
			owner = {
				NOT = { is_subject_of = ROOT }
				NOT = { truce_with = ROOT }
				NOT = { alliance_with = ROOT }
				NOT = { technology_group = western }
			}
		}
	}
	abort = {
		NOT = { num_of_ports = 1 }
		3144 = {
			owner = {
				alliance_with = ROOT
				technology_group = western
				is_subject_of = ROOT
			}
		}
	}
	success = {
		3144 = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			OR = {
				has_idea_group = expansion_ideas
				has_idea_group = exploration_ideas
			}
		}
		modifier = {
			factor = 5
			OR = {
				tag = POR
				tag = ENG
				tag = GBR
			}
		}
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}
		modifier = {
			factor = 1.5
			has_idea = vice_roys
		}
		modifier = {
			factor = 1.5
			has_idea = shrewd_commerce_practise
		}
		modifier = {
			factor = 1.5
			3144 = {
				owner = {
					NOT = { technology_group = western }
				}
			}
		}
	}
	immediate = {
		3144 = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		3144 = {
			remove_claim = ROOT
		}
	}
	effect = {
		3144 = {
			add_territorial_core_effect = yes
		}
		set_country_flag = western_india_mission_completed
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
	}
}

conquer_goa = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		normal_or_historical_nations = yes
		num_of_ports = 3
		is_free_or_tributary_trigger = yes
		religion_group = christian
		OR = {
			technology_group = western
			ai = no
		}
		OR = {
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		NOT = {
			indian_coast_group = {
				owned_by = ROOT
			}
		}
		NOT = { has_country_flag = western_india_mission_completed }
		NOT = { has_country_modifier = east_india_trade_rush }
		531 = {
			range = ROOT
			has_discovered = ROOT
			NOT = { owned_by = ROOT }
			owner = {
				NOT = { is_subject_of = ROOT }
				NOT = { truce_with = ROOT }
				NOT = { alliance_with = ROOT }
				NOT = { technology_group = western }
			}
		}
	}
	abort = {
		NOT = { num_of_ports = 1 }
		531 = {
			owner = {
				alliance_with = ROOT
				technology_group = western
				is_subject_of = ROOT
			}
		}
	}
	success = {
		531 = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			OR = {
				has_idea_group = expansion_ideas
				has_idea_group = exploration_ideas
			}
		}
		modifier = {
			factor = 5
			tag = POR
		}
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}
		modifier = {
			factor = 1.5
			has_idea = vice_roys
		}
		modifier = {
			factor = 1.5
			has_idea = shrewd_commerce_practise
		}
		modifier = {
			factor = 1.5
			531 = {
				owner = {
					NOT = { technology_group = western }
				}
			}
		}
	}
	immediate = {
		531 = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		531 = {
			remove_claim = ROOT
		}
	}
	effect = {
		531 = {
			add_territorial_core_effect = yes
		}
		set_country_flag = western_india_mission_completed
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
	}
}

conquer_madras = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		normal_or_historical_nations = yes
		num_of_ports = 3
		is_free_or_tributary_trigger = yes
		religion_group = christian
		OR = {
			technology_group = western
			ai = no
		}
		OR = {
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		NOT = {
			indian_coast_group = {
				owned_by = ROOT
			}
		}
		NOT = { has_country_flag = western_india_mission_completed }
		NOT = { has_country_modifier = east_india_trade_rush }
		542 = {
			range = ROOT
			has_discovered = ROOT
			NOT = { owned_by = ROOT }
			owner = {
				NOT = { is_subject_of = ROOT }
				NOT = { truce_with = ROOT }
				NOT = { alliance_with = ROOT }
				NOT = { technology_group = western }
			}
		}
	}
	abort = {
		NOT = { num_of_ports = 1 }
		542 = {
			owner = {
				alliance_with = ROOT
				technology_group = western
				is_subject_of = ROOT
			}
		}
	}
	success = {
		542 = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			OR = {
				has_idea_group = expansion_ideas
				has_idea_group = exploration_ideas
			}
		}
		modifier = {
			factor = 5
			OR = {
				tag = ENG
				tag = GBR
				tag = FRA
			}
		}
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}
		modifier = {
			factor = 1.5
			has_idea = vice_roys
		}
		modifier = {
			factor = 1.5
			has_idea = shrewd_commerce_practise
		}
		modifier = {
			factor = 1.5
			542 = {
				owner = {
					NOT = { technology_group = western }
				}
			}
		}
	}
	immediate = {
		542 = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		542 = {
			remove_claim = ROOT
		}
	}
	effect = {
		542 = {
			add_territorial_core_effect = yes
		}
		set_country_flag = western_india_mission_completed
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
	}
}

conquer_pondicherry = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		normal_or_historical_nations = yes
		num_of_ports = 3
		is_free_or_tributary_trigger = yes
		religion_group = christian
		OR = {
			technology_group = western
			ai = no
		}
		OR = {
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		NOT = {
			indian_coast_group = {
				owned_by = ROOT
			}
		}
		NOT = { has_country_flag = western_india_mission_completed }
		NOT = { has_country_modifier = east_india_trade_rush }
		2245 = {
			range = ROOT
			has_discovered = ROOT
			NOT = { owned_by = ROOT }
			owner = {
				NOT = { is_subject_of = ROOT }
				NOT = { truce_with = ROOT }
				NOT = { alliance_with = ROOT }
				NOT = { technology_group = western }
			}
		}
	}
	abort = {
		NOT = { num_of_ports = 1 }
		2245 = {
			owner = {
				alliance_with = ROOT
				technology_group = western
				is_subject_of = ROOT
			}
		}
	}
	success = {
		2245 = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			OR = {
				has_idea_group = expansion_ideas
				has_idea_group = exploration_ideas
			}
		}
		modifier = {
			factor = 5
			OR = {
				tag = ENG
				tag = GBR
				tag = FRA
				tag = SWE
				tag = DAN
			}
		}
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}
		modifier = {
			factor = 1.5
			has_idea = vice_roys
		}
		modifier = {
			factor = 1.5
			has_idea = shrewd_commerce_practise
		}
		modifier = {
			factor = 1.5
			2245 = {
				owner = {
					NOT = { technology_group = western }
				}
			}
		}
	}
	immediate = {
		2245 = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		2245 = {
			remove_claim = ROOT
		}
	}
	effect = {
		2245 = {
			add_territorial_core_effect = yes
		}
		set_country_flag = western_india_mission_completed
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
	}
}

conquer_diu = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		normal_or_historical_nations = yes
		num_of_ports = 3
		is_free_or_tributary_trigger = yes
		religion_group = christian
		OR = {
			technology_group = western
			ai = no
		}
		OR = {
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		NOT = {
			indian_coast_group = {
				owned_by = ROOT
			}
		}
		NOT = { has_country_flag = western_india_mission_completed }
		NOT = { has_country_modifier = east_india_trade_rush }
		3238 = {
			range = ROOT
			has_discovered = ROOT
			NOT = { owned_by = ROOT }
			owner = {
				NOT = { is_subject_of = ROOT }
				NOT = { truce_with = ROOT }
				NOT = { alliance_with = ROOT }
				NOT = { technology_group = western }
			}
		}
	}
	abort = {
		NOT = { num_of_ports = 1 }
		3238 = {
			owner = {
				alliance_with = ROOT
				technology_group = western
				is_subject_of = ROOT
			}
		}
	}
	success = {
		3238 = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			OR = {
				has_idea_group = expansion_ideas
				has_idea_group = exploration_ideas
			}
		}
		modifier = {
			factor = 5
			tag = POR
		}
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}
		modifier = {
			factor = 1.5
			has_idea = vice_roys
		}
		modifier = {
			factor = 1.5
			has_idea = shrewd_commerce_practise
		}
		modifier = {
			factor = 1.5
			3238 = {
				owner = {
					NOT = { technology_group = western }
				}
			}
		}
	}
	immediate = {
		3238 = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		3238 = {
			remove_claim = ROOT
		}
	}
	effect = {
		3238 = {
			add_territorial_core_effect = yes
		}
		set_country_flag = western_india_mission_completed
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
	}
}

conquer_calcutta = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		normal_or_historical_nations = yes
		num_of_ports = 3
		is_free_or_tributary_trigger = yes
		religion_group = christian
		OR = {
			technology_group = western
			ai = no
		}
		OR = {
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		NOT = {
			indian_coast_group = {
				owned_by = ROOT
			}
		}
		NOT = { has_country_flag = western_india_mission_completed }
		NOT = { has_country_modifier = east_india_trade_rush }
		561 = {
			range = ROOT
			has_discovered = ROOT
			NOT = { owned_by = ROOT }
			owner = {
				NOT = { is_subject_of = ROOT }
				NOT = { truce_with = ROOT }
				NOT = { alliance_with = ROOT }
				NOT = { technology_group = western }
			}
		}
	}
	abort = {
		NOT = { num_of_ports = 1 }
		561 = {
			owner = {
				alliance_with = ROOT
				technology_group = western
				is_subject_of = ROOT
			}
		}
	}
	success = {
		561 = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			OR = {
				has_idea_group = expansion_ideas
				has_idea_group = exploration_ideas
			}
		}
		modifier = {
			factor = 5
			OR = {
				tag = ENG
				tag = GBR
			}
		}
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}
		modifier = {
			factor = 1.5
			has_idea = vice_roys
		}
		modifier = {
			factor = 1.5
			has_idea = shrewd_commerce_practise
		}
		modifier = {
			factor = 1.5
			561 = {
				owner = {
					NOT = { technology_group = western }
				}
			}
		}
	}
	immediate = {
		561 = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		561 = {
			remove_claim = ROOT
		}
	}
	effect = {
		561 = {
			add_territorial_core_effect = yes
		}
		set_country_flag = western_india_mission_completed
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
	}
}

conquer_kotte = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		normal_or_historical_nations = yes
		num_of_ports = 3
		is_free_or_tributary_trigger = yes
		religion_group = christian
		OR = {
			technology_group = western
			ai = no
		}
		OR = {
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		NOT = {
			indian_coast_group = {
				owned_by = ROOT
			}
		}
		NOT = { has_country_flag = western_india_mission_completed }
		NOT = { has_country_modifier = east_india_trade_rush }
		573 = {
			range = ROOT
			has_discovered = ROOT
			NOT = { owned_by = ROOT }
			owner = {
				NOT = { is_subject_of = ROOT }
				NOT = { truce_with = ROOT }
				NOT = { alliance_with = ROOT }
				NOT = { technology_group = western }
			}
		}
	}
	abort = {
		NOT = { num_of_ports = 1 }
		573 = {
			owner = {
				alliance_with = ROOT
				technology_group = western
				is_subject_of = ROOT
			}
		}
	}
	success = {
		573 = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			OR = {
				has_idea_group = expansion_ideas
				has_idea_group = exploration_ideas
			}
		}
		modifier = {
			factor = 5
			OR = {
				tag = ENG
				tag = GBR
				tag = NED
				tag = POR
			}
		}
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}
		modifier = {
			factor = 1.5
			has_idea = vice_roys
		}
		modifier = {
			factor = 1.5
			has_idea = shrewd_commerce_practise
		}
		modifier = {
			factor = 1.5
			573 = {
				owner = {
					NOT = { technology_group = western }
				}
			}
		}
	}
	immediate = {
		573 = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		573 = {
			remove_claim = ROOT
		}
	}
	effect = {
		573 = {
			add_territorial_core_effect = yes
		}
		set_country_flag = western_india_mission_completed
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
	}
}

conquer_kochin = {
	
	type = country
	
	category = MIL
	ai_mission = yes
	
	allow = {
		normal_or_historical_nations = yes
		num_of_ports = 3
		is_free_or_tributary_trigger = yes
		religion_group = christian
		OR = {
			technology_group = western
			ai = no
		}
		OR = {
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		NOT = {
			indian_coast_group = {
				owned_by = ROOT
			}
		}
		NOT = { has_country_flag = western_india_mission_completed }
		NOT = { has_country_modifier = east_india_trade_rush }
		2570 = {
			range = ROOT
			has_discovered = ROOT
			NOT = { owned_by = ROOT }
			owner = {
				NOT = { is_subject_of = ROOT }
				NOT = { truce_with = ROOT }
				NOT = { alliance_with = ROOT }
				NOT = { technology_group = western }
			}
		}
	}
	abort = {
		NOT = { num_of_ports = 1 }
		2570 = {
			owner = {
				alliance_with = ROOT
				technology_group = western
				is_subject_of = ROOT
			}
		}
	}
	success = {
		2570 = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			OR = {
				has_idea_group = expansion_ideas
				has_idea_group = exploration_ideas
			}
		}
		modifier = {
			factor = 5
			tag = NED
		}
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}
		modifier = {
			factor = 1.5
			has_idea = vice_roys
		}
		modifier = {
			factor = 1.5
			has_idea = shrewd_commerce_practise
		}
		modifier = {
			factor = 1.5
			2570 = {
				owner = {
					NOT = { technology_group = western }
				}
			}
		}
	}
	immediate = {
		2570 = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		2570 = {
			remove_claim = ROOT
		}
	}
	effect = {
		2570 = {
			add_territorial_core_effect = yes
		}
		set_country_flag = western_india_mission_completed
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
	}
}

make_base_on_spice_islands_jakarta = {
	
	type = country
	
	category = DIP
	ai_mission = yes
	
	allow = {
		normal_or_historical_nations = yes
		num_of_ports = 3
		is_free_or_tributary_trigger = yes
		religion_group = christian
		OR = {
			technology_group = western
			ai = no
		}
		OR = {
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		NOT = { has_country_flag = spice_islands_completed }
		NOT = { has_country_modifier = east_india_trade_rush }
		NOT = { tag = POR }
		2108 = {
			has_discovered = ROOT
			range = ROOT
			NOT = { owned_by = ROOT }
			owner = {
				NOT = { is_subject_of = ROOT }
				NOT = { truce_with = ROOT }
				NOT = { alliance_with = ROOT }
				NOT = { technology_group = western }
			}
		}
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			is_subject_other_than_tributary_trigger = yes
		}
	}
	success = {
		2108 = {
			owned_by = ROOT
		}
	}
	immediate = {
		2108 = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		2108 = {
			remove_claim = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}
		modifier = {
			factor = 1.5
			has_idea = vice_roys
		}
		modifier = {
			factor = 2
			num_of_colonists = 2
		}
		modifier = {
			factor = 2
			num_of_colonists = 3
		}
		modifier = {
			factor = 1.5
			has_idea = quest_for_the_new_world
		}
	}
	effect = {
		2108 = {
			add_territorial_core_effect = yes
		}
		set_country_flag = spice_islands_completed
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
	}
}

make_base_on_spice_islands_pasai = {
	
	type = country
	
	category = DIP
	ai_mission = yes
	
	allow = {
		normal_or_historical_nations = yes
		num_of_ports = 3
		is_free_or_tributary_trigger = yes
		religion_group = christian
		OR = {
			technology_group = western
			ai = no
		}
		OR = {
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		NOT = { has_country_flag = spice_islands_completed }
		NOT = { has_country_modifier = east_india_trade_rush }
		NOT = { tag = POR }
		2093 = {
			has_discovered = ROOT
			range = ROOT
			NOT = { owned_by = ROOT }
			owner = {
				NOT = { is_subject_of = ROOT }
				NOT = { truce_with = ROOT }
				NOT = { alliance_with = ROOT }
				NOT = { technology_group = western }
			}
		}
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			is_subject_other_than_tributary_trigger = yes
		}
	}
	success = {
		2093 = {
			owned_by = ROOT
		}
	}
	immediate = {
		2093 = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		2093 = {
			remove_claim = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}
		modifier = {
			factor = 1.5
			has_idea = vice_roys
		}
		modifier = {
			factor = 2
			num_of_colonists = 2
		}
		modifier = {
			factor = 2
			num_of_colonists = 3
		}
		modifier = {
			factor = 1.5
			has_idea = quest_for_the_new_world
		}
	}
	effect = {
		2093 = {
			add_territorial_core_effect = yes
		}
		set_country_flag = spice_islands_completed
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
	}
}

make_base_on_spice_islands_makassar = {
	
	type = country
	
	category = DIP
	ai_mission = yes
	
	allow = {
		normal_or_historical_nations = yes
		num_of_ports = 3
		is_free_or_tributary_trigger = yes
		religion_group = christian
		OR = {
			technology_group = western
			ai = no
		}
		OR = {
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		NOT = { has_country_flag = spice_islands_completed }
		NOT = { has_country_modifier = east_india_trade_rush }
		NOT = { tag = POR }
		641 = {
			has_discovered = ROOT
			range = ROOT
			NOT = { owned_by = ROOT }
			owner = {
				NOT = { is_subject_of = ROOT }
				NOT = { truce_with = ROOT }
				NOT = { alliance_with = ROOT }
				NOT = { technology_group = western }
			}
		}
	}
	abort = {
		OR = {
			NOT = { num_of_ports = 1 }
			is_subject_other_than_tributary_trigger = yes
		}
	}
	success = {
		641 = {
			owned_by = ROOT
		}
	}
	immediate = {
		641 = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		641 = {
			remove_claim = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		modifier = {
			factor = 1.5
			num_of_ports = 5
		}
		modifier = {
			factor = 1.5
			has_idea = vice_roys
		}
		modifier = {
			factor = 2
			num_of_colonists = 2
		}
		modifier = {
			factor = 2
			num_of_colonists = 3
		}
		modifier = {
			factor = 1.5
			has_idea = quest_for_the_new_world
		}
	}
	effect = {
		641 = {
			add_territorial_core_effect = yes
		}
		set_country_flag = spice_islands_completed
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
	}
}

#Get Foothold in China:

conquer_macau = {
	
	type = country
	
	category = MIL
	#ai_mission = yes
	
	allow = {
		normal_or_historical_nations = yes
		num_of_ports = 5
		monthly_income = 100
		is_free_or_tributary_trigger = yes
		religion_group = christian
		OR = {
			technology_group = western
			ai = no
		}
		OR = {
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		NOT = { has_country_flag = chinese_port_taken }
		NOT = { has_country_modifier = east_india_trade_rush }
		677 = {
			range = ROOT
			has_discovered = ROOT
			NOT = { owned_by = ROOT }
			owner = {
				NOT = { is_subject_of = ROOT }
				NOT = { truce_with = ROOT }
				NOT = { alliance_with = ROOT }
				NOT = { technology_group = western }
			}
		}
		NOT = {
			677 = {
				owned_by = ROOT
			}
		}
		indian_coast_group = {
			owned_by = ROOT
		}
	}
	abort = {
		NOT = { num_of_ports = 1 }
	}
	success = {
		677 = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			OR = {
				has_idea_group = expansion_ideas
				has_idea_group = exploration_ideas
			}
		}
		modifier = {
			factor = 1.5
			num_of_ports = 10
		}
		modifier = {
			factor = 1.5
			has_idea = vice_roys
		}
		modifier = {
			factor = 1.5
			has_idea = shrewd_commerce_practise
		}
		modifier = {
			factor = 1.5
			677 = {
				owner = {
					NOT = { technology_group = western }
				}
			}
		}
	}
	immediate = {
		677 = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		677 = {
			remove_claim = ROOT
		}
	}
	effect = {
		set_country_flag = chinese_port_taken
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
		677 = {
			add_territorial_core_effect = yes
		}
	}
}

conquer_ningbo = {
	
	type = country
	
	category = MIL
	#ai_mission = yes
	
	allow = {
		normal_or_historical_nations = yes
		num_of_ports = 5
		monthly_income = 100
		is_free_or_tributary_trigger = yes
		religion_group = christian
		OR = {
			technology_group = western
			ai = no
		}
		OR = {
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		NOT = { has_country_flag = chinese_port_taken }
		NOT = { has_country_modifier = east_india_trade_rush }
		2272 = {
			range = ROOT
			has_discovered = ROOT
			NOT = { owned_by = ROOT }
			owner = {
				NOT = { is_subject_of = ROOT }
				NOT = { truce_with = ROOT }
				NOT = { alliance_with = ROOT }
				NOT = { technology_group = western }
			}
		}
		NOT = {
			2272 = {
				owned_by = ROOT
			}
		}
		indian_coast_group = {
			owned_by = ROOT
		}
	}
	abort = {
		NOT = { num_of_ports = 1 }
	}
	success = {
		2272 = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			OR = {
				has_idea_group = expansion_ideas
				has_idea_group = exploration_ideas
			}
		}
		modifier = {
			factor = 1.5
			num_of_ports = 10
		}
		modifier = {
			factor = 1.5
			has_idea = vice_roys
		}
		modifier = {
			factor = 1.5
			has_idea = shrewd_commerce_practise
		}
		modifier = {
			factor = 1.5
			2272 = {
				owner = {
					NOT = { technology_group = western }
				}
			}
		}
	}
	immediate = {
		2272 = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		2272 = {
			remove_claim = ROOT
		}
	}
	effect = {
		set_country_flag = chinese_port_taken
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
		2272 = {
			add_territorial_core_effect = yes
		}
	}
}

conquer_canton = {
	
	type = country
	
	category = MIL
	#ai_mission = yes
	
	allow = {
		normal_or_historical_nations = yes
		num_of_ports = 5
		monthly_income = 100
		is_free_or_tributary_trigger = yes
		religion_group = christian
		OR = {
			technology_group = western
			ai = no
		}
		OR = {
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		NOT = { has_country_flag = chinese_port_taken }
		NOT = { has_country_modifier = east_india_trade_rush }
		2121 = {
			range = ROOT
			has_discovered = ROOT
			NOT = { owned_by = ROOT }
			owner = {
				NOT = { is_subject_of = ROOT }
				NOT = { truce_with = ROOT }
				NOT = { alliance_with = ROOT }
				NOT = { technology_group = western }
			}
		}
		NOT = {
			2121 = {
				owned_by = ROOT
			}
		}
		indian_coast_group = {
			owned_by = ROOT
		}
	}
	abort = {
		NOT = { num_of_ports = 1 }
	}
	success = {
		2121 = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			OR = {
				has_idea_group = expansion_ideas
				has_idea_group = exploration_ideas
			}
		}
		modifier = {
			factor = 1.5
			num_of_ports = 10
		}
		modifier = {
			factor = 1.5
			has_idea = vice_roys
		}
		modifier = {
			factor = 1.5
			has_idea = shrewd_commerce_practise
		}
		modifier = {
			factor = 1.5
			2121 = {
				owner = {
					NOT = { technology_group = western }
				}
			}
		}
	}
	immediate = {
		2121 = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		2121 = {
			remove_claim = ROOT
		}
	}
	effect = {
		set_country_flag = chinese_port_taken
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
		2121 = {
			add_territorial_core_effect = yes
		}
	}
}

conquer_hongkong = {
	
	type = country
	
	category = MIL
	#ai_mission = yes
	
	allow = {
		normal_or_historical_nations = yes
		num_of_ports = 5
		monthly_income = 100
		is_free_or_tributary_trigger = yes
		religion_group = christian
		OR = {
			technology_group = western
			ai = no
		}
		OR = {
			has_idea_group = expansion_ideas
			has_idea_group = exploration_ideas
		}
		NOT = { has_country_flag = chinese_port_taken }
		NOT = { has_country_modifier = east_india_trade_rush }
		676 = {
			range = ROOT
			has_discovered = ROOT
			NOT = { owned_by = ROOT }
			owner = {
				NOT = { is_subject_of = ROOT }
				NOT = { truce_with = ROOT }
				NOT = { alliance_with = ROOT }
				NOT = { technology_group = western }
			}
		}
		NOT = {
			676 = {
				owned_by = ROOT
			}
		}
		indian_coast_group = {
			owned_by = ROOT
		}
	}
	abort = {
		NOT = { num_of_ports = 1 }
	}
	success = {
		676 = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 3
			OR = {
				has_idea_group = expansion_ideas
				has_idea_group = exploration_ideas
			}
		}
		modifier = {
			factor = 1.5
			num_of_ports = 10
		}
		modifier = {
			factor = 1.5
			has_idea = vice_roys
		}
		modifier = {
			factor = 1.5
			has_idea = shrewd_commerce_practise
		}
		modifier = {
			factor = 1.5
			2121 = {
				owner = {
					NOT = { technology_group = western }
				}
			}
		}
	}
	immediate = {
		676 = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		676 = {
			remove_claim = ROOT
		}
	}
	effect = {
		set_country_flag = chinese_port_taken
		add_country_modifier = {
			name = "east_india_trade_rush"
			duration = 3650
		}
		676 = {
			add_territorial_core_effect = yes
		}
	}
}