# YMN - Yamana clan

government = japanese_monarchy
government_rank = 1
mercantilism = 0.0
primary_culture = chugoku
religion = mahayana
technology_group = chinese
capital = 1031

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}

1328.1.1 = {
	monarch = {
		name = "Tokiuji" # 1303 - 1371
		dynasty = "Yamana"
		ADM = 4
		DIP = 4
		MIL = 4
	}
}

1328.1.1 = {
	heir = {
		name = "Moroyoshi"
		monarch_name = "Moroyoshi"
		dynasty = "Yamana"
		birth_date = 1328.1.1
		death_date = 1376.3.31
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1371.4.14 = {
	monarch = {
		name = "Moroyoshi"
		dynasty = "Yamana"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1376.3.31 = {
	monarch = {
		name = "Tokiyoshi"
		dynasty = "Yamana"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1389.1.1 = {
	monarch = {
		name = "Tokihiro" # 1367 - 1435
		dynasty = "Yamana"
		ADM = 3
		DIP = 3
		MIL = 2
	}
}

1404.7.6 = {
	heir = {
		name = "Mochitoyo"
		monarch_name = "Mochitoyo"
		dynasty = "Yamana"
		birth_date = 1404.7.6
		death_date = 1473.4.15
		claim = 90
		ADM = 3
		DIP = 4
		MIL = 4
	}
}

1432.1.1 = {
	monarch = {
		name = "Mochitoyo" # Yamana Sozen
		dynasty = "Yamana"
#		birth_date = 1404.7.6
#		death_date = 1473.4.15
		ADM = 3
		DIP = 4
		MIL = 4
	}
}

1432.1.1 = {
	heir = {
		name = "Noritoyo"
		monarch_name = "Noritoyo"
		dynasty = "Yamana"
		birth_date = 1424.1.1
		death_date = 1467.10.7
		claim = 90
		ADM = 2
		DIP = 2
		MIL = 3
	}
}

1467.10.7 = {
	heir = {
		name = "Masatoyo"
		monarch_name = "Masatoyo"
		dynasty = "Yamana"
		birth_date = 1441.1.1
		death_date = 1499.1.1
		claim = 90
		ADM = 2
		DIP = 2
		MIL = 3
	}
}

1473.4.15 = {
	monarch = {
		name = "Masatoyo"
		dynasty = "Yamana"
#		birth_date = 1441.1.1
#		death_date = 1499.1.1
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1475.1.1 = {
	heir = {
		name = "Okitoyo"
		monarch_name = "Okitoyo"
		dynasty = "Yamana"
		birth_date = 1475.1.1
		death_date = 1528.1.1
		claim = 90
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1499.1.1 = {
	monarch = {
		name = "Masatoyo"
		dynasty = "Yamana"
#		birth_date = 1475.1.1
#		death_date = 1528.1.1
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1511.1.1 = {
	heir = {
		name = "Suketoyo"
		monarch_name = "Suketoyo"
		dynasty = "Yamana"
		birth_date = 1511.1.1
		death_date = 1580.7.2
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 2
	}
}

1528.1.1 = {
	monarch = {
		name = "Suketoyo"
		dynasty = "Yamana"
#		birth_date = 1511.1.1
#		death_date = 1580.7.2
		ADM = 3
		DIP = 3
		MIL = 2
	}
}

1552.1.1 = {
	heir = {
		name = "Akihiro"
		monarch_name = "Akihiro"
		dynasty = "Yamana"
		birth_date = 1552.1.1
		death_date = 1612.1.1
		claim = 90
		ADM = 2
		DIP = 2
		MIL = 2
	}
}
