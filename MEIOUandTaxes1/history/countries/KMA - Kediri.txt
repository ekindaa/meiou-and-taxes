# Country: Kediri
# file name: KMA - Kediri
# MEIOU-FB Indonesia mod v3 - for IN+JV
# Kediri is used to represent Kediri + Malang
# 2010-jan-16 - FB - HT3 changes

government = eastern_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = javan
religion = hinduism
technology_group = austranesian
capital = 626	# Kediri

1000.1.1 = {
	add_country_modifier = { name = title_4 duration = -1 }
	set_country_flag = title_4
	#set_variable = { which = "centralization_decentralization" value = 1 }
	add_absolutism = -100
	add_absolutism = 40
}

1337.1.1 = {
	monarch = {
		name = "Kertajaya"
		dynasty = "Chandrakapala"
		ADM = 3
		DIP = 3
		MIL = 3
	}#Kertajaya ruled Kediri c1200
}

1550.1.1 = {
	religion = sunni
	remove_country_modifier = title_4 clr_country_flag = title_4 add_country_modifier = { name = title_4 duration = -1 }
	set_country_flag = title_4
}
