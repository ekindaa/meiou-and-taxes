# GGK - Empire of Gengis Khan
# Unification nation

government = tribal_nomads_altaic government_rank = 1
mercantilism = 0.0
technology_group = chinese
religion = vajrayana #DEI GRATIA
primary_culture = mongol
capital = 702	# Hohhot

1000.1.1 = {
	add_country_modifier = { name = title_6 duration = -1 }
	set_country_flag = title_6
	#set_variable = { which = "centralization_decentralization" value = 0 }
	add_absolutism = -100
	add_absolutism = 50
}
