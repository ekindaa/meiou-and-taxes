# SLB - Selabar

government = tribal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = sumatran
religion = hinduism
technology_group = austranesian
capital = 621

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}
