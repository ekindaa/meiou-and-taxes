#MEIOU-GG Governemnt changes

government = feudal_monarchy government_rank = 1
# centralization_decentralization = -5
mercantilism = 0.0
primary_culture = occitain
add_accepted_culture = gascon
add_accepted_culture = limousin
add_accepted_culture = auvergnat
religion = catholic
technology_group = western
capital = 196	# Toulouse
fixed_capital = 196

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = -5 }
	add_absolutism = -100
	add_absolutism = 100
}

1515.1.1 = {
	government = despotic_monarchy remove_country_modifier = title_3 clr_country_flag = title_3 add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
}

1560.1.1 = {
	religion = reformed
}

1589.8.3 = {
	government = administrative_monarchy remove_country_modifier = title_3 clr_country_flag = title_3 add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
}

1661.3.9 = {
	government = absolute_monarchy remove_country_modifier = title_3 clr_country_flag = title_3 add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
}
