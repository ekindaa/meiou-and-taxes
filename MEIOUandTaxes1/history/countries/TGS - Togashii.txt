# TGS - Togashi
# LS/GG - Japanese Civil War
# 2010-jan-20 - FB - HT3
# 2013-aug-07 - GG - EUIV changes

government = japanese_monarchy
government_rank = 1
mercantilism = 0.0
primary_culture = koshi
religion = mahayana
technology_group = chinese
capital = 3953 # Kaga

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}

1340.1.1 = {
	monarch = {
		name = "Ujiharu"
		dynasty = "Togashi"
		ADM = 3
		DIP = 4
		MIL = 3
	}
}

1350.1.1 = {
	heir = {
		name = "Masaie"
		monarch_name = "Masaie"
		dynasty = "Togashi"
		birth_date = 1350.1.1
		death_date = 1387.1.1
		claim = 90
		ADM = 4
		DIP = 4
		MIL = 3
	}
}
