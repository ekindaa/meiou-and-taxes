
government = chinese_monarchy_2 government_rank = 1
mercantilism = 0.0
technology_group = chinese
religion = confucianism
primary_culture = jilu
capital = 708

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = -2 }
	add_absolutism = -100
	add_absolutism = 70
}
