# BAC - Bacan
# GG - Modified from Divide et Impera

government = tribal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = halmahera
religion = polynesian_religion
technology_group = austranesian
capital = 2431	# Bacan

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}

1356.1.1 = {
	monarch = {
		name = "Kolano"
		dynasty = "Madehe"
		ADM = 4
		DIP = 4
		MIL = 2
	}
}
1521.1.1 = {
	religion = sunni
	government = eastern_monarchy
}

1557.1.1 = {
	religion = catholic
}

1578.1.1 = {
	religion = sunni
} # Ternate invades and the king apostatizes

1750.1.1 = {
	monarch = {
		name = "Sultan Hamarullah"
		ADM = 5
		DIP = 4
		MIL = 4
	}
}
