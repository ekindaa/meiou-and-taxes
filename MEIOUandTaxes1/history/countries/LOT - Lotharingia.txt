# LOT - Lotharinagia
# 2010-jan-21 - FB - HT3 changes

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = lorrain
add_accepted_culture = francien
add_accepted_culture = wallonian
add_accepted_culture = flemish
add_accepted_culture = brabantian
religion = catholic
technology_group = western
capital = 192	# Dijon

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 20
}

1506.9.26 = { government = despotic_monarchy remove_country_modifier = title_5 clr_country_flag = title_5 add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5 }

1589.8.3 = { government = administrative_monarchy remove_country_modifier = title_5 clr_country_flag = title_5 add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5 }

1661.3.9 = { government = absolute_monarchy remove_country_modifier = title_5 clr_country_flag = title_5 add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5 }
