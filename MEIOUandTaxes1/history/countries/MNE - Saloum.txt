# MNE - Saloum

government = tribal_monarchy_elective government_rank = 1
mercantilism = 0.0
primary_culture = wolof
religion = west_african_pagan_reformed
technology_group = sub_saharan
capital = 2218 # Kahone

1000.1.1 = {
	add_country_modifier = { name = title_2 duration = -1 }
	set_country_flag = title_2
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 0
}

1340.1.1 = {
	monarch = {
		name = "Biram Njeme Naxana"
		dynasty = "Gelwar"
		DIP = 3
		ADM = 3
		MIL = 3
	}
}