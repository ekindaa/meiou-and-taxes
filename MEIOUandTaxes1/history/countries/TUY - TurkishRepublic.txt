# TUR - Turkish Republic

government = constitutional_republic
mercantilism = 0.1
primary_culture = turkish
religion = sunni
technology_group = turkishtech
capital = 323

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = -2 }
}
