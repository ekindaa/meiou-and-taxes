# MIH - Mihrabanid
# 2010-jan-21 - FB - HT3 changes

government = despotic_monarchy government_rank = 1 #KINGDOM
mercantilism = 0.0
technology_group = muslim
primary_culture = persian
religion = sunni
capital = 551
fixed_capital = 551 # Sistan

1000.1.1 = {
	add_country_modifier = { name = title_4 duration = -1 }
	set_country_flag = title_4
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}

1236.1.1   = {
	monarch = {
		name = "Shams al-Din 'Ali"
		dynasty = "Mihrabanid"
		ADM = 4
		DIP = 3
		MIL = 5
	}
}

1261.1.1   = {
	monarch = {
		name = "Nasir al-Din Muhammad"
		dynasty = "Mihrabanid"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1318.1.1   = {
	monarch = {
		name = "Nusrat al-Din Muhammad"
		dynasty = "Mihrabanid"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1330.1.1   = {
	monarch = {
		name = "Qutb al-Din Muhammad"
		dynasty = "Mihrabanid"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1346.1.1   = {
	monarch = {
		name = "Taj al-Din ibn Qutb al-Din"
		dynasty = "Mihrabanid"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1350.1.1   = {
	monarch = {
		name = "Jalal al-Din Mahmud"
		dynasty = "Mihrabanid"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	heir  = {
		name = "'Izz al-Din"
		monarch_name = "'Izz al-Din ibn Rukn"
		dynasty = "Mihrabanid"
		birth_date = 1320.1.1
		death_date = 1380.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1352.1.1   = {
	monarch = {
		name = "'Izz al-Din ibn Rukn"
		dynasty = "Mihrabanid"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	heir  = {
		name = "Qutb al-Din"
		monarch_name = "Qutb al-Din ibn 'Izz"
		dynasty = "Mihrabanid"
		birth_date = 1340.1.1
		death_date = 1383.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1380.1.1   = {
	monarch = {
		name = "Qutb al-Din ibn 'Izz"
		dynasty = "Mihrabanid"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	heir  = {
		name = "Taj al-Din"
		monarch_name = "Taj al-Din Shah-i Shahan"
		dynasty = "Mihrabanid"
		birth_date = 1360.1.1
		death_date = 1403.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1383.1.1   = {
	monarch = {
		name = "Taj al-Din Shah-i Shahan"
		dynasty = "Mihrabanid"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	heir  = {
		name = "Qutb al-Din Muhammad"
		monarch_name = "Qutb al-Din Muhammad ibn Shams"
		dynasty = "Mihrabanid"
		birth_date = 1380.1.1
		death_date = 1419.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1403.1.1   = {
	monarch = {
		name = "Qutb al-Din Muhammad ibn Shams"
		dynasty = "Mihrabanid"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	heir  = {
		name = "Shams al-Din 'Ali"
		monarch_name = "Shams al-Din 'Ali ibn Qutb"
		dynasty = "Mihrabanid"
		birth_date = 1400.1.1
		death_date = 1438.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1419.1.1   = {
	monarch = {
		name = "Shams al-Din 'Ali ibn Qutb"
		dynasty = "Mihrabanid"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	heir  = {
		name = "Nizam al-Din Yahya"
		monarch_name = "Nizam al-Din Yahya"
		dynasty = "Mihrabanid"
		birth_date = 1419.1.1
		death_date = 1480.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1438.1.1   = {
	monarch = {
		name = "Nizam al-Din Yahya"
		dynasty = "Mihrabanid"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	heir  = {
		name = "Shams al-Din Muhammad"
		monarch_name = "Shams al-Din Muhammad"
		dynasty = "Mihrabanid"
		birth_date = 1438.1.1
		death_date = 1495.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1480.1.1   = {
	monarch = {
		name = "Shams al-Din Muhammad"
		dynasty = "Mihrabanid"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	heir  = {
		name = "Sultan Mahmud"
		monarch_name = "Sultan Mahmud ibn Nizam"
		dynasty = "Mihrabanid"
		birth_date = 1458.1.1
		death_date = 1537.1.1
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1495.1.1   = {
	monarch = {
		name = "Sultan Mahmud ibn Nizam"
		dynasty = "Mihrabanid"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

# 1537 - Recognizes the authority of the Saffavids
