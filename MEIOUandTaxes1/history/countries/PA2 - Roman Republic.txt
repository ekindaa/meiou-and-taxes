# PA2 - Roman Republic

government = noble_republic
government_rank = 1
primary_culture = umbrian
religion = catholic
technology_group = western
capital = 2530 #Roma
fixed_capital = 2530

1000.1.1 = {
	add_country_modifier = { name = title_1 duration = -1 }
	set_country_flag = title_1
	add_absolutism = -100
	add_absolutism = 20
}

1356.1.1 = {
	monarch = {
		name = "Agapito"
		dynasty = "Colonna"
		dip = 2
		adm = 2
		mil = 2
	}
}

