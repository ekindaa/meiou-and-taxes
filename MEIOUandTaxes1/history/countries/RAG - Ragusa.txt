# RAG - Ragusa.txt

government = merchant_republic government_rank = 1
mercantilism = 10
primary_culture = dalmatian
add_accepted_culture = croatian
religion = catholic
technology_group = eastern
capital = 137	# Ragusa
historical_friend = ANC
historical_rival = VEN

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 0 }
	add_absolutism = -100
	add_absolutism = 30
}

1356.1.1 = {
	monarch = {
		name = "Vijece"
		ADM = 4
		DIP = 4
		MIL = 3
	}
} # Noble councils. 
