# TGW - Tokugawa clan

government = japanese_monarchy
government_rank = 1
mercantilism = 0.0
primary_culture = kansai
religion = mahayana
technology_group = chinese
capital = 2283	# Yamashiro

historical_friend = KOR
historical_friend = JOS

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}

1549.1.1   = {
	monarch = {
		name = "Ieyasu"
		dynasty = "Tokugawa"
		ADM = 6
		DIP = 5
		MIL = 5
	}
}

1579.5.2 = {
	heir = {
		name = "Hidetada"
		monarch_name = "Hidetada"
		dynasty = "Tokugawa"
		birth_date = 1579.5.2
		death_date = 1632.3.14
		claim = 80
		ADM = 5
		DIP = 4
		MIL = 2
	}
}

1598.1.1   = {
	remove_country_modifier = title_3 clr_country_flag = title_3 add_country_modifier = { name = title_4 duration = -1 }
	set_country_flag = title_4
}

1600.9.15  = {
	government = japanese_monarchy
	remove_country_modifier = title_4 clr_country_flag = title_4 add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
}

1615.6.4   = {
	government = japanese_monarchy
}
