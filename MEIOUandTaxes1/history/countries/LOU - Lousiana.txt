# LOU - Louisiana

government = constitutional_republic government_rank = 1
mercantilism = 0.0
technology_group = western
primary_culture = french_colonial
religion = catholic
capital = 922	# Mobile

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = 5 }
	add_absolutism = -100
	add_absolutism = 0
}

1720.1.1 = { capital = 921 } # Biloxi
