# TFL - Tafilalt

government = despotic_monarchy government_rank = 1
primary_culture = fassi
religion = sunni
technology_group = turkishtech
unit_type = muslim
capital = 349	# Sijilmassa
historical_neutral = TLE

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 3 }
	add_absolutism = -100
	add_absolutism = 20
}

1659.6.5 = {
	monarch = {
		name = "Muhammad ibn Sharif"
		dynasty = "Alaouite"
		adm = 3
		dip = 4
		mil = 5
	}
	add_piety = 1.0
}

1668.8.2 = {
	monarch = {
		name = "ar-Rashid"
		dynasty = "Alaouite"
		adm = 4
		dip = 3
		mil = 5
		leader = { name = "ar-Rashid"		type = general	fire = 4	shock = 3	manuever = 4	siege = 1 }
	}
	add_piety = 1.0
}

#1668.9.7 - Reunification of Morocco under the Alaouites
