# KRA - Republic of Krakow
# 2010-jan-21 - FB - HT3 changes

government = constitutional_republic government_rank = 1
mercantilism = 0.0
primary_culture = polish
religion = catholic
technology_group = eastern
capital = 262	# Krakow

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}

1815.5.3 = {
	monarch = {
		name = "National Assembly"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
