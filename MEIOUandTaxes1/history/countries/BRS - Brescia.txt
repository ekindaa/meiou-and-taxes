# BRS - Brescia

government = merchant_republic government_rank = 1
mercantilism = 0.0
primary_culture = venetian
religion = catholic
technology_group = western
capital = 1345	# Brescia

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}
1356.1.1 = { #FB - temp pending further research
#1403.1.1 = {
	monarch = {
		name = "Gian"
		ADM = 2
		DIP = 4
		MIL = 2
	}
}

1404.9.12 = {
	monarch = {
		name = "Pandolf"
		ADM = 3
		DIP = 2
		MIL = 3
	}
}
