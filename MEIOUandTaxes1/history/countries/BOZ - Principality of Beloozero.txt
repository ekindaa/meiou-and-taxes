# BOZ - Principality of Beloozero

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = novgorodian
religion = orthodox
technology_group = eastern
capital = 291	# Beloozero

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}
1338.1.1 = {
	monarch = {
		name = "Fedor Romanovich"
		dynasty = "Rurikovich"
		ADM = 3
		DIP = 2
		MIL = 4
	}
}

1380.11.21 = {
	monarch = {
		name = "Iurii Vasil'evich"
		dynasty = "Rurikovich"
		ADM = 3
		DIP = 2
		MIL = 4
	}
}

# 1389.1.1 - Inherited by Principality of Moskwa
