
government = constitutional_republic government_rank = 1
mercantilism = 0.0
religion = catholic
primary_culture = canadian
technology_group = western
capital = 990	# Quebec city

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
	#set_variable = { which = "centralization_decentralization" value = -1 }
	add_absolutism = -100
	add_absolutism = 60
}
