# Principality of Kiev
# Tag : KIE
# 2010-jan-16 - FB - HT3 changes

government = despotic_monarchy government_rank = 1
mercantilism = 0.0
technology_group = eastern
religion = orthodox
primary_culture = ruthenian
capital = 280	# Kiev

1000.1.1 = {
	add_country_modifier = { name = title_4 duration = -1 }
	set_country_flag = title_4
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 10
}

1331.1.1 = {
	monarch = {
		name = "Fiodor" # His pagan name is unknown
		dynasty = "Gediminai"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
# Fiodor of Kiev. Brother of Gediminas of Lithuania.
# Vassal of the Horde

1362.1.1 = {
	monarch = {
		name = "Vladimiras"
		dynasty = "Gediminai"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
# Vladimiras Algirdaitis, son of Algirdas of Lithuania
# Vassal of Lithuania after 1362. He is deposed in 1394 by Vytautas and his brother Skirgaila.

1395.1.1 = {
	monarch = {
		name = "Skirgaila"
		dynasty = "Gediminai"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
# Skirgaila Algirdaitis, another son of Algirdas

1397.1.1 = {
	monarch = {
		name = "Ivan"
		dynasty = "Olshansky" # Jonas Alseniskis
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
# A loyal companion of Vytautas

1404.1.1 = {
	monarch = {
		name = "Jerzy"
		dynasty = "Giedygold" # Jurgis Gedgaudas
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1412.1.1 = {
	monarch = {
		name = "Andriy Ivanovych"
		dynasty = "Olshansky"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1422.1.1 = {
	monarch = {
		name = "Mykhailo Ivanovych"
		dynasty = "Olshansky"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1433.1.1 = {
	monarch = {
		name = "Mykhailo Semenovych"
		dynasty = "Olshansky"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1435.1.1 = {
	monarch = {
		name = "Svitrigaila"
		dynasty = "Gediminai" # Grand Duke of Lithuania 1430-1432
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1443.1.1 = {
	monarch = {
		name = "Olelko Volodymyrovych" # Aleksandras Olelka
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1454.1.1 = {
	monarch = {
		name = "Semen Olelkovych" # Simonas Olelkaitis
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
