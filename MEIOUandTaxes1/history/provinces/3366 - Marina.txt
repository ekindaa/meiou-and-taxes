# 3366 - Onteniente + Alcoy

owner = ARA
controller = ARA
add_core = ARA

capital = "Alcoy"
trade_goods = fish
culture = catalan # valencian
religion = catholic

hre = no

base_tax = 4
base_production = 1
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes
town_hall = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	road_network = no
	paved_road_network = yes
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castille
1713.4.11 = {
	remove_core = CAS
}
1808.6.6 = {
	controller = REB
}
1811.1.1 = {
	controller = SPA
}
1812.10.1 = {
	controller = REB
}
1813.12.11 = {
	controller = SPA
}
