# 2334 - Gen�ve

owner = GNV
controller = GNV
add_core = GNV

capital = "Gen�ve"
trade_goods = livestock
culture = arpitan
religion = catholic

hre = yes

base_tax = 1
base_production = 0
base_manpower = 0

is_city = yes
temple = yes
town_hall = yes
marketplace = yes
workshop = yes
road_network = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim

450.1.1 = {
	set_province_flag = pocket_province
	set_province_flag = has_confluence
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "geneva_lake"
		duration = -1
	}
}
1356.1.1 = {
	set_variable = { which = starting_rural_pop value = 14.831 }
	set_variable = { which = starting_urban_pop value = 6.043 }
}
1500.1.1 = {
	road_network = yes
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 1
	base_production = 1
	base_manpower = 0
}
1523.1.1 = {
	religion = reformed
	#reformation_center = reformed
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
}
1618.1.1 = {
	hre = no
}
1653.1.1 = {
	unrest = 5
} # Peasant rebellion against overtaxation
1654.1.1 = {
	unrest = 0
}
1656.1.1 = {
	unrest = 7
} # Protestants are expelled from Schwyz which lead to the First War of Villmergen
1657.1.1 = {
	unrest = 2
} # An agreement is concluded at Villmergren but tensions remain
1798.3.5 = {
	controller = FRA
} # French occupation
1798.4.12 = {
	owner = SWI
	controller = SWI
	add_core = SWI
	hre = no
} # The establishment of the Helvetic Republic
1798.4.15 = {
	controller = REB
} # The Nidwalden Revolt
1798.9.1 = {
	controller = SWI
} # The revolt is supressed
1802.6.1 = {
	controller = REB
} # Swiss rebellion
1802.9.18 = {
	controller = SWI
}
