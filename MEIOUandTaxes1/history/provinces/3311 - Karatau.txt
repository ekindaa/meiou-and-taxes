# No previous file for Karatau

owner = MGH
controller = MGH
add_core = MGH

capital = "Taraz"
trade_goods = wool
culture = chaghatai
religion = sunni

base_tax = 5
base_production = 1
base_manpower = 1

is_city = yes
town_hall = yes

discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1356.1.1 = {
	add_core = KZH
}
1370.4.1 = {
	owner = TIM
	controller = TIM
	add_core = TIM
	remove_core = MGH
}
1446.1.1 = {
	owner = SHY
	controller = SHY
	culture = uzbehk
	add_core = SHY
	remove_core = TIM
}
#1465.1.1 = {
#	owner = KZH
#	controller = KZH
#}
1501.1.1 = {
	base_tax = 7
}
1502.1.1 = {
	owner = SHY
	controller = SHY
}
1520.1.1 = {
	owner = BUK
	controller = BUK
	add_core = BUK
	remove_core = SHY
} # Emirate of Bukhara established
1709.1.1 = {
	owner = KOK
	controller = KOK
	add_core = KOK
	remove_core = BUK
}
