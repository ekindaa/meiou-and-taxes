# 1349 - Baden-Durlach

owner = BAD
controller = BAD
add_core = BAD
capital = "Pforzheim"
trade_goods = wine
culture = eastfranconian
religion = catholic

hre = yes

base_tax = 7
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes
#road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 8
}
1538.1.1 = {
	religion = protestant
} # Protestant majority
1650.1.1 = {
	religion = catholic
}
1792.10.3 = {
	controller = FRA
} # Occupied by French troops
1796.8.7 = {
	controller = BAD
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
