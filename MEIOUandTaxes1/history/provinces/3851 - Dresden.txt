# No previous file for Dresden

owner = MEI
controller = MEI
add_core = MEI

capital = "Dresden"
trade_goods = lumber
culture = high_saxon
religion = catholic

hre = yes

base_tax = 10
base_production = 1
base_manpower = 1

is_city = yes
town_hall = yes
marketplace = yes
local_fortification_1 = yes
road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

#450.1.1 = {
#	add_permanent_province_modifier = {
#		name = "elbe_navigable_river"
#		duration = -1
#	}
#}
1423.6.1 = {
	owner = SAX
	controller = SAX
	add_core = SAX
	remove_core = MEI
}#Margraviate of Meissen inherits Saxony and becomes the Elector.
1485.11.11 = {
	owner = MEI
	controller = MEI
	add_core = MEI
	remove_core = SAX
} #Treaty of Leipzig
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 12
	base_production = 1
	base_manpower = 1
}
1520.12.10 = {
	religion = protestant
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1547.5.19 = {
	owner = SAX
	controller = SAX
	add_core = SAX
	remove_core = MEI
} #Treaty of Wittenberg
1560.1.1 = {
	fort_15th = yes
}
1790.8.1 = {
	unrest = 5
} # Peasant revolt
1791.1.1 = {
	unrest = 0
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved