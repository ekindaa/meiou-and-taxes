# No previous file for Rijeka

owner = FIU
controller = FIU
add_core = FIU

capital = "Rijeka"
trade_goods = wine
culture = dalmatian
religion = catholic

hre = yes

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes
temple = yes
harbour_infrastructure_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_claim = CRO
	#add_claim = VEN
	#add_claim = HAB
	rename_capital = "Fiume"
	change_province_name = "Fiume"
}
1466.1.1 = {
	owner = STY
	controller = STY
	add_core = STY
	remove_claim = CRO
} # bought by Friedrich III von Habsburg
1490.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = STY
}
1500.1.1 = {
	road_network = yes
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
}
# 1719 - declared a free port
1805.1.1 = {
	controller = FRA
}
1805.12.26 = {
	controller = HAB
} # Treaty of Pressburg
1809.8.1 = {
	controller = FRA
}
1809.10.14 = {
	owner = FRA
	add_core = FRA
}
1813.9.20 = {
	controller = HAB
} # Occupied by Austrian forces
1814.4.6 = {
	owner = HAB
	remove_core = FRA
} # Napoleon abdicates
