# 2152 - Saamod

capital = "Taymyr"
trade_goods = unknown
culture = nenet
religion = tengri_pagan_reformed

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 50
native_ferocity = 2
native_hostileness = 1

450.1.1 = {
	set_province_flag = tribals_control_province
}
1750.1.1 = {
	discovered_by = RUS
	owner = RUS
	controller = RUS
	citysize = 10
#	culture = russian
#	religion = orthodox
	trade_goods = fur
	rename_capital = "Dudinka"
	change_province_name = "Khantayskoye"
	set_province_flag = trade_good_set
}
