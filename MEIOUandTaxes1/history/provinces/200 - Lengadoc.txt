# 200 - Languedoc
#Principal cities: N�mes

owner = FRA
controller = FRA
add_core = FRA

capital = "Nemze"
trade_goods = wine
culture = provencal
religion = catholic

hre = no

base_tax = 13
base_production = 1
base_manpower = 1

is_city = yes
local_fortification_1 = yes
road_network = yes
town_hall = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech

450.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_cloth
		duration = -1
	}
	add_permanent_province_modifier = {
		name = "lack_of_harbour"
		duration = -1
	}
	set_province_flag = mined_goods
	set_province_flag = salt
}
1356.1.1 = {
	add_core = TOU
}
1422.10.21 = {
	owner = DAU
	controller = DAU
	add_core = DAU
	remove_core = FRA
}
1429.7.17 = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = DAU
}
1520.5.5 = {
	base_tax = 16
	base_production = 1
	base_manpower = 1
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
}

# La Cath�drale Saint-Pierre de Montpellier
1545.1.1 = {
	fort_14th = yes
}
1560.1.1 = {
	religion = reformed
}
1565.1.1 = {
	unrest = 8
} # France is restless once again as ultra-catholic intentions become clear
1568.9.1 = {
	unrest = 15
} # Catherine de Medici and Charles IX side with the Guise faction, religious intolerance peaks
1570.8.8 = {
	unrest = 10
} # Edict of Saint-Germain: temporary pacification
1573.9.1 = {
	unrest = 15
} # Saint Barthelew's Day Massacre: the consequences in the land
1574.5.1 = {
	unrest = 7
} # Charles IX dies, situation cools a bit
1584.1.1 = {
	unrest = 12
} # Situation heats up again
1588.12.1 = {
	unrest = 15
} # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1 = {
	unrest = 10
} # 'Paris vaut bien une messe!', Henri converts to Catholicism
1598.4.13 = {
	unrest = 3
} # Edict of Nantes, a lot more freedom to the protestants
1598.5.2 = {
	unrest = 0
} # Peace of Vervins, formal end to the Wars of Religion

# Henri IV's quest to eliminate corruption and establish state control
1635.1.1 = {
	fort_14th = no
	fort_15th = yes
	unrest = 3
}
1637.1.1 = {
	unrest = 0
}
1639.1.1 = {
	unrest = 3
}
1641.1.1 = {
	unrest = 0
}
1644.1.1 = {
	unrest = 3
}
1647.1.1 = {
	unrest = 0
}
1650.1.14 = {
	unrest = 7
} # Mazarin arrests the Princes Cond�, Conti & Longueville, the beginning of the Second Fronde
1651.4.1 = {
	unrest = 4
} # An unstable peace is concluded
1651.12.1 = {
	unrest = 7
} # Mazarin returns from exile, Cond� sides with Spain, situation heats up again
1652.10.21 = {
	unrest = 0
} # The King is allowed to enter Paris again, Mazarin leaves France for good. Second Fronde over.
1685.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1685.10.18 = {
	unrest = 8
} # Edict of Nantes revoked by Louis XIV
1686.1.17 = {
	religion = catholic
} # Dragonnard campaign successful: region reverts back to catholicism
1689.1.1 = {
	unrest = 0
} # War of the Grand Alliance erupts: Louis XIV can't pursue his religious policy anymore
1730.1.1 = {
	fort_16th = no
	fort_17th = yes
}
