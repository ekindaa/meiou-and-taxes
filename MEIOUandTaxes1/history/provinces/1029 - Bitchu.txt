# 1029 - Bitchu
# GG/LS - Japanese Civil War

owner = HKW
controller = HKW
add_core = HKW

capital = "Matuyama"
trade_goods = rice #linen
culture = chugoku
religion = mahayana #shinbutsu

hre = no

base_tax = 12
base_production = 0
base_manpower = 1

is_city = yes

discovered_by = chinese

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}
1356.1.1 = {
	add_core = UKI
}
1501.1.1 = {
	base_tax = 20
	base_production = 1
	base_manpower = 2
}
1525.1.1 = {
	owner = UKI
	controller = UKI
	add_core = MRI
}
1542.1.1 = {
	discovered_by = POR
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
