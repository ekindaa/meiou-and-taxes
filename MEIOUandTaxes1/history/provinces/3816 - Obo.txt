# 3816 - Obo

capital = "Obo"
trade_goods = sugar
culture = bakongo
religion = animism

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 2
native_hostileness = 7

450.1.1 = {
	set_province_flag = tribals_control_province
}
1356.1.1 = {
	add_permanent_province_modifier = {
		name = trading_post_province
		duration = -1
	}
}
1472.12.21 = {
	discovered_by = POR
	owner = POR
	controller = POR
	add_core = POR
	culture = portugese
	religion = catholic
	citysize = 500
	capital = "S�o Tom�"
	naval_arsenal = yes
	marketplace = yes
	fort_14th = yes
}
1550.1.1 = {
	is_city = yes
}
