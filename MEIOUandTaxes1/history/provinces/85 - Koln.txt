# 85 - Cologne

owner = FRK
controller = FRK
add_core = FRK

capital = "K�ln"
trade_goods = wheat #steel
culture = ripuarianfranconian
religion = catholic

hre = yes

base_tax = 2
base_production = 5
base_manpower = 0

is_city = yes
local_fortification_1 = yes
merchant_guild = yes
urban_infrastructure_1 = yes
workshop = yes
road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

# First Printer opens in K�ln
500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_steel
		duration = -1
	}
	set_province_flag = pocket_province
}
1388.1.1 = {
	small_university = yes
}
1520.1.1 = {
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 4
	base_production = 4
	base_manpower = 0
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
	weapons = yes
}
1553.1.1 = { } # Stock Exchange Founded
1638.1.1 = { } # K�ln manages to stay neutral in the 30 years war and prospers through weapon sales.
1716.1.1 = {
	refinery = yes
	weapons = no
} # Farnia begins exporting "Eau de Cologne"
1801.2.9 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Treaty of Lun�ville
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1815.6.9 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = FRA
} # Congress of Vienna
