# 2451 - County of Loon

owner = LIE
controller = LIE
add_core = LIE

capital = "Hasselt"
trade_goods = wheat #beer
culture = brabantian
religion = catholic

hre = yes

base_tax = 2
base_production = 1
base_manpower = 0

is_city = yes
town_hall = yes
workshop = yes
local_fortification_1 = yes
road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

#500.1.1 = {
#	add_permanent_province_modifier = {
#		name = urban_goods_beer
#		duration = -1
#	}
#}
1300.1.1 = {
	set_variable = { which = starting_rural_pop value = 27.320 }
	set_variable = { which = starting_urban_pop value = 10.145 }
}
1350.1.1 = {
	set_variable = { which = starting_rural_pop value = 27.070 }
	set_variable = { which = starting_urban_pop value = 10.251 }
}
1400.1.1 = {
	set_variable = { which = starting_rural_pop value = 7.635 }
	set_variable = { which = starting_urban_pop value = 16.251 }
}
1450.1.1 = {
	set_variable = { which = starting_rural_pop value = 18.856 }
	set_variable = { which = starting_urban_pop value = 16.012 }
}
1453.1.1 = {
	add_core = BUR
}
1465.1.1 = {
	unrest = 4
} # Revolt imminent
1465.4.22 = {
	controller = REB
} # Citizens revolt
1465.10.19 = {
	controller = LIE
	unrest = 0
} # Peace is restored
1467.1.1 = {
	owner = BUR
	controller = BUR
	unrest = 5
} # Charles the Bold installs Louis de Bourbon
1468.9.1 = {
	controller = REB
} # Citizens rise up against the disliked Louis de Bourbon
1468.9.4 = {
	controller = BUR
} # Charles the Bold sacks Li�ge
1477.1.5 = {
	owner = LIE
	controller = LIE
	remove_core = BUR
	add_core = HAB
	unrest = 0
} # Charles the Bold dies and Li�ge is re-established
1492.8.12 = {
	remove_core = HAB
} # Li�ge signs a perpetual treaty of neutrality with Austria (and France)
1500.1.1 = {
	set_variable = { which = starting_rural_pop value = 19.570 }
	set_variable = { which = starting_urban_pop value = 17.765 }
	road_network = yes
}
1518.1.1 = {
	fort_14th = yes
}
# Saint Paul's Cathedral finished
1520.5.5 = {
	base_tax = 4
	base_production = 2
	base_manpower = 0
}
1550.1.1 = {
	set_variable = { which = starting_rural_pop value = 39.142 }
	set_variable = { which = starting_urban_pop value = 16.251 }
}
1579.1.6 = {
	add_core = EBU
}
# Li�ge was always a centre of arms production
1600.1.1 = {
	set_variable = { which = starting_rural_pop value = 53.913 }
	set_variable = { which = starting_urban_pop value = 20.141 }
}
1650.1.1 = {
	set_variable = { which = starting_rural_pop value = 68.978 }
	set_variable = { which = starting_urban_pop value = 19.512 }
	fort_14th = no
	fort_15th = yes
}
1700.1.1 = {
	set_variable = { which = starting_rural_pop value = 101.217 }
	set_variable = { which = starting_urban_pop value = 19.123 }
}
1715.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1750.1.1 = {
	set_variable = { which = starting_rural_pop value = 165.252 }
	set_variable = { which = starting_urban_pop value = 19.422 }
}
1789.12.3 = {
	controller = REB
	add_core = EBU
}
1790.1.11 = {
	owner = EBU
	controller = EBU
	remove_core = LIE
}
1791.1.1 = {
	owner = HAB
	controller = HAB
}
1797.12.26 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Treaty of Campo Formio
1800.1.1 = {
	set_variable = { which = starting_rural_pop value = 261.374 }
	set_variable = { which = starting_urban_pop value = 25.124 }
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1815.3.16 = {
	owner = NED
	controller = NED
	add_core = NED
	remove_core = FRA
} # The United Kingdom of the Netherlands
1830.1.1 = {
	owner = EBU
	controller = EBU
}
1850.1.1 = {
	set_variable = { which = starting_rural_pop value = 148.613 }
	set_variable = { which = starting_urban_pop value = 35.239 }
}
