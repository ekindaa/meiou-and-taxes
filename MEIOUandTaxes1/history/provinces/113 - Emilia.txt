# 113 - Emilia
# Bologna

owner = PAP
controller = PAP

capital = "Bul�ggna"
trade_goods = hemp
culture = emilian
religion = catholic

hre = no

base_tax = 8
base_production = 5
base_manpower = 1

is_city = yes
local_fortification_1 = yes
temple = yes
urban_infrastructure_1 = yes
road_network = yes
workshop = yes
medium_university = yes	# Founded 1088 # Completed 1135

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_paper
		duration = -1
	}
}
1309.1.1 = {
	owner = PA2
	controller = PA2
}
1348.1.1 = {
	owner = MLO
	controller = MLO
	add_core = MLO
}
#1355.1.1 = {
#	owner = PA2
#	controller = PA2
#}
1356.1.10 = {
	add_core = BLG
	owner = BLG
	controller = BLG
	add_core = PA2
}
1360.1.1 = {
	owner = PA2
	controller = PA2
	add_core = PA2
}
1492.1.1 = {
	remove_core = BLG
}
1503.9.1 = {
	revolt = {
		type = anti_tax_rebels
		size = 0
	}
	controller = REB
	remove_core = PA2
} # Loss of Papal authority after the death of Alexander III, Venetian influence
1506.1.1 = {
	revolt = { }
	owner = PAP
	controller = PAP
}
1520.5.5 = {
	base_tax = 10
	base_production = 6
	base_manpower = 1
	fort_14th = yes
	art_corporation = yes # Bolognese School by 1500
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
	remove_core = MLO
}
1796.11.15 = {
	owner = ITD
	controller = ITD
	add_core = ITD
	remove_core = HAB
} # Cispadane Republic
1797.6.29 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = ITD
} # Cisalpine Republic
1814.4.11 = {
	owner = PAP
	controller = PAP
	add_core = PAP
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1860.3.20 = {
	owner = SPI
	controller = SPI
	add_core = SPI
	remove_core = PAP
}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
