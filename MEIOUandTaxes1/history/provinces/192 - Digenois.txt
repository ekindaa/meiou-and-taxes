# 192 Bourgogne - Principal cities: Dijon

owner = BUR
controller = BUR
add_core = BUR

capital = "Dyjon"
trade_goods = wine
culture = bourguignon
religion = catholic

hre = no

base_tax = 22
base_production = 2
base_manpower = 2

is_city = yes
marketplace = yes
town_hall = yes
temple = yes # the Abbey of Cluny
fort_14th = yes
road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1 = {
	add_core = FRA
}
1444.1.1 = {
	remove_core = FRA
}
1477.1.5 = {
	add_core = FRA
}
1482.3.27 = {
	owner = FRA
	controller = FRA
	add_core = HAB
} # Charles the Bold dies and transfers Bourgogne to France
1520.5.5 = {
	base_tax = 27
}
1525.1.1 = {
	fort_14th = yes
}
1526.1.14 = {
	add_core = HAB
} # Treaty of Madrid, Austria claims Burgundy, but Fran�ois I refuses to hand it over subsequently
1529.8.3 = {
	remove_core = HAB
} # Peace of Cambrai (Ladies' Peace): Charles V renounces his claims on Bourgogne
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1588.12.1 = {
	unrest = 5
} # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1 = {
	unrest = 0
} # 'Paris vaut bien une messe!', Henri converts to Catholicism
# Henri IV's quest to eliminate corruption and establish state control
1630.1.1 = {
	fort_14th = no
	fort_15th = yes
	unrest = 3
}
1632.1.1 = {
	unrest = 0
}
1636.8.30 = {
	controller = HAB
} # Habsburg forces ravage the region in the Thirty Years War
1636.10.1 = {
	controller = FRA
} # Bernhard of Saxe-Weimar defeats the invaders and gradually pushes them back
1650.1.14 = {
	unrest = 7
} # Mazarin arrests the Princes Cond�, Conti & Longueville, the beginning of the Second Fronde
1651.4.1 = {
	unrest = 4
} # An unstable peace is concluded
1651.12.1 = {
	unrest = 7
} # Mazarin returns from exile, Cond� sides with Spain, situation heats up again
1652.10.21 = {
	unrest = 0
} # The King is allowed to enter Paris again, Mazarin leaves France for good. Second Fronde over.
1685.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1750.1.1 = {
	fort_16th = no
	fort_17th = yes
}
