#252 - Highlands

owner = SCO
controller = SCO
add_core = SCO

capital = "Inbhir Aora" # Inveraray
trade_goods = livestock
culture = highland_scottish
religion = catholic

hre = no

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

#1356.1.1 = {
#	#add_core = HIG
#}
1350.1.1 = {
	set_variable = { which = starting_rural_pop value = 41.901 }
	set_variable = { which = starting_urban_pop value = 265 }
}
1520.5.5 = {
	base_tax = 5
	base_production = 0
	base_manpower = 0
}
1560.8.1 = {
	religion = reformed
}
1644.1.1 = {
	controller = REB
}
1647.1.1 = {
	controller = SCO
} #Estimated
1689.3.1 = {
	controller = REB
} #Jacobite Rising
1689.11.1 = {
	controller = SCO
}
1690.1.1 = {
	fort_14th = yes
}
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
 #marketplace, Regimental Camp Estimated
