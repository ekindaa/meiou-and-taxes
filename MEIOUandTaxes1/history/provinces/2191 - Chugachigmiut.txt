# No previous file for Chugachigmiut

capital = "Chugachigmiut"
trade_goods = unknown
culture = tanaina
religion = totemism

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 3
native_hostileness = 8

450.1.1 = {
	set_province_flag = tribals_control_province
}
1741.1.1 = {
	discovered_by = RUS
} # Vitus Bering
1778.1.1 = {
	discovered_by = GBR
} # James Cook
1786.1.1 = {
	owner = RUS
	controller = RUS
	citysize = 840
#	culture = russian
#	religion = orthodox
	trade_goods = fur
} # Russian settlement founded by Grigori Shelekho
1809.1.1 = {
	add_core = RUS
}
1867.1.1 = {
	owner = USA
	controller = USA
}
