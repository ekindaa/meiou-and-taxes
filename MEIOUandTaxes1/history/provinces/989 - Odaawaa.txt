# No previous file for Odaawaa

owner = OTW
controller = OTW
add_core = OTW

capital = "Odaawaa"
trade_goods = fur
culture = odawa
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes

native_size = 20
native_ferocity = 1
native_hostileness = 6

450.1.1 = {
	set_province_flag = tribals_control_province
}