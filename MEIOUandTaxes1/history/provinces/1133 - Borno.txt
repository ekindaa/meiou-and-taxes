# 1133 - Borno

owner = KBO
controller = KBO
add_core = KBO

capital = "Ngazargamu"
trade_goods = cotton
culture = kanouri
religion = sunni

hre = no

base_tax = 8
base_production = 2
base_manpower = 0

is_city = yes
urban_infrastructure_1 = yes
marketplace = yes
workshop = yes
local_fortification_1 = yes

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 4 }
	set_variable = { which = settlement_score_progress_preset	value = 63 }
}