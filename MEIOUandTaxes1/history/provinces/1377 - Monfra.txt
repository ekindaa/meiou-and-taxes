# 1377 - Monfr�

owner = MFT
controller = MFT
add_core = MFT

capital = "Monfr�"
trade_goods = wine
culture = piedmontese
religion = catholic

hre = yes

base_tax = 8
base_production = 0
base_manpower = 0

is_city = yes
temple = yes
road_network = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim

1520.5.5 = {
	base_tax = 10
	base_production = 0
	base_manpower = 0
}
1530.2.27 = {
	hre = no
}
1533.1.1 = {
	owner = SPA
	controller = SPA
}
1536.1.1 = {
	owner = MAN
	controller = MAN
	add_core = MAN
	add_core = SAV
}
1600.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1618.1.1 = {
	hre = no
}
1700.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1708.6.30 = {
	controller = SAV
	owner = SAV
	remove_core = MAN
	remove_core = MFT
}
1713.4.11 = {
	owner = SIC
	controller = SIC
	add_core = SIC
} # Treaty of Utrecht(2) gave Sicily to House of Savoy
1718.8.2 = {
	owner = SPI
	controller = SPI
	add_core = SPI
	remove_core = SIC
} # Kingdom of Piedmont-Sardinia
1792.9.21 = {
	controller = FRA
} # Conquered by the French
1796.4.25 = {
	owner = FRA
	add_core = FRA
} # The Republic of Alba
1814.4.11 = {
	owner = SPI
	controller = SPI
	remove_core = FRA
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
