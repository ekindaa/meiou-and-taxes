# 2288 - Totomi
# GG/LS - Japanese Civil War

owner = IGW
controller = IGW
add_core = IGW

capital = "Famamatu"
trade_goods = fish
culture = chubu
religion = mahayana #shinbutsu

hre = no

base_tax = 13
base_production = 0
base_manpower = 1

is_city = yes
harbour_infrastructure_1 = yes
road_network = yes

discovered_by = chinese

1501.1.1 = {
	base_tax = 22
	base_manpower = 2
}
1525.1.1 = {
	fort_14th = yes
} # Nagoya Castle
1542.1.1 = {
	discovered_by = POR
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
