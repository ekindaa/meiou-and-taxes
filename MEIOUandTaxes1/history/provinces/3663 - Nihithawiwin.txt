# No previous file for Nihithawiwin

capital = "Nihithawiwin"
trade_goods = unknown
culture = cree
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 45
native_ferocity = 2
native_hostileness = 5

450.1.1 = {
	set_province_flag = tribals_control_province
}
1692.1.1 = {
	discovered_by = ENG
} # Henry Kelsey
1707.5.12 = {
	discovered_by = GBR
}
1790.1.1 = {
	owner = GBR
	controller = GBR
	culture = english
	religion = protestant
	citysize = 200
	trade_goods = fur
} # Split Lake House, later Norway House
1815.1.1 = {
	add_core = GBR
}
