# 2281 - Kii
# GG/LS - Japanese Civil War

owner = JAP
controller = JAP
add_core = JAP

capital = "Wakayama"
trade_goods = livestock
culture = kansai
religion = mahayana #shinbutsu

hre = no

base_tax = 19
base_production = 0
base_manpower = 2

is_city = yes

discovered_by = chinese

1356.1.1 = {
	add_core = HKY
}
1477.1.1 = {
	revolt = {
		type = ikko_ikki_rebels
		size = 1
	}
	controller = REB
}
1501.1.1 = {
	base_tax = 33
	base_manpower = 3
}
1525.1.1 = {
	fort_14th = yes
}
1542.1.1 = {
	discovered_by = POR
}
1545.1.1 = {
	revolt = { }
	owner = ODA
	controller = ODA
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
