# 2183 - Maliseet

capital = "Maliseet"
trade_goods = unknown
culture = miqmaq
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 10
native_ferocity = 1
native_hostileness = 3

450.1.1 = {
	set_province_flag = tribals_control_province
}
1609.1.1 = { } # Samuel de Champlain
1650.1.1 = {
	owner = ABE
	controller = ABE
	add_core = ABE
	is_city = yes
	trade_goods = fur
} # Extent of the Abenaki/Wabanaki c. the Beaver Wars
