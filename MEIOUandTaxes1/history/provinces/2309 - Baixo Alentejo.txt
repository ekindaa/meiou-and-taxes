# 2309 - Baixo Alentejo

owner = POR
controller = POR
add_core = POR

capital = "Beja"
trade_goods = wheat	#could be copper or lead
culture = portugese
religion = catholic

hre = no

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech
# estate order militaro

500.1.1 = {
	add_permanent_province_modifier = {
		name = "lack_of_harbour"
		duration = -1
	}
}
1249.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = copper
}
1372.5.5 = {
	unrest = 1
} # Marriage between King Ferdinand and D. Leonor de Menezes that brought civil unrest and revolt.
1373.5.5 = {
	unrest = 0
} # Civil unrest repressed.
1382.1.1 = {
	controller = CAS
} # Third Fernandine War
1382.12.31 = {
	controller = POR
}
1384.1.1 = {
	controller = CAS
} # Portuguese Interregnum
1384.9.3 = {
	controller = POR
} # CAS retreats.
1420.1.1 = {
	base_tax = 2
	base_production = 1
}
1500.3.3 = {
	base_tax = 5
	base_production = 1
	base_manpower = 0
}
1515.1.1 = {
	road_network = no
	paved_road_network = yes
}
1580.8.25 = {
	controller = SPA
}
1580.8.26 = {
	controller = POR
}
1640.1.1 = {
	unrest = 8
} # Revolt headed by John of Bragan�a
1640.12.1 = {
	unrest = 0
}
1807.11.30 = {
	controller = FRA
} # Occupied by France
1808.8.30 = {
	controller = POR
}
1810.7.25 = {
	controller = FRA
} # Invaded after the battle of C�a
1811.1.1 = {
	controller = POR
} # The Napoleonic army retreats from Portugal
