# 653 - Davao

capital = "Butuan"
trade_goods = unknown
culture = maguindanao
religion = hinduism

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 10
native_ferocity = 3
native_hostileness = 9

discovered_by = chinese
discovered_by = austranesian

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "maguindanae_natural_harbour"
		duration = -1
	}
}
1501.1.1 = {
	base_tax = 2
	native_size = 20
}
1515.1.1 = {
	discovered_by = MGD
	owner = MGD
	controller = MGD
	add_core = MGD
	religion = sunni
	is_city = yes
	trade_goods = gold
	set_province_flag = trade_good_set
} # Sultanate of Maguindanao
1527.1.1 = {
	discovered_by = SPA
}
1847.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
	unrest = 1
}
