# 1014 - Hokuzan (Northern Mountain)
# GG/LS - Japanese Civil War

owner = RYU
controller = RYU
add_core = RYU

capital = "Amami"
trade_goods = fish #chinaware
culture = ryukyuan
religion = animism

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes

discovered_by = chinese

1501.1.1 = {
	base_tax = 4
}
1542.1.1 = {
	discovered_by = POR
}
1609.1.1 = {
	controller = SMZ
} # Occupation de Satsuma, King Sho Nei was taken prisoner
1611.1.1 = {
	controller = RYU
} # Sho Nei is released
1624.1.1 = {
	owner = SMZ
	controller = SMZ
	religion = animism
} # Annexion par les Shimazu de Satsuma
1650.1.1 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
