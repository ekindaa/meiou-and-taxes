# No previous file for Krishnagiri

owner = GNG
controller = GNG
add_core = GNG

capital = "Krishnagiri"
trade_goods = rice
culture = tamil
religion = hinduism

hre = no

base_tax = 28
base_production = 0
base_manpower = 2

is_city = yes

discovered_by = indian
discovered_by = muslim

1356.1.1 = {
	owner = MAD
	controller = MAD
}
1378.1.1 = {
	owner = VIJ
	controller = VIJ
}
1428.1.1 = {
	add_core = VIJ
}
1498.1.1 = {
	discovered_by = POR
}
1511.1.1 = {
	base_tax = 36
}
1513.1.1 = {
	controller = VIJ
} # Substantial Vijayanagar expansion
1519.8.1 = {
	owner = VIJ
} # Substantial Vijayanagar expansion
1530.1.1 = {
	#owner = GNG
	#controller = GNG
	add_core = GNG
	#remove_core = VIJ
}
1530.3.17 = {
	marketplace = yes
}
1565.7.1 = {
	owner = GNG
	controller = GNG
	fort_14th = no
	fort_16th = yes
} # The Vijayanagar empire collapses, the Nayaks proclaimed themselves rulers
1649.1.1 = {
	controller = BIJ
} # Conquered by Bijapur
1649.12.1 = {
	owner = BIJ
} # Conquered by Bijapur
1685.1.1 = {
	controller = MUG
}
1686.1.1 = {
	owner = MUG
}
1710.1.1 = {
	owner = KRK
	controller = KRK
	add_core = KRK
} # Nawab of Arcot / Carnatic
1740.5.1 = {
	controller = MAR
} # Arcot falls to the Marathas
1743.3.1 = {
	controller = KRK
} # Arcot falls to the Nizam
1751.9.1 = {
	controller = GBR
} #Conquered by Clive
1754.1.1 = {
	controller = KRK
}
1761.1.1 = {
	controller = GBR
}
1764.1.1 = {
	controller = KRK
}
1801.1.1 = {
	owner = GBR
	controller = GBR
}
