# 2980 - Mafangano

owner = NDO
controller = NDO
add_core = NDO

capital = "Mafangano"
trade_goods = millet
culture = mbundu
religion = animism

hre = no

base_tax = 10
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = central_african

450.1.1 = {
	set_province_flag = tribals_control_province
	set_province_flag = mined_goods
	set_province_flag = silver
}
1628.1.1 = {
	discovered_by = POR
	add_core = POR
	owner = POR
	controller = POR
} # Effectively under Portuguese control
1670.1.1 = {
	revolt = {
		type = nationalist_rebels
		size = 1
		leader = "Filipe Hari"
	}
	controller = REB
}
1671.1.1 = {
	revolt = { }
	controller = POR
}
