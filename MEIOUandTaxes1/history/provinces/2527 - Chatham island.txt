# 2527 - Rapa Nui

culture = polynesian
religion = polynesian_religion
capital = "Rapa Nui"
trade_goods = unknown # fish

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 3
native_ferocity = 2
native_hostileness = 9

450.1.1 = {
	set_province_flag = tribals_control_province
	add_permanent_province_modifier = {
		name = remote_island
		duration = -1
	}
}
