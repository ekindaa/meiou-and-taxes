# 5231 - Saldanha

capital = "Saldanha"
trade_goods = unknown
culture = khoisan
religion = animism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 0.5
native_hostileness = 0

450.1.1 = {
	set_province_flag = tribals_control_province
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = great_natural_place
}
1488.1.1 = {
	discovered_by = POR
} # Bartolomeu Dias
1616.1.1 = {
	discovered_by = NED
} # Dirk Hartog
