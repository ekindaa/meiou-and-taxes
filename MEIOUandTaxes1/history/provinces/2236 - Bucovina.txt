# 2236 - Bukovina

owner = MOL
controller = MOL

capital = "Siret"
trade_goods = wine
culture = moldovian
religion = orthodox

hre = no

base_tax = 2
base_production = 2
base_manpower = 0

is_city = yes
town_hall = yes
workshop = yes
road_network = yes
marketplace = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1352.1.1 = {
	add_core = MOL
}
#1498.1.1 = {
#	add_core = TUR
#} # Bayezid II forces Stephen the Great to accept Ottoman suzereignty.
1520.5.5 = {
	base_tax = 3
	base_production = 2
	base_manpower = 0
}
1772.1.1 = {
	add_core = HAB
}
1775.1.1 = {
	owner = HAB
	controller = HAB
	remove_core = TUR
}
