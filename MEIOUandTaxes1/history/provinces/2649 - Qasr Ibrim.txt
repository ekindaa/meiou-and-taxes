# 2649 - Qasr Ibrim

owner = MKU
controller = MKU
add_core = MKU

capital = "Qasr Ibrim"
trade_goods = wheat
culture = nubian
religion = sunni

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = east_african
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

450.1.1 = {
	set_province_flag = tribals_control_province
}
# 1453.1.1 = {
#	controller = REB
#} # Under control of Awlad Kenz
1510.1.1 = {
	discovered_by = SEN
} # Funj replace Mamluks in control of Suakin
1516.1.1 = {
	add_core = TUR
} # Mamluks fall to Ottomans, Ottomans do not advance up Nile
1524.1.1 = {
	owner = MKU
	controller = MKU
	add_core = MKU
	#add_core = MAM
	remove_core = TUR
}
1540.1.1 = {
	owner = MAM
	controller = MAM
	capital = "Al Dirr"
} #Ottomans occupy Lower Nubia
1802.5.13 = {
	unrest = 8
} # Turkish rule is restored but a few troublesome years follow
1805.1.1 = {
	unrest = 0
	owner = EGY
	controller = EGY
}
1811.6.1 = {
	owner = TUR
	controller = TUR
} # Order is restored
