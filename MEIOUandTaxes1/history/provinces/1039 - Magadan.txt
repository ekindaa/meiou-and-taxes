# 1039 - Magadam

capital = "Verkhoyansk"
trade_goods = unknown
culture = eveni
religion = tengri_pagan_reformed

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 1
native_hostileness = 3

450.1.1 = {
	set_province_flag = tribals_control_province
}
1638.1.1 = {
	discovered_by = RUS
} # Semyon Dezhnev
1732.1.1 = {
	owner = RUS
	controller = RUS
	citysize = 350
#	culture = russian
#	religion = orthodox
	trade_goods = fur
	set_province_flag = trade_good_set
} # Conquered by Russia
1750.1.1 = {
	citysize = 870
}
1757.1.1 = {
	add_core = RUS
}
1782.1.1 = {
	is_city = yes
}
