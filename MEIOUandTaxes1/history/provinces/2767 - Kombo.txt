# 2767 - Kombo

owner = MAL
controller = MAL
add_core = MAL

capital = "Kombo"
trade_goods = rice
culture = mandinka
religion = west_african_pagan_reformed

hre = no

base_tax = 9
base_production = 0
base_manpower = 1

is_city = yes
local_fortification_1 = yes

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 9 }
	set_variable = { which = settlement_score_progress_preset	value = 78 }
}
1356.1.1 = {
	add_core = KAA
}
1520.1.1 = {
	base_tax = 14
}