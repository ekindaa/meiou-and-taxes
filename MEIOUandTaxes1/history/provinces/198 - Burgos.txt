# 198 - Burgos

owner = CAS #Juan II of Castille
controller = CAS
add_core = CAS

capital = "Burgos"
trade_goods = wheat
culture = castillian
religion = catholic

hre = no

base_tax = 9
base_production = 2
base_manpower = 2

is_city = yes
local_fortification_1 = yes
road_network = yes
urban_infrastructure_1 = yes
workshop = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_metalwork
		duration = -1
	}
}
1356.1.1 = {
	add_core = ENR
}
1360.1.1 = {
	controller = ENR
}
1369.3.23 = {
	remove_core = ENR
	controller = CAS
}
1464.5.1 = {
	unrest = 3
} #Nobiliary uprising against King Enrique, Castilla goes into anarchy
1468.9.18 = {
	unrest = 0
} #Pactos de los Toros de Guisando. Isabel of Castille becomes heir to the throne and a temporary peace is achieved
1470.1.1 = {
	unrest = 3
} #Isabel marries with Fernando of Aragon, breaking the Pacts of Guisando. King Enrique choses his daughter Juana ("La Beltraneja") as new heiress and a succession War erupts.
1479.9.4 = {
	unrest = 0
} #Peace of Alca�ovas, between Queen Isabel and King Alfonso of Portugal who had entered the war supporting her wife Juana
1500.3.3 = {
	base_tax = 11
	base_production = 2
	base_manpower = 2
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	road_network = no
	paved_road_network = yes
	trade_goods = iron
	weapons = yes
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1520.5.1 = {
	unrest = 5
} # War of the Comunidades
1521.4.1 = {
	unrest = 0
} # The army of the "Comuneros" is defeated at Villalar. Its leaders are promptly beheaded.
1713.4.11 = {
	remove_core = CAS
}
