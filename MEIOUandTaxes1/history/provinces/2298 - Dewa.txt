# 2298 - Dewa
# GG/LS - Japanese Civil War

owner = DTE
controller = DTE
add_core = DTE

capital = "Yamagata"
trade_goods = rice
culture = tohoku
religion = mahayana #shinbutsu

hre = no

base_tax = 17
base_production = 1
base_manpower = 1

is_city = yes
harbour_infrastructure_1 = yes

discovered_by = chinese

1501.1.1 = {
	base_tax = 24
	base_production = 6
	base_manpower = 3
}
1542.1.1 = {
	discovered_by = POR
}
1630.1.1 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
