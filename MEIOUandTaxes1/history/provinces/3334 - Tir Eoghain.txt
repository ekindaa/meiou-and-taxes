# No previous file for T�r Eoghain

owner = TYR
controller = TYR
add_core = TYR

capital = "D�n Geanainn" # Dungannon
trade_goods = livestock
culture = irish
religion = catholic

hre = no

base_tax = 7
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes
warehouse = yes
marketplace = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

700.1.1 = {
	add_permanent_province_modifier = {
		name = clan_land
		duration = -1
	}
}
1350.1.1 = {
	set_variable = { which = starting_rural_pop value = 75.155 }
	set_variable = { which = starting_urban_pop value = 6.762 }
}
1520.5.5 = {
	base_tax = 10
	base_production = 0
	base_manpower = 1
}
1607.9.4 = {
	owner = ENG
	controller = ENG
	add_core = ENG
	capital = "Belfast"
} # Flight of the Earls
1641.10.22 = {
	controller = REB
}
1642.5.1 = {
	controller = ENG
} # Estimated
1646.6.5 = {
	controller = IRE
}
1650.6.21 = {
	controller = ENG
} # Battle of Scarrifhollis
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
1798.5.23 = {
	controller = REB
} # Irish rebellion
1798.7.14 = {
	controller = GBR
} # The rebels are defeated
