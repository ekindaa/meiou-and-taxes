# No previous file for Ogulmiut

capital = "Ogulmiut"
trade_goods = unknown
culture = yupik
religion = animism

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 3
native_hostileness = 8

450.1.1 = {
	set_province_flag = tribals_control_province
}
1741.1.1 = {
	discovered_by = RUS
} # Vitus Bering
1750.1.1 = {
	base_tax = 1
}
1778.1.1 = {
	discovered_by = GBR
} # James Cook
1784.1.1 = {
	owner = RUS
	controller = RUS
	citysize = 650
#	culture = russian
#	religion = orthodox
	trade_goods = fur
}
1809.1.1 = {
	add_core = RUS
}
1867.1.1 = {
	owner = USA
	controller = USA
}
