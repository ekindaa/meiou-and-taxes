# 2590 - Nagoniara

capital = "Nagoniara"
trade_goods = unknown # fish
culture = melanesian
religion = polynesian_religion

hre = no

base_tax = 5
base_production = 0
base_manpower = 0

native_size = 45
native_ferocity = 2
native_hostileness = 9

450.1.1 = {
	set_province_flag = tribals_control_province
}
1568.1.1 = {
	discovered_by = SPA
} # Pedro Sarmiento de Gamboa
