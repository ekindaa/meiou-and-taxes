# 1858 - Belluno

owner = BOH
controller = BOH
add_core = BOH

capital = "Bel�n"
trade_goods = lumber
culture = venetian
religion = catholic

hre = yes

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes
road_network = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_claim = VER
}
1361.1.1 = {
	owner = PAD
	controller = PAD
	add_core = PAD
	remove_core = BOH
}
1404.1.1 = {
	owner = VEN
	controller = VEN
	add_core = VEN
	remove_claim = VER
}
1509.6.1 = {
	controller = FRA
	remove_core = PAD
} # Venice collapses
1512.1.1 = {
	controller = VEN
} # Brescia revolts
1512.2.18 = {
	controller = FRA
} # Sack of Brescia
1513.3.23 = {
	controller = VEN
	hre = no
}
1520.5.5 = {
	base_tax = 7
	base_production = 0
	base_manpower = 0
}
1618.1.1 = {
	hre = no
}
1797.10.17 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = VEN
} # Treaty of Campo Formio
1805.3.17 = {
	owner = ITE
	controller = ITE
	add_core = ITE
} # Merged with the Cisalpine Republic
1814.4.11 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1866.1.1 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
