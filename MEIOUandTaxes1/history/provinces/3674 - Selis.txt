# No previous file for Selis

capital = "Selis"
trade_goods = unknown
culture = salish
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 3
native_hostileness = 4

450.1.1 = {
	set_province_flag = tribals_control_province
}
1808.1.1 = {
	owner = GBR
	controller = GBR
	citysize = 350
	trade_goods = fur
	religion = protestant
	culture = american
} # Kootenae house
