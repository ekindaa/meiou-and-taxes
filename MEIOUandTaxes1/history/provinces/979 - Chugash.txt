# No previous file for Chugash

capital = "Chugash"
trade_goods = unknown
culture = yupik
religion = animism

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 5
native_ferocity = 1
native_hostileness = 6

450.1.1 = {
	set_province_flag = tribals_control_province
	add_permanent_province_modifier = {
		name = barren_island
		duration = -1
	}
}
1741.1.1 = {
	discovered_by = RUS
} # Vitus Bering
1778.1.1 = {
	discovered_by = GBR
} # James Cook
1784.1.1 = {
	owner = RUS
	controller = RUS
	add_core = RUS
#	culture = russian
#	religion = orthodox
	citysize = 840
	trade_goods = fur
}
1867.1.1 = {
	owner = USA
	controller = USA
}

