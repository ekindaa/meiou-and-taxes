# 2520 - Ascension

capital = "Ascension"
trade_goods = unknown # fish

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 0
native_ferocity = 0
native_hostileness = 0

450.1.1 = {
	add_permanent_province_modifier = {
		name = barren_island
		duration = -1
	}
}
1503.5.21 = {
	discovered_by = POR
}
