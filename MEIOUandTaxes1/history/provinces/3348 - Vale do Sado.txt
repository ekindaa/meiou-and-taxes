# 3339 - Vale do Sado

owner = POR
add_core = POR
controller = POR
capital = "Set�bal"
trade_goods = fish
culture = portugese
religion = catholic
base_tax = 5
base_production = 0
base_manpower = 0
is_city = yes
harbour_infrastructure_1 = yes
# estate order militaro
discovered_by = western
discovered_by = turkishtech
discovered_by = muslim
discovered_by = eastern

450.1.1 = {
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "lisboa_natural_harbour"
		duration = -1
	}
	set_province_flag = mined_goods
	set_province_flag = salt
}
1372.5.5 = {
	unrest = 6
} # Marriage between King Ferdinand and D. Leonor de Menezes that brought civil unrest and revolt.
1373.5.5 = {
	unrest = 0
} # Civil unrest repressed.
1420.1.1 = {
	base_tax = 3
}
1500.3.3 = {
	base_tax = 5
	base_production = 1
	base_manpower = 0
}
1515.1.1 = {
	road_network = no
	paved_road_network = yes
}
1522.3.20 = {
	naval_arsenal = yes
}
1580.8.25 = {
	controller = SPA
}
1580.8.26 = {
	controller = POR
}
1640.1.1 = {
	unrest = 7
} # Revolt headed by John of Bragan�a
1640.12.1 = {
	unrest = 0
}
1807.12.1 = {
	controller = FRA
} # Occupied by France
1808.8.30 = {
	controller = POR
}