#50 - Brandenburg

owner = BRA
controller = BRA
add_core = BRA

capital = "Berlin"
trade_goods = livestock
culture = low_saxon
religion = catholic

hre = yes

base_tax = 21
base_production = 2
base_manpower = 2

is_city = yes
marketplace = yes
workshop = yes
town_hall = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1417.1.1 = {
	capital = "Berlin"
}
1498.1.1 = {
	small_university = yes
} # University of Frankfurt on Oder
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 23
	base_production = 3
	base_manpower = 2
}
1525.1.1 = {
	fort_14th = yes
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1539.1.1 = {
	religion = protestant
}
1650.1.1 = {
	culture = prussian
}
1700.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1701.1.18 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = BRA
} # Friedrich III becomes king in Prussia
1725.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1775.1.1 = {
	fort_16th = no
	fort_17th = yes
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1806.10.27 = {
	controller = FRA
}
1807.7.9 = {
	controller = PRU
} # The Second treaty of Tilsit
