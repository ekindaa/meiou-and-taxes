# 3355 - Kawachi

owner = HKY
controller = HKY
add_core = HKY

capital = "Kawatinagano"
trade_goods = rice
culture = kansai
religion = mahayana

hre = no

base_tax = 8
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = chinese

1356.1.1 = {
	controller = JAP
}
1359.1.1 = {
	controller = HKY
}
1369.1.1 = {
	controller = JAP
} # Captured by Kusunoki Masanori until surrender ADD TAG
1382.1.1 = {
	owner = HKY
	controller = HKY
}
1501.1.1 = {
	base_tax = 13
	base_manpower = 1
}
1542.1.1 = {
	discovered_by = POR
}
1568.1.1 = {
	owner = MIY
	controller = MIY
}
1572.1.1 = {
	owner = ODA
	controller = ODA
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
