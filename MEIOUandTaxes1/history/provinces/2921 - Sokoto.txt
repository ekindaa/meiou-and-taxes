# 2921 - Sokoto

owner = KBI
controller = KBI
add_core = KBI

capital = "Sokoto"
trade_goods = wool
culture = haussa
religion = sunni

hre = no

base_tax = 8
base_production = 0
base_manpower = 0

is_city = yes
marketplace = yes
workshop = yes
urban_infrastructure_1 = yes
local_fortification_1 = yes

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 28 }
	set_variable = { which = settlement_score_progress_preset	value = 41 }
}