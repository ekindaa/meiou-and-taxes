# 1131 - Edo

owner = BEN
controller = BEN
add_core = BEN

capital = "Edo"
trade_goods = palm
culture = edo_c
religion = west_african_pagan_reformed

hre = no

base_tax = 9
base_production = 2
base_manpower = 0

is_city = yes
urban_infrastructure_1 = yes
marketplace = yes
workshop = yes
local_fortification_1 = yes

discovered_by = soudantech
discovered_by = sub_saharan

1520.1.1 = {
	base_tax = 8
}