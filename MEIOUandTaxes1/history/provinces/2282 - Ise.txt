# 2282 - Ise
# GG/LS - Japanese Civil War

owner = KTK
controller = KTK
add_core = KTK

capital = "Suzuka"
trade_goods = cotton
culture = kansai
religion = mahayana #shinbutsu

hre = no

base_tax = 15
base_production = 2
base_manpower = 2

is_city = yes
road_network = yes
town_hall = yes
workshop = yes
harbour_infrastructure_2 = yes
marketplace = yes
local_fortification_1 = yes

discovered_by = chinese

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}
1501.1.1 = {
	base_tax = 26
	base_production = 5
	base_manpower = 3
}
1525.1.1 = {
	owner = KTK
	controller = KTK
}
1542.1.1 = {
	discovered_by = POR
}
1572.1.1 = {
	owner = ODA
	controller = ODA
}
1583.1.1 = {
	owner = TGW
	controller = TGW
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
