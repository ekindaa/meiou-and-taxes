# 62 - Lienz

owner = GOR
controller = GOR
add_core = GOR

capital = "Lienz"
trade_goods = livestock
culture = austrian
religion = catholic

hre = yes

base_tax = 1
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = turkishtech
discovered_by = muslim

1356.1.1 = {
	add_core = HAB
}
1500.1.1 = {
	road_network = yes
}
1500.4.12 = {
	owner = HAB
	controller = HAB
}
1520.5.5 = {
	base_tax = 3
	base_production = 0
	base_manpower = 0
}
1525.3.1 = {
	unrest = 6
} # Peasant Revolts
1525.9.1 = {
	unrest = 0
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
