# 169 Bro Naozhon Bro Bro Dol - Principal cities: Rennes Dol

owner = BRI
controller = BRI
add_core = BRI

capital = "Rennes"
trade_goods = livestock
culture = breton
religion = catholic

hre = no

base_tax = 17
base_production = 1
base_manpower = 0

is_city = yes
local_fortification_3 = yes
temple = yes
workshop = yes
town_hall = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}
1341.4.30 = {
	owner = BLO
	controller = BLO
	add_core = MNF
	add_core = BLO
	remove_core = BRI
} # Jean III de Bretagne dies in Caen
1364.9.29 = {
	controller = MNF
} # Battle of Auray, Charles de Blois is killed
1365.4.12 = {
	owner = BRI
	controller = BRI
	add_core = BRI
	remove_core = BLO
	remove_core = MNF
} # End of the Brittany war of succession with the death of Charles de Blois
1378.1.1 = {
	add_core = FRA
} # Charles V invades Brittany without resistance
1520.5.5 = {
	base_tax = 17
	base_production = 4
}
1530.8.4 = {
	owner = FRA
	controller = FRA
} # Union Treaty
1545.1.1 = {
	fort_14th = yes
}
1560.1.1 = {
	religion = reformed
}
1588.12.1 = {
	unrest = 5
} # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1 = {
	unrest = 0
} # 'Paris vaut bien une messe!', Henri converts to Catholicism
1636.1.1 = {
	unrest = 3
} # Revolt imminent
1638.1.1 = {
	unrest = 0
}
1639.1.1 = {
	unrest = 3
}
1640.1.1 = {
	fort_14th = no
	fort_15th = yes
}
1641.1.1 = {
	unrest = 0
}
1686.1.17 = {
	religion = catholic
} # Dragonnard campaign successful: region reverts back to catholicism
1750.1.1 = {
	fort_15th = no
	fort_16th = yes
}
#1786.1.1 = {
#		base_tax = 6
#	base_production = 6
#} # The Eden Agreement
1793.3.7 = { } # Guerres de l'Ouest
1796.12.23 = { } # The last rebels are defeated at the battle of Savenay
1799.10.15 = { } # Guerres de l'Ouest
