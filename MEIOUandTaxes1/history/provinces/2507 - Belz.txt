# 2507 - Belz

owner = GVO
controller = GVO
add_core = GVO

capital = "Belz"
trade_goods = livestock
culture = ruthenian
religion = orthodox

base_tax = 10
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech

1355.1.1 = {
	add_permanent_province_modifier = {
		name = polish_estates
		duration = -1
	}
}
1356.1.1 = {
	add_core = POL
	add_core = LIT
}
1366.9.1 = {
	owner = POL
	controller = POL
}
1388.1.1 = {
	owner = MAZ
	controller = MAZ
	add_core = MAZ
}
1393.1.1 = {
	remove_core = LIT
	remove_core = GVO
}
1462.1.1 = {
	owner = POL
	controller = POL
	remove_core = MAZ
}
1501.1.1 = {
	base_tax = 11
}
1569.7.1 = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1588.1.1 = {
	controller = REB
} # Civil war
1589.1.1 = {
	controller = PLC
} # Coronation of Sigismund III
1606.1.1 = {
	controller = REB
} # Civil war
1608.1.1 = {
	controller = PLC
} # Minor victory of Sigismund
1655.1.1 = {
	controller = SWE
} # The Deluge
1660.1.1 = {
	controller = PLC
}
1733.1.1 = {
	controller = REB
} # The war of Polish succession
1735.1.1 = {
	controller = PLC
}
1772.8.5 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = POL
	remove_core = PLC
} # First partition of Poland
