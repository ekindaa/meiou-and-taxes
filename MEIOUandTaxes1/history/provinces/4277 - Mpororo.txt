# No previous file for Mpororo

owner = MPO
controller = MPO
add_core = MPO

capital = "Kajara" #Should really change with each monarch
trade_goods = iron
culture = ganda
religion = animism

hre = no

base_tax = 8
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = central_african

450.1.1 = {
	set_province_flag = tribals_control_province
}
1520.1.1 = {
	base_tax = 13
}