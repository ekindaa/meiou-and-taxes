# 3953 - Kaga

owner = TGS
controller = TGS
add_core = TGS

capital = "Kanazawa"
trade_goods = rice #chinaware
culture = koshi
religion = mahayana

hre = no

base_tax = 13
base_production = 0
base_manpower = 1

is_city = yes
harbour_infrastructure_1 = yes

discovered_by = chinese

1387.1.1 = {
	add_core = SBA
	controller = SBA
	owner = SBA
}
1414.1.1 = {
	controller = TGS
	owner = TGS
}
1458.1.1 = {
	add_core = AKM
	controller = AKM
	owner = AKM
}
1464.1.1 = {
	controller = TGS
	owner = TGS
}
1501.1.1 = {
	base_tax = 17
	base_production = 4
	base_manpower = 3
}
1542.1.1 = {
	discovered_by = POR
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
