# 81 - N�rnberg

owner = NUR
controller = NUR
add_core = NUR

capital = "N�rnberg"
trade_goods = iron # steel
culture = eastfranconian
religion = catholic

hre = yes

base_tax = 3
base_production = 1
base_manpower = 0

is_city = yes
local_fortification_1 = yes
marketplace = yes
corporation_guild = yes
town_hall = yes
temple = yes
road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_steel
		duration = -1
	}
}
1500.1.1 = {
	fort_14th = yes
}
1501.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 1
	base_production = 4
	base_manpower = 0
}
1527.1.1 = { } # Philipp der Grossm�tige
1530.1.3 = {
	road_network = no
	paved_road_network = yes
	weapons = yes
}
1532.1.1 = {
	religion = protestant
}
1620.1.1 = {
	fort_14th = yes
}
1630.1.1 = {
	controller = SWE
}
1632.1.1 = {
	controller = NUR
}
1685.1.1 = { } # Reformed refugees find shelter in Kassel (-> Oberneustadt founded)
1806.1.1 = {
	owner = BAV
	controller = BAV
	add_core = BAV
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
