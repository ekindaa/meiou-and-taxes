# 974 - Bamberg
# GG - 22/07/2008

owner = BMB
controller = BMB
add_core = BMB

capital = "Bamberg"
trade_goods = wheat
culture = eastfranconian
religion = catholic

hre = yes

base_tax = 8
base_production = 1
base_manpower = 0

is_city = yes
town_hall = yes
local_fortification_1 = yes
road_network = yes
temple = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 9
	base_production = 1
	base_manpower = 1
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1803.4.27 = {
	owner = BAV
	controller = BAV
	add_core = BAV
	remove_core = BMB
} #Reichsdeputationshauptschluss
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
