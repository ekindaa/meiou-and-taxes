# Fero� islands

owner = NOR
controller = NOR
add_core = NOR

capital = "Torshavn"
trade_goods = fish
culture = norse
religion = catholic

hre = no

base_tax = 0
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_2 = yes

discovered_by = western

1200.1.1 = {
	set_province_flag = freeholders_control_province
} # Established during the 12th century
1350.1.1 = {
	set_variable = { which = starting_rural_pop value = 3.242 }
	set_variable = { which = starting_urban_pop value = 0.193 }
}
1523.6.21 = {
	owner = DAN
	controller = DAN
	add_core = DAN
	remove_core = NOR
}
1538.1.1 = {
	religion = reformed
} #First Protestant Minister Arrives in the Orkneys
1752.1.1 = {
	trade_goods = wool
} # Wool becomes more important.
1814.5.17 = {
	owner = DEN
	controller = DEN
	add_core = DEN
	remove_core = DAN
}
