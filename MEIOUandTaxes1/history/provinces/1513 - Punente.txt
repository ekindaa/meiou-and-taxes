# 1513 - Sann-a

owner = GEN
controller = GEN
add_core = GEN

capital = "Sann-a"
trade_goods = olive
culture = ligurian
religion = catholic

hre = no

base_tax = 11
base_production = 1
base_manpower = 1

is_city = yes
temple = yes
harbour_infrastructure_1 = yes
town_hall = yes
local_fortification_1 = yes
road_network = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_permanent_province_modifier = {
		name = "principality_of_monaco"
		duration = -1
	}
}
1477.3.20 = {
	controller = REB
}
1477.4.28 = {
	controller = GEN # MLO
}
1478.7.7 = {
	controller = REB
}
1488.1.6 = {
	controller = GEN # MLO
}
1488.8.7 = {
	controller = REB
}
1488.9.13 = {
	controller = GEN # MLO
}
1499.1.1 = {
	owner = FRA
	controller = FRA
	add_core = FRA
}
1507.4.10 = {
	controller = REB
}
1507.4.27 = {
	controller = FRA
}
1512.1.1 = {
	controller = REB
}
1513.1.1 = {
	controller = FRA
}
1520.5.5 = {
	base_tax = 14
	base_production = 1
	base_manpower = 1
}
1522.1.1 = {
	owner = GEN
	controller = GEN
}
1527.1.1 = {
	owner = FRA
	controller = FRA
}
1528.1.1 = {
	owner = GEN
	controller = GEN
	remove_core = FRA
} #Andrea Doria
1530.1.2 = {
	road_network = no
	paved_road_network = yes
}
1618.1.1 = {
	hre = no
}
1805.6.10 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Annexed by France
1814.4.11 = {
	owner = SPI
	controller = SPI
	add_core = SPI
	remove_core = FRA
} # Incorporated into the kingdom of Sardinia
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
