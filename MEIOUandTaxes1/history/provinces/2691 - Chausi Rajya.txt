# 2691 - Chausi Rajya

owner = NPL
controller = NPL
add_core = NPL

capital = "Ghurka"
trade_goods = lumber #naval_supplies
culture = nepali
religion = hinduism

hre = no

base_tax = 27
base_production = 0
base_manpower = 1

is_city = yes
local_fortification_1 = yes
#fort_14th = yes

discovered_by = indian
discovered_by = muslim
discovered_by = chinese
discovered_by = steppestech

1511.1.1 = {
	base_tax = 31
}
1690.1.1 = {
	discovered_by = ENG
}
1707.5.12 = {
	discovered_by = GBR
}
