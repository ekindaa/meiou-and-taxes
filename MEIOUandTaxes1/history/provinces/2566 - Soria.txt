# 2566 - Soria

owner = CAS #Juan II of Castille
controller = CAS
add_core = CAS

capital = "Soria"
trade_goods = lumber
culture = castillian
religion = catholic

hre = no

base_tax = 10
base_production = 1
base_manpower = 1

is_city = yes
town_hall = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_core = ENR
}
1369.3.23 = {
	remove_core = ENR
}
1489.1.1 = {
	small_university = yes
}
1500.3.3 = {
	base_tax = 12
	base_production = 1
	base_manpower = 1
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	road_network = no
	paved_road_network = yes
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castille
1713.4.11 = {
	remove_core = CAS
}
1808.6.6 = {
	controller = REB
}
1811.1.1 = {
	controller = SPA
}
1812.10.1 = {
	controller = REB
}
1813.12.11 = {
	controller = SPA
}
