# 1360 - Ansbach

owner = NUS
controller = NUS
add_core = NUS

capital = "Ansbach"
trade_goods = livestock
culture = eastfranconian
religion = catholic

hre = yes

base_tax = 12
base_production = 1
base_manpower = 1

is_city = yes
town_hall = yes
road_network = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

790.1.1 = {
	rename_capital = "Onoltesbach"
	change_province_name = "Onoltesbach"
} # Onold's Brook in Old High German
837.1.1 = {
	rename_capital = "Onoltespah"
	change_province_name = "Onoltespah"
}
1141.1.1 = {
	rename_capital = "Onoldesbach"
	change_province_name = "Onoldesbach"
}
1230.1.1 = {
	rename_capital = "Onoldsbach"
	change_province_name = "Onoldsbach"
}
1338.1.1 = {
	rename_capital = "Onelspach"
	change_province_name = "Onelspach"
}
1398.1.21 = {
	owner = ANS
	controller = ANS
	add_core = ANS
	remove_core = NUS
}
1500.1.1 = {
	road_network = yes
}
1508.1.1 = {
	rename_capital = "Onsbach"
	change_province_name = "Onsbach"
}
1519.1.1 = {
	religion = protestant
	fort_14th = no
	fort_15th = yes
} # After attacking the free town of Reutlingen the Duke of Würtemberg is sent fleeing and the country governed by the Austrians.
1520.5.5 = {
	base_tax = 14
	base_production = 1
	base_manpower = 1
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1732.1.1 = {
	rename_capital = "Ansbach"
	change_province_name = "Ansbach"
}
1796.1.18 = {
	owner = PRU
	controller = PRU
	add_core = PRU
}
1805.12.15 = {
	controller = FRA
	add_core = FRA
	owner = FRA
} # First Treaty of Schönburg
1806.7.12 = {
	hre = no
	owner = BAV
	controller = BAV
	add_core = BAV
	remove_core = PRU
	remove_core = FRA
}
