# 208 - Zamora & Toro

owner = CAS #Juan II of Castille
controller = CAS

capital = "Zamora"
trade_goods = wheat
culture = asturian
religion = catholic

hre = no

base_tax = 9
base_production = 1
base_manpower = 1

is_city = yes
local_fortification_1 = yes
town_hall = yes
temple = yes # Zamora temple
road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1 = {
	add_core = ENR
	add_core = CAS
	add_core = LEO
	add_permanent_province_modifier = {
		name = "kingdom_of_leon"
		duration = -1
	}
}
1369.3.23 = {
	remove_core = ENR
}
1464.5.1 = {
	unrest = 3
} #Nobiliary uprising against King Enrique, Castilla goes into anarchy
1468.9.18 = {
	unrest = 0
} #Pactos de los Toros de Guisando. Isabel of Castille becomes heir to the throne and a temporary peace is achieved
1470.1.1 = {
	unrest = 3
} #Isabel marries with Fernando of Aragon, breaking the Pacts of Guisando. King Enrique choses his daughter Juana ("La Beltraneja") as new heiress and a succession War erupts.
1475.6.2 = {
	controller = POR
}
1476.3.2 = {
	controller = CAS
}
1479.9.4 = {
	unrest = 0
} #Peace of Alca�ovas, between Queen Isabel and King Alfonso of Portugal who had entered the war supporting her wife Juana
1500.3.3 = {
	base_tax = 9
	base_production = 2
	base_manpower = 1
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = CAS
	road_network = no
	paved_road_network = yes
	remove_core = LEO
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castilla
1520.5.1 = {
	unrest = 5
} # War of the Comunidades
1521.4.1 = {
	unrest = 0
} # The army of the "Comuneros" is defeated at Villalar. Its leaders are promptly beheaded.
1713.4.11 = {
	remove_core = LEO
}
1808.6.6 = {
	controller = REB
}
1808.12.1 = {
	controller = SPA
}
1812.10.1 = {
	controller = REB
}
1813.12.11 = {
	controller = SPA
}
