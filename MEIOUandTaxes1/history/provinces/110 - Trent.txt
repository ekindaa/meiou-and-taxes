# 110 - Tr�nt

owner = TNT
controller = TNT
add_core = TNT

capital = "Tr�nt"
trade_goods = wine
culture = venetian
religion = catholic

hre = yes

base_tax = 10
base_production = 0
base_manpower = 1

is_city = yes
road_network = yes
temple = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim

1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 12
	base_production = 1
	base_manpower = 1
}
1525.3.1 = {
	unrest = 8
}
1525.9.1 = {
	unrest = 0
}
1646.1.1 = {
	fort_14th = yes
}
1801.2.9 = {
	owner = HAB
	controller = HAB
	add_core = HAB
} # The Treaty of Lun�ville
1805.12.26 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = HAB
} # The Treaty of Pressburg
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1814.4.11 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
