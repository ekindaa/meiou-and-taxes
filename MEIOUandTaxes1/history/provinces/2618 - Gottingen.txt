# 2618 - Göttingen

owner = GTG
controller = GTG
add_core = GTG

capital = "Göttingen"
trade_goods = wool
culture = eastphalian
religion = catholic

hre = yes

base_tax = 5
base_production = 1
base_manpower = 0

is_city = yes
town_hall = yes
workshop = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1495.1.1 = {
	owner = HAN
	controller = HAN
	add_core = HAN
	remove_core = GTG
}
1500.1.1 = {
	road_network = yes
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 8
	base_production = 0
	base_manpower = 0
}
1529.1.1 = {
	religion = protestant
}
#1692.1.1 = {
#	owner = HAN
#	controller = HAN
#	add_core = HAN
#	remove_core = LUN
#}
1757.1.1 = {
	controller = FRA
}
1762.1.1 = {
	controller = HAN
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1807.7.9 = {
	owner = WES
	controller = WES
	add_core = WES
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1813.10.14 = {
	owner = HAN
	controller = HAN
	remove_core = WES
} # Westfalia is dissolved after the Battle of Leipsig
