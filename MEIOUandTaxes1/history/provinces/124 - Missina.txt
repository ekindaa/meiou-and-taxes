# 124 - Missina

owner = SIC
controller = SIC
add_core = SIC

capital = "Missina"
trade_goods = fish #silk
culture = sicilian
religion = catholic

hre = no

base_tax = 8
base_production = 4
base_manpower = 1

is_city = yes
local_fortification_1 = yes
road_network = yes
urban_infrastructure_1 = yes
harbour_infrastructure_2 = yes
marketplace = yes
workshop = yes
fort_14th = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

450.1.1 = {
	add_permanent_province_modifier = {
		name = "strait_of_messina_strait"
		duration = -1
	}
}
500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_silk
		duration = -1
	}
}
1356.1.1 = {
	add_core = ARA
	add_core = KNP
}
1409.1.1 = {
	owner = ARA
	controller = ARA
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = ARA
	road_network = no
	paved_road_network = yes
	fort_14th = yes
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1520.5.5 = {
	base_tax = 7
	base_production = 9
	base_manpower = 1
}
1522.3.20 = {
	naval_arsenal = yes
}
1530.1.1 = {
	owner = KNP
	controller = KNP
	remove_core = SPA
}
1531.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
}
1706.7.1 = {
	controller = SAV
}
1713.4.11 = {
	owner = SIC
	controller = SIC
	remove_core = SPA
}
1718.8.2 = {
	owner = HAB
	controller = HAB
	add_core = HAB
}
1734.6.2 = {
	owner = NAP
	controller = NAP
	add_core = NAP
	remove_core = HAB
}
#1815.5.3 = {
#	owner = SIC
#	controller = SIC
#	remove_core = NAP
#}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
