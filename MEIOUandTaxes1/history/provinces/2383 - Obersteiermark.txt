# 2383 - Obersteiermark

owner = HAB
controller = HAB
add_core = HAB

capital = "Judenburg"
trade_goods = wheat
culture = austrian
religion = catholic

hre = yes

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1379.1.1 = {
	controller = STY
	owner = STY
	add_core = STY
}
1490.1.1 = {
	controller = HAB
	owner = HAB
}
1500.1.1 = {
	road_network = yes
}
1515.4.1 = {
	unrest = 4
} # Bund - Farmer insurrections
1515.10.1 = {
	unrest = 0
}
1520.5.5 = {
	base_tax = 11
	base_production = 0
	base_manpower = 0
}
1525.5.1 = {
	unrest = 8
} # serious Farmer insurrections
1526.1.1 = {
	unrest = 0
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
}
1578.1.1 = {
	fort_14th = yes
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
