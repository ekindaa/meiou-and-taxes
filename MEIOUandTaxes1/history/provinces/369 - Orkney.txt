# Orkney / Orkneyjar

owner = NOR
controller = NOR
add_core = NOR

capital = "Kirkjuvagr"
trade_goods = fish
culture = norse
religion = catholic

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_2 = yes
temple = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	add_permanent_province_modifier = {
		name = "orkney_natural_harbour"
		duration = -1
	}
}
1350.1.1 = {
	set_variable = { which = starting_rural_pop value = 20.201 }
	set_variable = { which = starting_urban_pop value = 0.164 }
}
1469.1.1 = {
	owner = SCO
	controller = SCO
	add_core = SCO
	remove_core = NOR
	culture = highland_scottish
	capital = "Kirkwall"
} # Dowry of Margaret, Daughter of Christian I of Denmark and Norway and Wife of James III of Scotland
1609.1.1 = {
	religion = reformed
} # First Protestant Minister Arrives in the Orkneys
1650.3.1 = {
	controller = REB
} # Landing of Montrose
1650.4.27 = {
	controller = SCO
} # Battle of Carbisdale
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
1750.1.1 = {
	fort_14th = yes
}
