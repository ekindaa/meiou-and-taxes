#128 - Koroska (Karnten, carinthie)
#Celovec (klagenfurt)

owner = HAB
controller = HAB
add_core = HAB

capital = "Celovec"
trade_goods = wheat
culture = slovenian
religion = catholic

hre = yes

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes
fort_14th = yes
temple = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1270.1.1 = {
	controller = BOH
	owner = BOH
	add_core = BOH
}
1278.1.1 = {
	controller = HAB
	owner = HAB
	add_core = CAR
	remove_core = BOH
}
1379.1.1 = {
	controller = STY
	owner = STY
	add_core = STY
}
1490.1.1 = {
	controller = HAB
	owner = HAB
	remove_core = STY
}
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 17
	base_production = 0
	base_manpower = 1
	fort_14th = yes
}
1525.5.1 = {
	unrest = 2
} # Farmer insurrections
1526.1.1 = {
	unrest = 0
	remove_core = CAR
}
#1578.1.1 = {
#	fort_14th = yes
#}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
