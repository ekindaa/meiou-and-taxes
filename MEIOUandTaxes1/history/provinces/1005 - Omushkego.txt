# No previous file for Omushkego

capital = "Omushkego"
trade_goods = unknown
culture = cree
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 25
native_ferocity = 2
native_hostileness = 5

450.1.1 = {
	set_province_flag = tribals_control_province
}
1611.1.1 = {
	discovered_by = ENG
} # Henry Hudson
1689.1.1 = {
	discovered_by = GBR
	owner = ENG
	controller = ENG
	citysize = 100
	culture = english
	religion = protestant
} # Construction of Fort Severn
1699.1.1 = {
	discovered_by = FRA
} # Pierre le Moyne
1707.5.12 = {
	discovered_by = GBR
	owner = GBR
	controller = GBR
}
1714.1.1 = {
	add_core = GBR
}
1750.1.1 = {
	citysize = 880
	trade_goods = fur
}
1800.1.1 = {
	citysize = 1150
}
