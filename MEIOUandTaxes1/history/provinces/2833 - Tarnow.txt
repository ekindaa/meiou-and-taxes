# 2833 - Tarnow

owner = POL
controller = POL
add_core = POL

capital = "Tarnow"
trade_goods = hemp
culture = polish
religion = catholic

hre = no

base_tax = 9
base_production = 0
base_manpower = 1

is_city = yes
road_network = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1356.1.1 = {
	add_permanent_province_modifier = {
		name = polish_estates
		duration = -1
	}
}
1520.5.5 = {
	base_tax = 14
}
1569.7.1 = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1588.1.1 = {
	controller = REB
} # Civil war
1589.1.1 = {
	controller = PLC
} # Coronation of Sigismund III
1606.1.1 = {
	controller = REB
} # Civil war
1608.1.1 = {
	controller = PLC
} # Minor victory of Sigismund
1655.1.1 = {
	controller = SWE
} # The Deluge
1657.1.1 = {
	unrest = 0
} # Rebellion fails
1660.1.1 = {
	controller = PLC
}
1733.1.1 = {
	controller = REB
} # The war of Polish succession
1735.1.1 = {
	controller = PLC
}
1772.8.5 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = POL
	remove_core = PLC
} # First partition of Poland
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
