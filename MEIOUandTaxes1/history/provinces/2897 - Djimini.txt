# 2897 - Djimini

owner = KNG
controller = KNG
add_core = KNG

capital = "Kong"
trade_goods = millet
culture = senufo
religion = west_african_pagan_reformed

hre = no

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes
marketplace = yes

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 78 }
	add_permanent_province_modifier = {
		name = "ivory_low"
		duration = -1
	}
}