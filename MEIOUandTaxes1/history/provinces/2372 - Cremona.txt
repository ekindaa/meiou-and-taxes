# 2372 - Cremona

owner = MLO
controller = MLO
add_core = MLO

capital = "Cremona"
trade_goods = wheat #linen
culture = lombard
religion = catholic

hre = yes

base_tax = 5
base_production = 5
base_manpower = 0

is_city = yes
temple = yes
urban_infrastructure_2 = yes
workshop = yes
marketplace = yes
local_fortification_1 = yes
road_network = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_linen
		duration = -1
	}
}
1440.1.1 = {
	fort_14th = yes
}
1499.1.1 = {
	controller = VEN
	owner = VEN
	add_core = VEN
}
1509.1.1 = {
	controller = MLO
	owner = MLO
	remove_core = VEN
}
1513.3.23 = {
	owner = SPA
	controller = SPA
	add_core = SPA
} # Treaty of Noyons
1520.5.5 = {
	base_tax = 7
	base_production = 5
	base_manpower = 0
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
}
1530.2.27 = {
	hre = no
}
1618.1.1 = {
	hre = no
}
1707.4.10 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = SPA
}
1796.11.15 = {
	owner = ITC
	controller = ITC
	add_core = ITC
} # Transpadane Republic
1797.6.29 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = ITC
} # Cisalpine Republic
#1802.6.26 = {
#	owner = ITA
#	controller = ITA
#	add_core = ITA
#	remove_core = ITE
#} # Italian Republic
# 1805.3.17 - Kingdom of Italy
1814.4.11 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = ITE
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1860.3.20 = {
	owner = SPI
	controller = SPI
	add_core = SPI
} # ??
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
