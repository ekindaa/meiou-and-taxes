# No previous file for Pah� S�pa

capital = "Paha Sapa"
trade_goods = unknown
culture = cheyenne
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 15
native_ferocity = 3
native_hostileness = 6

450.1.1 = {
	set_province_flag = tribals_control_province
}
1760.1.1 = {
	owner = CHY
	controller = CHY
	add_core = CHY
	trade_goods = fur
	culture = cheyenne
	is_city = yes
} # Horses allow plain tribes to expand
