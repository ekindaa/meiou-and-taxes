# Agdesiden
# MEIOU - Gigau

owner = NOR
controller = NOR
add_core = NOR

capital = "Arendall"
trade_goods = lumber
culture = norwegian
religion = catholic

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

450.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = copper
}
1350.1.1 = {
	set_variable = { which = starting_rural_pop value = 20.572 }
	set_variable = { which = starting_urban_pop value = 0.341 }
}
1523.6.21 = {
	owner = DAN
	controller = DAN
	add_core = DAN
}
1531.11.1 = {
	revolt = {
		type = nationalist_rebels
		size = 0
	}
	controller = REB
} #The Return of Christian II
1532.7.15 = {
	revolt = { }
	controller = DAN
}
1536.1.1 = {
	religion = protestant
} #Unknown date
1641.1.1 = {
	capital = "Kristiansand"
}
1814.1.14 = {
	owner = SWE
	revolt = {
		type = nationalist_rebels
		size = 0
	}
	controller = REB
	remove_core = DAN
} # Norway is ceded to Sweden following the Treaty of Kiel
1814.5.17 = {
	revolt = { }
	owner = NOR
	controller = NOR
}
