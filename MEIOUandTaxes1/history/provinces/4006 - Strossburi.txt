# 4006 - Strossburi

owner = FST
controller = FST
add_core = FST

capital = "Stra�burg"
trade_goods = wine
culture = rhine_alemanisch
religion = catholic

hre = yes

base_tax = 8
base_production = 2
base_manpower = 0

is_city = yes
local_fortification_1 = yes
temple = yes
marketplace = yes
urban_infrastructure_1 = yes
workshop = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

450.1.1 = {
	add_permanent_province_modifier = {
		name = "strasburg_confluence"
		duration = -1
	}
	add_permanent_province_modifier = {
		name = urban_goods_cloth
		duration = -1
	}
}
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 10
	base_production = 2
	base_manpower = 0
}
1681.9.30 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Captured by Louis XIV to cut the Imperial gateway into French Alsace
1697.9.20 = {
	hre = no
} # Treaty of Ryswick, imperial recognition
