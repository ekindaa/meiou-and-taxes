# 3380 - Langensalza

owner = THU
controller = THU
add_core = THU

capital = "Langensalza"
trade_goods = wool
culture = thuringian
religion = catholic

hre = yes

base_tax = 8
base_production = 1
base_manpower = 0

is_city = yes
town_hall = yes
workshop = yes
local_fortification_1 = yes
road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1440.1.1 = {
	owner = SAX
	controller = SAX
	add_core = SAX
	remove_core = MEI
}#Duchy of Thuringia is inherited by Saxony
1485.11.11 = {
	owner = MEI
	controller = MEI
	add_core = MEI
	remove_core = SAX
} #Treaty of Leipzig
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 8
	base_production = 2
	base_manpower = 1
}
1520.12.10 = {
	religion = protestant
}
1547.5.19 = {
	owner = SAX
	controller = SAX
	add_core = SAX
	remove_core = MEI
} #Treaty of Wittenberg
1560.1.1 = {
	fort_15th = yes
}
1790.8.1 = {
	unrest = 5
} # Peasant revolt
1791.1.1 = {
	unrest = 0
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1815.6.9 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = SAX
} #Ceded to Prussia