# 1121 - Timbo

owner = FLO
controller = FLO
add_core = FLO

capital = "Timbo"
trade_goods = livestock
culture = fulani
religion = sunni

hre = no

base_tax = 6
base_production = 0
base_manpower = 1

is_city = yes

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 86 }
	add_permanent_province_modifier = {
		name = "ivory_medium"
		duration = -1
	}
}
1520.1.1 = {
	base_tax = 16
}
