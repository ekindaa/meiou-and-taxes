# 3850 - Osterland

owner = MEI
controller = MEI
add_core = MEI

capital = "Colditz"
trade_goods = wheat
culture = high_saxon
religion = catholic

hre = yes

base_tax = 7
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1423.6.1 = {
	owner = SAX
	controller = SAX
	add_core = SAX
} # Margraviate of Meissen inherits Saxony and becomes the Elector.
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 8
}
1539.1.1 = {
	religion = protestant
}
1790.8.1 = {
	unrest = 5
} # Peasant revolt
1791.1.1 = {
	unrest = 0
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved

