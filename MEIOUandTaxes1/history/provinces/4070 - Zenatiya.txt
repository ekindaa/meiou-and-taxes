# 4070 - Zenatiya

owner = FEZ
controller = FEZ
add_core = FEZ

capital = "Zenatiya"
trade_goods = fish
culture = rifain
religion = sunni

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = "lack_of_harbour"
		duration = -1
	}
}
1530.1.1 = {
	add_core = MOR
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1554.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
}
1603.1.1 = {
	unrest = 5
} # The death of the Saadita Ahmad I al-Mansur
1604.1.1 = {
	unrest = 0
}
1638.1.1 = {
	owner = FEZ
	controller = FEZ
	remove_core = MOR
}
1664.1.1 = {
	owner = TFL
	controller = TFL
	add_core = TFL
	remove_core = MOR
}
1668.8.2 = {
	owner = MOR
	controller = MOR
	remove_core = TFL
}
1672.1.1 = {
	unrest = 4
} # Oppositions against Ismail, & the idea of a unified state
1727.1.1 = {
	unrest = 0
}
