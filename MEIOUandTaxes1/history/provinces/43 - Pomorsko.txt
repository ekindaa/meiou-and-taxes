# 43 - P�ck�

owner = TEU
controller = TEU
add_core = TEU

capital = "P�ck�" # Kashubian: P�ck�, P�ck, P�ck, German: Putzig, Latvian: Pucka
trade_goods = wheat
culture = kashubian # pommeranian
religion = catholic

hre = no

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1 = {
	set_province_flag = add_local_autonomy_25 # still recovering after the Danzig slaughter
}
1444.1.1 = {
	#culture = prussian
}
1454.3.6 = {
	add_core = POL
} # Beginning of the "Thirteen years war"
1466.10.19 = {
	owner = POL
	controller = POL
	remove_core = TEU
	#culture = prussian
	capital = "Putzig"
} # Peace treaty, "Peace of Torun"
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 7
}
1524.1.25 = {
	unrest = 6
} # Debt crisis
1525.1.1 = {
	unrest = 0
	#religion = protestant
}
1525.4.10 = {
	#add_core = PRU
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1569.7.1 = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1572.1.1 = {
	unrest = 6
} # Sigismund II dies
1576.1.1 = {
	unrest = 8
} # Danzig rebellion
1577.6.13 = {
	controller = PLC
} # Danzig War, under siege by Poland
1588.1.1 = {
	controller = REB
} # Civil war
1589.1.1 = {
	controller = PLC
	unrest = 0
} # Coronation of Sigismund III
1702.5.1 = {
	controller = SWE
} # Occupied again
1706.9.24 = {
	controller = PLC
} # Karl XII defeated in the battle of Poltava
1733.1.1 = {
	controller = REB
} # The war of Polish succession
1735.1.1 = {
	controller = PLC
}
1772.8.5 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = PLC
} # First partition
1794.3.24 = {
	unrest = 5
} # Kosciuszko uprising
1794.11.16 = {
	unrest = 0
} # The end of the uprising
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1807.3.1 = {
	controller = FRA
} # Occupied by French troops
1807.7.9 = {
	controller = PRU
}
