# 202 Avignon - Principal cities: Avignon

owner = PAP
controller = PAP
add_core = PAP

capital = "Avinho"
trade_goods = wine # silk
culture = provencal
religion = catholic

hre = no

base_tax = 1
base_production = 4
base_manpower = 1

is_city = yes
temple = yes
town_hall = yes
marketplace = yes # Exception: center of banking and finance
workshop = yes
art_corporation = yes
road_network = yes
small_university = yes #1303 L'Universit� d'Avignon
local_fortification_1 = yes
fort_14th = yes # Large exception: Le Palais des Papes is super strong, and build on and beneath a natural rock formation
# Exception: center of banking and finance

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_silk
		duration = -1
	}
}
1249.1.1 = {
	set_province_flag = has_confluence
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "avignon_confluence"
		duration = -1
	}
}
1309.1.1 = {
	save_global_event_target_as = the_vatican
}
#1356.1.1 = {
#	add_core = AVI
#}
#1378.3.27 = {
#	owner = AVI
#	controller = AVI
#}
1378.9.20 = {
	owner = AVI
	controller = AVI
	add_core = AVI
}
1417.10.18 = {
	owner = PAP
	controller = PAP
	remove_core = AVI
}
#1439.1.1 = {
#	owner = AVI
#	controller = AVI
#	add_core = AVI
#}
#1449.1.1 = {
#	owner = PAP
#	controller = PAP
#	remove_core = AVI
#}
1520.5.5 = {
	base_tax = 3
	base_production = 3
	base_manpower = 0
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
}
1670.1.1 = {
	add_core = FRA
} # Louis XIV wants to annex the city-state (Chambres de R�union)
1791.9.14 = {
	owner = FRA
	controller = FRA
	remove_core = PAP
} # Avignon is incorporated into France
