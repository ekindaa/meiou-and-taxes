#342 - Ouadanne

capital = "Ouadanne"
trade_goods = palm_date
culture = hassaniya
religion = sunni

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

native_size = 60
native_ferocity = 4.5
native_hostileness = 9

discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 86 }
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}
1437.1.1 = {
	discovered_by = POR
} # Cadamosto
