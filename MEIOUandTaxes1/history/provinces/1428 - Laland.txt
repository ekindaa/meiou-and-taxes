# Oerne
# MEIOU - Gigau

owner = DEN
controller = DEN
add_core = DEN

capital = "Nacascogh"
trade_goods = wheat
culture = danish
religion = catholic

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1350.1.1 = {
	set_variable = { which = starting_rural_pop value = 32.124 }
	set_variable = { which = starting_urban_pop value = 0.243 }
}
1416.1.1 = {
	temple = yes
}
1500.1.1 = {
	road_network = yes
}
1500.3.3 = {
	base_tax = 3
	base_production = 0
	base_manpower = 0
}
1522.2.15 = {
	shipyard = yes
}
1523.6.21 = {
	owner = DAN
	controller = DAN
	add_core = DAN
	remove_core = DEN
}
1526.1.1 = {
	religion = protestant
} #preaching of Hans Tausen
1529.12.17 = {
	merchant_guild = yes
}
1536.1.1 = {
	religion = protestant
}
1658.1.31 = {
	controller = SWE
} #Karl X Gustavs First Danish War-Captured by Karl X Gustav
1658.2.26 = {
	controller = DAN
} #The Peace of Roskilde
1814.5.17 = {
	owner = DEN
	controller = DEN
	add_core = DEN
	remove_core = DAN
}
