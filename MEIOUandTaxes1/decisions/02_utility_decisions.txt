country_decisions = {
	
	show_utility_decisions = {
		potential = {
			ai = no
			has_country_flag = hide_utility_decisions
		}
		allow = {
		}
		effect = {
			custom_tooltip = show_utility_decisions_tt
			hidden_effect = {
				clr_country_flag = hide_utility_decisions
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	hide_utility_decisions = {
		potential = {
			ai = no
			NOT = { has_country_flag = hide_utility_decisions }
		}
		allow = {
		}
		effect = {
			custom_tooltip = hide_utility_decisions_tt
			hidden_effect = {
				set_country_flag = hide_utility_decisions
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	## BYOA
	revise_demesne = {
		potential = {
			always = no
			ai = no
			NOT = { has_country_flag = hide_utility_decisions }
		}
		allow = {}
		effect = {
			census_and_ce_calc_effect = yes
			indepotent_byoa_calc_effect = yes
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	## CE
	show_ce_modifiers = {
		potential = {
			ai = no
			NOT = { has_country_flag = hide_utility_decisions }
		}
		allow = {
			NOT = { has_country_modifier = show_ce }
		}
		effect = {
			custom_tooltip = show_ce_modifiers_tt
			
			
			hidden_effect = {
				add_country_modifier = {
					name = show_ce
					duration = 30
					hidden = yes
				}
				every_owned_province = {
					remove_ce_mod_mapmode = yes
					set_ce_mod_mapmode = yes
				}
			}
		}
		ai_will_do = {
			factor = 0
		}
	}
	
	
	## SI
	hide_cultural_decisions = {
		potential = {
			ai = no
			NOT = { has_country_flag = hide_utility_decisions }
			NOT = { has_country_flag = cultural_decision_off }
			OR = {
				technology_group = western
				culture_group = west_slavic
			}
		}
		allow = {
		}
		effect = {
			set_country_flag = cultural_decision_off
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	view_cultural_decisions = {
		potential = {
			ai = no
			NOT = { has_country_flag = hide_utility_decisions }
			has_country_flag = cultural_decision_off
		}
		allow = {
		}
		effect = {
			clr_country_flag = cultural_decision_off
		}
		ai_will_do = {
			factor = 1
		}
	}
}