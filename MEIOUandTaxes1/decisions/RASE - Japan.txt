country_decisions = {
	# Ryukyu adopts Chinese-style government
	ryukyu_adopt_chinese = {
		major = yes
		potential = {
			tag = RYU
			OR = {
				government = tribal_monarchy
				government = tribal_monarchy_elective
				government = tribal_theocracy
			}
		}
		allow = {
			is_subject_of_type = tributary_state
			overlord = { has_country_flag = mandate_of_heaven_claimed }
			absolutism = 25
			adm_power = 500
			dip_power = 500
		}
		effect = {
			set_country_flag = adopted_chinese_government
			change_government = chinese_monarchy_3
			add_legitimacy = 10
			add_adm_power = -500
			add_dip_power = -500
			add_absolutism = 10
		}
		ai_will_do = {
			factor = 1
		}
	}
	
}
