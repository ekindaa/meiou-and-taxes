# Tianxia Historical Decisions
# 1. The Unified China

country_decisions = {
	
	the_unified_china = {
		major = yes
		potential = {
			culture_group = chinese_group
			NOT = { dynasty = "Borjigin" }
			NOT = {
				has_country_flag = request_defect
			}
			OR = {
				is_free_or_tributary_trigger = yes
				overlord = {
					has_country_flag = mandate_of_heaven_claimed
				}
			}
			any_neighbor_country = {
				has_country_flag = mandate_of_heaven_claimed
				culture_group = chinese_group
				is_free_or_tributary_trigger = yes
			}
			NOT = { is_year = 1400 }
		}
		allow = {
			any_neighbor_country = {
				has_country_flag = mandate_of_heaven_claimed
				NOT = { war_with = ROOT }
				culture_group = chinese_group
				is_free_or_tributary_trigger = yes
			}
			NOT = { overlord = { NOT = { culture_group = chinese_group } } }
			NOT = {
				has_ruler_modifier = unified_china
			}
		}
		effect = {
			random_neighbor_country = {
				limit = {
					has_country_flag = mandate_of_heaven_claimed
					NOT = { war_with = ROOT }
					culture_group = chinese_group
					is_free_or_tributary_trigger = yes
				}
				country_event = { id = "tianxia.15" }
			}
			set_country_flag = request_defect
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
}