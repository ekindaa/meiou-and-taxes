#16 - Qizilbash Cavalry

unit_type = muslim
type = cavalry
maneuver = 2

offensive_morale = 3
defensive_morale = 4
offensive_fire = 0
defensive_fire = 0
offensive_shock = 4
defensive_shock = 3

trigger = {
	OR = {
		NOT = { religion = shiite }
		NOT = { primary_culture = persian }
	}
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}

