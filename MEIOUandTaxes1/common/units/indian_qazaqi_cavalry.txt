# Qazaqi Raiders (28)

unit_type = indian
type = cavalry
maneuver = 2

offensive_morale = 6
defensive_morale = 3
offensive_fire = 3
defensive_fire = 2
offensive_shock = 4
defensive_shock = 2

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}