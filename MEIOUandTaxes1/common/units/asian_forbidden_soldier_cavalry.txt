#Forbidden Soldiers (25)

type = cavalry
unit_type = chinese
maneuver = 2

offensive_morale = 3
defensive_morale = 6
offensive_fire = 0
defensive_fire = 1
offensive_shock = 4
defensive_shock = 4

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}