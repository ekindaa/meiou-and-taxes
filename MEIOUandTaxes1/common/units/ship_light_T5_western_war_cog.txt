#5 - War Cog
type = light_ship

hull_size = 11 #140 tonnage, modified to 8 to allow for upgrade
base_cannons = 12 #4 guns
sail_speed = 6 #?
trade_power = 1

#sailors = 36

sprite_level = 1

trigger = {
	NOT = { technology_group = eastern }
	NOT = { technology_group = muslim }
	NOT = { technology_group = turkishtech }
	NOT = { technology_group = high_turkishtech }
	NOT = { technology_group = nomad_group }
	NOT = { technology_group = steppestech }
	NOT = { technology_group = soudantech }
	NOT = { technology_group = sub_saharan }
	NOT = { technology_group = indian }
	NOT = { technology_group = hawaii_tech }
	NOT = { technology_group = chinese }
	NOT = { technology_group = austranesian }
	
	NOT = { has_country_flag = raised_special_units }
}

