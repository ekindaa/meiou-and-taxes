# Western Mounted Archers (7)

type = cavalry
unit_type = western
maneuver = 2

offensive_morale = 3
defensive_morale = 2
offensive_fire = 0
defensive_fire = 0
offensive_shock = 3
defensive_shock = 1

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}