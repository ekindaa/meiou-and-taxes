#25 - Mounted Arquebusiers

unit_type = western
type = cavalry
maneuver = 2

offensive_morale = 3
defensive_morale = 3
offensive_fire = 4
defensive_fire = 2
offensive_shock = 3
defensive_shock = 3
trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}