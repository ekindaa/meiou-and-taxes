# Western Gallop Cavalry (30)

type = cavalry
unit_type = western
maneuver = 2

offensive_morale = 6
defensive_morale = 3
offensive_fire = 2
defensive_fire = 1
offensive_shock = 5
defensive_shock = 4

trigger = {
	NOT = { tag = KAL } #Hakkapeliitta
	NOT = { culture_group = scandinavian } #Hakkapeliitta
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}


