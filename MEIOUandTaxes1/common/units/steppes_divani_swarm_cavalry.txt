#Divani Swarm Cavalry (8)

type = cavalry
unit_type = steppestech
maneuver = 2

offensive_morale = 1
defensive_morale = 3
offensive_fire = 0
defensive_fire = 0
offensive_shock = 3
defensive_shock = 3

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}