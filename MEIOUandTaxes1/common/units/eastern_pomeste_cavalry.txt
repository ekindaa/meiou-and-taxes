#Pomeste cavalry
#tech level 18

unit_type = eastern
type = cavalry
maneuver = 2

offensive_morale = 3
defensive_morale = 1
offensive_fire = 2
defensive_fire = 2
offensive_shock = 4
defensive_shock = 3

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}