#Country Name: See the Name of this File.

graphical_culture = indiangfx

color = { 63  20  177 }

historical_idea_groups = {
	leadership_ideas
	trade_ideas
	quality_ideas
	diplomatic_ideas
	administrative_ideas
	naval_ideas
	popular_religion_ideas
	economic_ideas
	spy_ideas
}

historical_units = { #North Indian group
	indian_kashmir_slinger_infantry
	indian_war_elephant_cavalry
	indian_bow_infantry
	indian_heavy_elephant_cavalry
	indian_nayaka_infantry
	indian_armored_elephant_cavalry
	indian_fauj_infantry
	indian_tabinan_cavalry
	indian_banduqchi_infantry
	indian_silhedar_cavalry
	indian_mawle_infantry
	indian_musketeer_elephantry
	indian_saranjam_infantry
	indian_ekanda_cavalry
	indian_ganimi_kava_infantry
	indian_pindari_cavalry
	indian_westernized_infantry
	indian_uhlan_cavalry
	indian_rifled_infantry
	indian_lighthussar_cavalry
	indian_drill_infantry
	indian_hunter_cavalry
	indian_impulse_infantry
	indian_lancer_cavalry
	indian_breech_infantry
}

monarch_names = {
	"Bicharpatipha #0" = 20
	"Vikramadityapha #0" = 20
	"Mahamanipha #0" = 20
	"Manipha #0" = 20
	"Ladapha #0" = 20
	"Khorapha #0" = 20
	"Khunkhorapha #0" = 20
	"Detsungpha #0" = 20
	"Nirbhay Narayan #0" = 20
	"Harmesvar #0" = 5
	"Megha Narayana #0" = 5
	"Satrrudaman #0" = 10
	"Nar Narayan #0" = 10
	"Bhimdarpa Narayan #0" = 10
	"Indraballabh Narayan #0" = 10
	"Makardhwaj #0" = 10
	"Udayaditya #0" = 10
	
	"Saw Sit #0" = -1
	"Saw Pu Nyo #0" = -1
	"Saw Pyauk #0" = -1
	"Saw Pyinsa #0" = -1
	"Saw Yin Mi #0" = -1
	"Gita Chand #0" = -1
	"Uma #0" = -1
	"Krishna Devi #0" = -1
	"Mangaldahi #0" = -1
	"Jira #0" = -1
	"Hira #0" = -1
}

leader_names = {
	Udayaditya
	Makardhwaj
	Chandra
	Ladapha
	Deka
	Manipha
	Mahamanipha
	Narayan
	Vikramadityapha
	Bicharpatipha
	Detsungpha
}

ship_names = {
	Brahmaputra "Chingri Mach" Darrang
	Devi Dibang "Durga Ma" Ganga "Hilsa Mach"
	"Ilish Mach" Machli "Kali Ma" "Kali Meghana"
	"Kali Nauk" Kamrup Lakimpur "Lal Nauk"
	"Loki Devi" Luhit "Manasa Devi" Manjuli
	Meghna "Nil Nauk" "Nil Sagor" "Parvati Devi"
	"Rui Mach" Saraswoti "Sagorer Bahini" Shakti
	"Shaktir Nauk" "Shasti Ma" Sibsagar Trishool
	"Uma Devi"
}
