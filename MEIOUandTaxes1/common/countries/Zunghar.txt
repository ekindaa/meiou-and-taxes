#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 114  137  92 }

historical_idea_groups = {
	popular_religion_ideas
	trade_ideas
	quantity_ideas
	leadership_ideas
	merchant_marine_ideas
	economic_ideas
	logistic_ideas
	administrative_ideas
}

historical_units = {
	#Steppes Group
	steppes_spear_infantry
	steppes_swarm_cavalry
	steppes_archer_infantry
	steppes_heavy_swarm_cavalry
	steppes_divani_swarm_cavalry
	steppes_heavy_infantry
	steppes_tumandar_cavalry
	steppes_handgunner_infantry
	steppes_yikitlar_cavalry
	steppes_firelock_cavalry
	steppes_banner_infantry
	steppes_raid_cavalry
	steppes_green_standard_infantry
	steppes_shock_cavalry
	steppes_field_army_infantry
	steppes_armeblanche_cavalry
	steppes_hunter_cavalry
	steppes_drill_infantry
	steppes_impulse_infantry
	steppes_lancer_cavalry
	steppes_breech_infantry
}

monarch_names = {
	"Batula" = 40
	"Toghan" = 40
	"Galdan" = 40
	"Amursana" = 20
	"Erdeni" = 20
	"Choghtu" = 20
	"Dawa" = 20
	"Esen" = 20
	"G�shi" = 20
	"Lha-bzang" = 20
	"Khara Khula" = 20
	"Kho" = 20
	"Sengge" = 20
	"Tseten" = 20
	"Tsewang" = 20
	"Kundelung" = 15
	"Lobzang" = 15
	"Sutai" = 15
	"Tsobasa" = 15
	"Ubasi" = 15
	"Quduqa" = 10
	"Toghto" = 10
	"Altai" = 1
	"B�ki" = 1
	"Bayan" = 1
	"Dayan" = 1
	"Hami" = 1
	"Molon" = 1
	"Oori" = 1
	"Orl�k" = 1
	"Sechen" = 1
	"Radi" = 1
	"Talman" = 1
	"Todo" = 1
	"Tumen" = 1
	"Tumu" = 1
	"Ubashi" = 1
	"Ulan" = 1
	"Wehe" = 1
	"Yelu" = 1
	"Zaya" = 1
	
	"Heying" = -1
	"Chunmei" = -1
	"Tianhui" = -1
	"Kon" = -1
	"Mao" = -1
	"Aigiarm" = -1
	"Borte" = -1
	"Ho'elun" = -1
	"Orqina" = -1
	"Toregene" = -1
	"Hoelun" = -1
	"Tam�l�n" = -1
	"Sorghaghtani" = -1
}

leader_names = {
	Borjigin
	Chuluun
	Dairtan
	Dotno
	Duutan
	Okhid
	Khuushan
	Onkhod
	Khaliuchin
	Myangad
	Khavkhchin
	Tangud
	Oimuud
	Besud
	Togoruutan
	ar-togoruutan
	Ikh-khotgoid
	Baga-khotgoid
	Uvur-togoruutan
	Tsuvdag
	Kherdeg
	Orchid
	Burged
	Unduriinkhen
	Doloon
	Khorkhoinkhon
	Barga
	Khariad
	avkhainkhan
	Tsookhor
	Toos
	Sharnuud
	Zelmen
	Tumt
	ayagachin
	Darkhad
	Bicheech
	Khurniad
	Uushgiinkhan
	Uushin
	Choros
	Ogniud
	Khukhnuud
	Khirgis
	Bashgid
	Mudar
	Doloon-suvai
	Khukh-nokhoinkhon
	Todno
	Khemchigiinkhen
	Uuld
	Tugchin
	Ukher-tsagaanikhan
	Tsognuud
	Khukh-nuur
	Ukher-onkhod
	Khonin-khotgoid
}

ship_names = {
	Bargut Buzav Kerait Naiman "D�rvn ��rd"
	Khoshut Olo Dzungar Torgut Dorbot
	Amdo "Altan Khan" Khoshot Ol�ts G�shi
}

army_names = {
	"$PROVINCE$ Tumen"
}