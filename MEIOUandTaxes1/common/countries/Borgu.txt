#Country Name: Borgu
#Tag: BOR
#MEIOU-FB - African/Sudan tech group split Nov 08
#dharper's African cavalry added

graphical_culture = africangfx

color = { 17  158  75 }

historical_idea_groups = {
	trade_ideas
	leadership_ideas
	administrative_ideas
	quality_ideas
	aristocracy_ideas
	spy_ideas
	diplomatic_ideas
	logistic_ideas
}

historical_units = { #Savannah Empires
	sudanese_ton_tigi_light_cavalry
	sudanese_free_skirmisher_infantry
	sudanese_farima_heavy_cavalry
	unique_MAL_jonow_infantry
	sudanese_quilted_heavy_cavalry
	sudanese_ranked_infantry
	sudanese_armored_heavy_cavalry
	sudanese_elite_archer_infantry
	sudanese_angola_gun_infantry
	sudanese_eso_heavy_cavalry
	sudanese_spear_and_shot_infantry
	sudanese_slave_javelin_cavalry
	sudanese_savannah_musketeer_infantry
	sudanese_sofa_rifle_infantry
	sudanese_mass_infantry
	sudanese_lighthussar_cavalry
	sudanese_columnar_infantry
	sudanese_lancer_cavalry
	sudanese_jominian_infantry
}

monarch_names = {
	"Beriguwiemda #0" = 40
	"Zolgo #0" = 20
	"Zongman #0" = 20
	"Nengmitoni #0" = 20
	"Dimani #0" = 20
	"Yanza #0" = 20
	"Dariziogo #0" = 20
	"Luro #0" = 20
	"Tutugri #0" = 20
	"Zagale #0" = 20
	"Zokuli #0" = 20
	"Gungoble #0" = 20
	"Boadu Akofu Berempon #0" = 20
	"Boa Amponsem #0" = 20
	"Ntim Gyakari #0" = 20
	"Ose Tutu #0" = 20
	"Opoku Fofie #0" = 20
	"Opoku Ware #0" = 20
	"Kusi Obodum #0" = 20
	"Osei Bonsu #0" = 1
	"Osei Kojo #0" = 20
	"Osei Kwame #0" = 20
	"Osei Fofie #0" = 20
	"Osei Yaw #0" = 20
	"Kwaku Dua #0" = 20
	"Salifu Saatankugri #0" = 1
	"Mahama Kuluguba #0" = 1
	"Nyagse #0" = 1
	"Zulande #0" = 1
	"Zangina #0" = 1
	"Andan Sigili #0" = 1
	"Jimli #0" = 1
	"Zibirim #0" = 1
	"Gariba #0" = 1
	"Yakuba #0" = 1
	"Ayekeraa #0" = 1
	"Kokobo #0" = 1
	"Ahaha #0" = 1
	"Warembe Ampen #0" = 1
	"Agyinamoa #0" = 1
	"Twum #0" = 1
	"Kobla Amana #0" = 1
	"Akenten #0" = 1
	"Obiri Yeboa #0" = 1
	"Opoku #0" = 1
	
	"Hude #0" = -1
	"Amoako #0" = -1
	"Pokou #0" = -1
	"Emose #0" = -1
	"Amina #0" = -1
	"Lingeer #0" = -1
	"Orrorro #0" = -1
	"Saraounia #0" = -1
}

leader_names = {
	Addy Afrifa Akoto
	Donkor
	Moyoyo
	Offei
	Sabah Sadami Sribor
	Zakari
}

ship_names = {
	Benada Bia
	Dwoada
	Fiada
	Kwasiada
	Memeneda
	Nyame
	Tano
	Wukuada
	Yawoada
	"Asase Yaa"
}
