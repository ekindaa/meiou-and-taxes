#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 200  155  155 }

historical_idea_groups = {
	leadership_ideas
	logistic_ideas
	naval_ideas
	trade_ideas
	quality_ideas
	spy_ideas
	diplomatic_ideas
	administrative_ideas
}

#Japanese group
historical_units = {
	asian_light_foot_infantry
	asian_horse_archer_cavalry
	asian_bushi_cavalry
	asian_shashu_no_ashigaru_infantry
	asian_samurai_cavalry
	asian_samurai_infantry
	asian_yarigumi_infantry
	asian_late_samurai_cavalry
	asian_arquebusier_infantry
	asian_musketeer_infantry
	asian_horse_guard_cavalry
	asian_new_guard_infantry
	asian_volley_infantry
	asian_armeblanche_cavalry
	asian_bayonet_infantry
	asian_lighthussar_cavalry
	asian_drill_infantry
	asian_columnar_infantry
	asian_lancer_cavalry
	asian_breech_infantry
}

monarch_names = {
	"Nobuhiro" = 100
	"Mitshiro" = 100
	"Yoshihiro" = 100
	"Suehiro" = 100
	"Kinhiro" = 100
	"Ujihiro" = 100
	"Takahiro" = 100
	"Norihiro" = 100
	"Kunihiro" = 100
	"Sukehiro" = 100
	"Michihiro" = 100
	"Akihiro" = 100
	
	"Ako" = -1
	"Asahi" = -1
	"Aya" = -1
	"Harukiri" = -1
	"Inuwaka" = -1
	"Itoito" = -1
	"Itsuitsu" = -1
	"Koneneme" = -1
	"Mitsu" = -1
	"Narime" = -1
	"Sakami" = -1
	"Shiro" = -1
	"Tatsuko" = -1
	"Tomiko" = -1
	"Toyome" = -1
	"Yamabukime" = -1
}

leader_names = {
	# Daimyo names
	Akamatsu Akechi Ashikaga Amago Kyogoku Rokkaku Asakura Asano Aso Chosokabe Date Hachisuka
	Hatakeyama Hojo Hosokawa Ichijo Keda Imagawa Isshiki Ito Jinbo Kikuchi Kono Maeda
	Matsunaga Mimura Miyoshi Mogami Mori Nabeshima Nagao Oda Ogasawara Otomo Ouchi
	Ryuzoji Sagara Saito Sanada Satake Satomi Shiba Shibata Shimazu Shoni Takeda Toki
	Tokugawa Matsudaira Toyotomi Tsutsui Uesugi Uragami Yamana Yamauchi
	# Kakizaki Flavor
	Kakizaki Kakizaki Kakizaki Kakizaki Kakizaki Kakizaki Kakizaki Kakizaki Kakizaki Kakizaki
	Matsumae Matsumae Matsumae Matsumae Matsumae Matsumae Matsumae Matsumae Matsumae Matsumae
	# Regional names:Tohoku
	Matsumae Tsugaru Namioka Shiba Nanbu Kunohe Kudo Asonuma Waga Kurosawa Kita Hei
	Kasai Nikaido Osaki Sudo Rusu Katakura Ujiie Rusu Kurokawa Watari Moniwa Kokubun
	Shiraishi Endo Akita Asari Onodera Tozawa Yuri Nikaho Mogami Tendo
	Sakenobe Sagae Daihoji Tateoka Date Tamura Ishikawa Ouchi Hatakeyama Ashina Inawashiro
	Soma Sato Iwaki Shirakawa Kanagami
}

ship_names = {
	"Mutsu Maru" "Dewa Maru" "Shimotsuke Maru"
	"Hitachi Maru"
	"Kazusa Maru" "Shimousa Maru" "Awa Maru" "Musashi Maru" "Kozuke Maru" "Izu Maru" "Echigo Maru" "Ettchu Maru"
	"Kaga Maru" "Noto Maru" "Suruga Maru" "Totomi Maru" "Joshu Maru" "Enshu Maru" "Bushu Maru" "Esshu Maru"
	"Mikawa Maru" "Ushu Maru" "Soushu Maru" "Boushu Maru" "Kai Maru" "Shinshu Maru" "Koshu Maru"
	"Owari Maru" "Bishu Maru" "Mino Maru" "Echizen Maru" "Ise Maru" "Shima Maru" "Omi Maru" 
	"Goushu Maru" "Yamashiro Maru" "Wakasa Maru" "Tanba Maru" "Tango Maru" "Yamato Maru" "Kii Maru"
	"Kishu Maru" "Iga Maru" "Sanuki Maru" "Tosa Maru"
	"Iyo Maru" "Harima Maru" "Banshu Maru"
	"Hoki Maru" "Inaba Maru" "Bizen Maru" "Mimasaka Maru" "Binshu Maru"
	"Bingo Maru" "Bittchu Maru" "Aki Maru" "Izumo Maru" "Iwami Maru" "Suou Maru" "Nagato Maru" "Bungo Maru"
	"Buzen Maru" "Chikuzen Maru"
	"Chikugo Maru" "Hizen Maru" "Higo Maru" "Chikushu Maru" "Hyuga Maru" "Osumi Maru" "Satsuma Maru" "Sasshu Maru"
}

army_names = {
	"Kakizaki Gun" "$PROVINCE$ Gun" "$PROVINCE$ Zei"  "$PROVINCE$ Dan" 
}

fleet_names = {
	"Kakizaki Suigun" "$PROVINCE$ Suigun" "$PROVINCE$ Sendan" "$PROVINCE$ Tai" 
}