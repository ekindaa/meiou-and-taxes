#Country Name: Edo
#Tag: BEN

graphical_culture = africangfx

color = { 110  173  129 }

historical_idea_groups = {
	trade_ideas
	leadership_ideas
	administrative_ideas
	quality_ideas
	aristocracy_ideas
	spy_ideas
	diplomatic_ideas
	logistic_ideas
}

historical_units = { #Coastal states
	sudanese_tuareg_camelry
	sudanese_archer_infantry
	sudanese_tribal_raider_light_cavalry
	sudanese_sword_infantry
	sudanese_war_raider_light_cavalry
	sudanese_sofa_infantry
	sudanese_pony_archer_cavalry
	sudanese_sofa_musketeer_infantry
	sudanese_angola_gun_infantry
	sudanese_eso_heavy_cavalry
	sudanese_gold_coast_infantry
	sudanese_musketeer_infantry
	sudanese_carabiner_cavalry
	sudanese_countermarch_musketeer_infantry
	unique_DAH_amazon_infantry
	sudanese_rifled_infantry
	sudanese_hunter_cavalry
	sudanese_impulse_infantry
	sudanese_lancer_cavalry
	sudanese_breech_infantry
}

monarch_names = {
	"Orobiru #0" = 20
	"Uwaifiokun #0" = 20
	"Ewuare #0" = 20
	"Ezoti #0" = 20
	"Olua #0" = 20
	"Ozolua #0" = 20
	"Esigie #0" = 20
	"Orhogbua #0" = 20
	"Ehengbuda #0" = 20
	"Ohuan #0" = 20
	"Ahenzae #0" = 20
	"Akengboi #0" = 20
	"Akenkpaye #0" = 20
	"Akengbedo #0" = 20
	"Oreoghene #0" = 20
	"Ewuakpe #0" = 20
	"Ozuere #0" = 20
	"Akenzua #0" = 20
	"Eresonyen #0" = 20
	"Akengbuda #0" = 20
	"Obanosa #0" = 20
	"Ogbebo #0" = 20
	"Osemwede #0" = 20
	"Alagbariye #0" = 1
	"Okapara #0" = 1
	"Asimini #0" = 1
	"Edimi #0" = 1
	"Kamba #0" = 1
	"Kamalu #0" = 1
	"Dappa #0" = 1
	"Amakiri #0" = 1
	"Appinya #0" = 1
	"Warri #0" = 1
	"Awusa #0" = 1
	"Egbani #0" = 1
	"Ogbodo #0" = 1
	"Owagi #0" = 1
	"Ogio #0" = 1
	"Peresuo #0" = 1
	"Basuo #0" = 1
	"Mingi #0" = 1
	"Glele #0" = 1
	
	"Hude #0" = -1
	"Amoako #0" = -1
	"Pokou #0" = -1
	"Emose #0" = -1
	"Amina #0" = -1
	"Lingeer #0" = -1
	"Orrorro #0" = -1
	"Saraounia #0" = -1
}

leader_names = {
	Ahomadegbe
	Apithy
	Dangbo
	Kerekou
	Kouandete
	Kuetey
	Maga
	Soglo
	Trudo
	Zinsou
}

ship_names = {
	Xevioso Sogbo "Nana Buluku" "Mawu-Lisa"
	Dan Gbadu Da Gu Ayaba Loko Sakpata Gleti
	Zinsu Zinsi
}
