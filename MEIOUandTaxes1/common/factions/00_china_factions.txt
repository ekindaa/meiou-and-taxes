temples =
{
	#Changed to the Imperial Dictatorship by Mod
	monarch_power = MIL
	always = yes
	
	modifier = 
	{
#		free_leader_pool = 1 # Mod
		land_forcelimit_modifier = 0.1 # Mod
		discipline = 0.02 # Mod
		land_morale = 0.10
		unjustified_demands = -0.5
		core_creation = -0.15
		war_exhaustion_cost = -0.1
		global_unrest = 3
		stability_cost_modifier = 0.1
		state_maintenance_modifier = 0.20 #global_tax_modifier = -0.15 # Mod:Emperor needs more gold for his prestige
	}
}

enuchs =
{
	monarch_power = DIP
	always = yes
	
	#Merchants, Traders, Expansions.
	
	modifier = 
	{
		trade_efficiency = 0.05
		merchants = 2
		global_foreign_trade_power = 0.15
		naval_forcelimit_modifier = 0.25
		production_efficiency = -0.1
		global_unrest = 2
		legitimacy = -0.25
		global_spy_defence = 0.1 # Mod
		state_maintenance_modifier = -0.05 #global_tax_modifier = 0.05 # Mod
		stability_cost_modifier = 0.3
		heir_chance = 0.5 # Mod:Emperor is staying in his harem.
	}
}

bureaucrats =
{
	monarch_power = ADM
	always = yes
	
	#buildings, Internal Stability.
	modifier = 
	{
		advisor_pool = 1 # Mod
		#advisor_cost = -0.1
		stability_cost_modifier = -0.1
		global_unrest = -3
		legitimacy = 0.5
		state_maintenance_modifier = 0.05 #global_tax_modifier = -0.10
		production_efficiency = 0.05
		global_regiment_recruit_speed = 0.1
		build_cost = -0.05
		religious_unity = 0.05
	}
}
