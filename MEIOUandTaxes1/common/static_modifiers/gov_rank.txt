gov_rank_1 = {
	manpower_recovery_speed = 0.20
	land_forcelimit = 2
	stability_cost_modifier = -0.3
}

gov_rank_2 = {
	manpower_recovery_speed = 0.10
	land_forcelimit = 1
	stability_cost_modifier = -0.2
	liberty_desire_from_subject_development = -0.111
}

gov_rank_3 = {
	stability_cost_modifier = -0.1
	diplomatic_upkeep = 1
	advisor_cost = 0.15
	liberty_desire_from_subject_development = -0.166
}

gov_rank_4 = {
	merchants = 1
	diplomatic_upkeep = 2
	advisor_cost = 0.30
	advisor_pool = 1
	liberty_desire_from_subject_development = -0.222
}

gov_rank_5 = {
	stability_cost_modifier = 0.1
	merchants = 1
	free_leader_pool = 1
	diplomatic_upkeep = 4
	advisor_cost = 0.60
	advisor_pool = 1
	liberty_desire_from_subject_development = -0.277
}

# gains cultural union
gov_rank_6 = {
	stability_cost_modifier = 0.2
	merchants = 1
	free_leader_pool = 1
	diplomatic_upkeep = 4
	advisor_cost = 1.00
	advisor_pool = 2
	liberty_desire_from_subject_development = -0.333
}

gov_rank_7 = {}

gov_rank_8 = {}

gov_rank_9 = {}

gov_rank_10 = {}
