absolutism_lookup = {
	set_variable = { which = $variable$ value = 0 }
	trigger_switch = {
		on_trigger = absolutism
		50 = {
			trigger_switch = {
				on_trigger = absolutism
				75 = {
					trigger_switch = {
						on_trigger = absolutism
						88 = {
							trigger_switch = {
								on_trigger = absolutism
								94 = {
									trigger_switch = {
										on_trigger = absolutism
										97 = {
											trigger_switch = {
												on_trigger = absolutism
												99 = {
													trigger_switch = {
														on_trigger = absolutism
														100 = { set_variable = { which = $variable$ value = 100 } }
														99 = { set_variable = { which = $variable$ value = 99 } }
													}
												}
												97 = {
													trigger_switch = {
														on_trigger = absolutism
														98 = { set_variable = { which = $variable$ value = 98 } }
														97 = { set_variable = { which = $variable$ value = 97 } }
													}
												}
											}
										}
										94 = {
											trigger_switch = {
												on_trigger = absolutism
												96 = { set_variable = { which = $variable$ value = 96 } }
												95 = { set_variable = { which = $variable$ value = 95 } }
												94 = { set_variable = { which = $variable$ value = 94 } }
											}
										}
									}
								}
								88 = {
									trigger_switch = {
										on_trigger = absolutism
										91 = {
											trigger_switch = {
												on_trigger = absolutism
												93 = { set_variable = { which = $variable$ value = 93 } }
												92 = { set_variable = { which = $variable$ value = 92 } }
												91 = { set_variable = { which = $variable$ value = 91 } }
											}
										}
										88 = {
											trigger_switch = {
												on_trigger = absolutism
												90 = { set_variable = { which = $variable$ value = 90 } }
												89 = { set_variable = { which = $variable$ value = 89 } }
												88 = { set_variable = { which = $variable$ value = 88 } }
											}
										}
									}
								}
							}
						}
						75 = {
							trigger_switch = {
								on_trigger = absolutism
								81 = {
									trigger_switch = {
										on_trigger = absolutism
										84 = {
											trigger_switch = {
												on_trigger = absolutism
												86 = {
													trigger_switch = {
														on_trigger = absolutism
														87 = { set_variable = { which = $variable$ value = 87 } }
														86 = { set_variable = { which = $variable$ value = 86 } }
													}
												}
												84 = {
													trigger_switch = {
														on_trigger = absolutism
														85 = { set_variable = { which = $variable$ value = 85 } }
														84 = { set_variable = { which = $variable$ value = 84 } }
													}
												}
											}
										}
										81 = {
											trigger_switch = {
												on_trigger = absolutism
												83 = { set_variable = { which = $variable$ value = 83 } }
												82 = { set_variable = { which = $variable$ value = 82 } }
												81 = { set_variable = { which = $variable$ value = 81 } }
											}
										}
									}
								}
								75 = {
									trigger_switch = {
										on_trigger = absolutism
										78 = {
											trigger_switch = {
												on_trigger = absolutism
												80 = { set_variable = { which = $variable$ value = 80 } }
												79 = { set_variable = { which = $variable$ value = 79 } }
												78 = { set_variable = { which = $variable$ value = 78 } }
											}
										}
										75 = {
											trigger_switch = {
												on_trigger = absolutism
												77 = { set_variable = { which = $variable$ value = 77 } }
												76 = { set_variable = { which = $variable$ value = 76 } }
												75 = { set_variable = { which = $variable$ value = 75 } }
											}
										}
									}
								}
							}
						}
					}
				}
				50 = {
					trigger_switch = {
						on_trigger = absolutism
						62 = {
							trigger_switch = {
								on_trigger = absolutism
								68 = {
									trigger_switch = {
										on_trigger = absolutism
										71 = {
											trigger_switch = {
												on_trigger = absolutism
												73 = {
													trigger_switch = {
														on_trigger = absolutism
														74 = { set_variable = { which = $variable$ value = 74 } }
														73 = { set_variable = { which = $variable$ value = 73 } }
													}
												}
												71 = {
													trigger_switch = {
														on_trigger = absolutism
														72 = { set_variable = { which = $variable$ value = 72 } }
														71 = { set_variable = { which = $variable$ value = 71 } }
													}
												}
											}
										}
										68 = {
											trigger_switch = {
												on_trigger = absolutism
												70 = { set_variable = { which = $variable$ value = 70 } }
												69 = { set_variable = { which = $variable$ value = 69 } }
												68 = { set_variable = { which = $variable$ value = 68 } }
											}
										}
									}
								}
								62 = {
									trigger_switch = {
										on_trigger = absolutism
										65 = {
											trigger_switch = {
												on_trigger = absolutism
												67 = { set_variable = { which = $variable$ value = 67 } }
												66 = { set_variable = { which = $variable$ value = 66 } }
												65 = { set_variable = { which = $variable$ value = 65 } }
											}
										}
										62 = {
											trigger_switch = {
												on_trigger = absolutism
												64 = { set_variable = { which = $variable$ value = 64 } }
												63 = { set_variable = { which = $variable$ value = 63 } }
												62 = { set_variable = { which = $variable$ value = 62 } }
											}
										}
									}
								}
							}
						}
						50 = {
							trigger_switch = {
								on_trigger = absolutism
								56 = {
									trigger_switch = {
										on_trigger = absolutism
										59 = {
											trigger_switch = {
												on_trigger = absolutism
												61 = { set_variable = { which = $variable$ value = 61 } }
												60 = { set_variable = { which = $variable$ value = 60 } }
												59 = { set_variable = { which = $variable$ value = 59 } }
											}
										}
										56 = {
											trigger_switch = {
												on_trigger = absolutism
												58 = { set_variable = { which = $variable$ value = 58 } }
												57 = { set_variable = { which = $variable$ value = 57 } }
												56 = { set_variable = { which = $variable$ value = 56 } }
											}
										}
									}
								}
								50 = {
									trigger_switch = {
										on_trigger = absolutism
										53 = {
											trigger_switch = {
												on_trigger = absolutism
												55 = { set_variable = { which = $variable$ value = 55 } }
												54 = { set_variable = { which = $variable$ value = 54 } }
												53 = { set_variable = { which = $variable$ value = 53 } }
											}
										}
										50 = {
											trigger_switch = {
												on_trigger = absolutism
												52 = { set_variable = { which = $variable$ value = 52 } }
												51 = { set_variable = { which = $variable$ value = 51 } }
												50 = { set_variable = { which = $variable$ value = 50 } }
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		1 = {
			trigger_switch = {
				on_trigger = absolutism
				25 = {
					trigger_switch = {
						on_trigger = absolutism
						37 = {
							trigger_switch = {
								on_trigger = absolutism
								43 = {
									trigger_switch = {
										on_trigger = absolutism
										46 = {
											trigger_switch = {
												on_trigger = absolutism
												48 = {
													trigger_switch = {
														on_trigger = absolutism
														49 = { set_variable = { which = $variable$ value = 49 } }
														48 = { set_variable = { which = $variable$ value = 48 } }
													}
												}
												46 = {
													trigger_switch = {
														on_trigger = absolutism
														47 = { set_variable = { which = $variable$ value = 47 } }
														46 = { set_variable = { which = $variable$ value = 46 } }
													}
												}
											}
										}
										43 = {
											trigger_switch = {
												on_trigger = absolutism
												45 = { set_variable = { which = $variable$ value = 45 } }
												44 = { set_variable = { which = $variable$ value = 44 } }
												43 = { set_variable = { which = $variable$ value = 43 } }
											}
										}
									}
								}
								37 = {
									trigger_switch = {
										on_trigger = absolutism
										40 = {
											trigger_switch = {
												on_trigger = absolutism
												42 = { set_variable = { which = $variable$ value = 42 } }
												41 = { set_variable = { which = $variable$ value = 41 } }
												40 = { set_variable = { which = $variable$ value = 40 } }
											}
										}
										37 = {
											trigger_switch = {
												on_trigger = absolutism
												39 = { set_variable = { which = $variable$ value = 39 } }
												38 = { set_variable = { which = $variable$ value = 38 } }
												37 = { set_variable = { which = $variable$ value = 37 } }
											}
										}
									}
								}
							}
						}
						25 = {
							trigger_switch = {
								on_trigger = absolutism
								31 = {
									trigger_switch = {
										on_trigger = absolutism
										34 = {
											trigger_switch = {
												on_trigger = absolutism
												36 = { set_variable = { which = $variable$ value = 36 } }
												35 = { set_variable = { which = $variable$ value = 35 } }
												34 = { set_variable = { which = $variable$ value = 34 } }
											}
										}
										31 = {
											trigger_switch = {
												on_trigger = absolutism
												33 = { set_variable = { which = $variable$ value = 33 } }
												32 = { set_variable = { which = $variable$ value = 32 } }
												31 = { set_variable = { which = $variable$ value = 31 } }
											}
										}
									}
								}
								25 = {
									trigger_switch = {
										on_trigger = absolutism
										28 = {
											trigger_switch = {
												on_trigger = absolutism
												30 = { set_variable = { which = $variable$ value = 30 } }
												29 = { set_variable = { which = $variable$ value = 29 } }
												28 = { set_variable = { which = $variable$ value = 28 } }
											}
										}
										25 = {
											trigger_switch = {
												on_trigger = absolutism
												27 = { set_variable = { which = $variable$ value = 27 } }
												26 = { set_variable = { which = $variable$ value = 26 } }
												25 = { set_variable = { which = $variable$ value = 25 } }
											}
										}
									}
								}
							}
						}
					}
				}
				1 = {
					trigger_switch = {
						on_trigger = absolutism
						12 = {
							trigger_switch = {
								on_trigger = absolutism
								18 = {
									trigger_switch = {
										on_trigger = absolutism
										21 = {
											trigger_switch = {
												on_trigger = absolutism
												23 = {
													trigger_switch = {
														on_trigger = absolutism
														24 = { set_variable = { which = $variable$ value = 24 } }
														23 = { set_variable = { which = $variable$ value = 23 } }
													}
												}
												21 = {
													trigger_switch = {
														on_trigger = absolutism
														22 = { set_variable = { which = $variable$ value = 22 } }
														21 = { set_variable = { which = $variable$ value = 21 } }
													}
												}
											}
										}
										18 = {
											trigger_switch = {
												on_trigger = absolutism
												20 = { set_variable = { which = $variable$ value = 20 } }
												19 = { set_variable = { which = $variable$ value = 19 } }
												18 = { set_variable = { which = $variable$ value = 18 } }
											}
										}
									}
								}
								12 = {
									trigger_switch = {
										on_trigger = absolutism
										15 = {
											trigger_switch = {
												on_trigger = absolutism
												17 = { set_variable = { which = $variable$ value = 17 } }
												16 = { set_variable = { which = $variable$ value = 16 } }
												15 = { set_variable = { which = $variable$ value = 15 } }
											}
										}
										12 = {
											trigger_switch = {
												on_trigger = absolutism
												14 = { set_variable = { which = $variable$ value = 14 } }
												13 = { set_variable = { which = $variable$ value = 13 } }
												12 = { set_variable = { which = $variable$ value = 12 } }
											}
										}
									}
								}
							}
						}
						1 = {
							trigger_switch = {
								on_trigger = absolutism
								6 = {
									trigger_switch = {
										on_trigger = absolutism
										9 = {
											trigger_switch = {
												on_trigger = absolutism
												11 = { set_variable = { which = $variable$ value = 11 } }
												10 = { set_variable = { which = $variable$ value = 10 } }
												9 = { set_variable = { which = $variable$ value = 9 } }
											}
										}
										6 = {
											trigger_switch = {
												on_trigger = absolutism
												8 = { set_variable = { which = $variable$ value = 8 } }
												7 = { set_variable = { which = $variable$ value = 7 } }
												6 = { set_variable = { which = $variable$ value = 6 } }
											}
										}
									}
								}
								1 = {
									trigger_switch = {
										on_trigger = absolutism
										3 = {
											trigger_switch = {
												on_trigger = absolutism
												5 = { set_variable = { which = $variable$ value = 5 } }
												4 = { set_variable = { which = $variable$ value = 4 } }
												3 = { set_variable = { which = $variable$ value = 3 } }
											}
										}
										1 = {
											trigger_switch = {
												on_trigger = absolutism
												2 = { set_variable = { which = $variable$ value = 2 } }
												1 = { set_variable = { which = $variable$ value = 1 } }
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

initial_autonomy_effect = {
	set_local_autonomy = 0
	set_variable = { which = added_autonomy	value = 30 }
	set_variable = { which = removed_autonomy	value = 0 }
}

base_autonomy_effect = {
	trigger_switch = {
		on_trigger = has_province_modifier
		principality_of_taranto = {
			change_variable = { which = added_autonomy	value = 60 }
		}
		kurdish_princelings = {
			change_variable = { which = added_autonomy	value = 20 }
		}
		beylik_of_karasi = {
			change_variable = { which = added_autonomy	value = 20 }
		}
		beylik_of_saruhan = {
			change_variable = { which = added_autonomy	value = 20 }
		}
		beylik_of_isfendiyar = {
			change_variable = { which = added_autonomy	value = 20 }
		}
		beylik_of_artuqids = {
			change_variable = { which = added_autonomy	value = 20 }
		}
		beylik_of_ankara = {
			change_variable = { which = added_autonomy	value = 20 }
		}
		ahis_fraternity = {
			change_variable = { which = added_autonomy	value = 20 }
		}
		duchy_of_wroclaw = {
			change_variable = { which = added_autonomy	value = 40 }
		}
		ditmarsian_republic = {
			change_variable = { which = added_autonomy	value = 40 }
		}
		coalition_member = {
			change_variable = { which = added_autonomy	value = 40 }
		}
		marches_of_wales = {
			change_variable = { which = added_autonomy	value = 20 }
		}
		marches_of_scotland = {
			change_variable = { which = added_autonomy	value = 30 }
		}
		marches_of_the_south = {
			change_variable = { which = added_autonomy	value = 30 }
		}
		meccan_protectorate = {
			change_variable = { which = added_autonomy	value = 100 }
		}
		saint_malo_city1 = {
			change_variable = { which = added_autonomy	value = 30 }
		}
		saint_malo_city2 = {
			change_variable = { which = added_autonomy	value = 40 }
		}
		saint_malo_city3 = {
			change_variable = { which = added_autonomy	value = 90 }
		}
	}
	if = {
		limit = {
			has_province_modifier = city_charter
		}
		change_variable = { which = added_autonomy	value = 40 }
	}
	if = {
		limit = {
			has_province_modifier = trade_post_modifier
		}
		change_variable = { which = removed_autonomy	value = 25 }
	}
	if = {
		limit = {
			has_province_modifier = county_palatine
		}
		change_variable = { which = added_autonomy	value = 30 }
	}
	if = {
		limit = {
			has_province_modifier = north_of_england
		}
		change_variable = { which = added_autonomy	value = 16 }
	}
	if = {
		limit = {
			has_province_modifier = clan_land
		}
		change_variable = { which = added_autonomy	value = 30 }
	}
	trigger_switch = {
		on_trigger = has_building
		local_fortification_1 = {
			change_variable = { which = added_autonomy	value = 5 }
		}
		local_fortification_2 = {
			change_variable = { which = added_autonomy	value = 10 }
		}
		local_fortification_3 = {
			change_variable = { which = added_autonomy	value = 20 }
		}
	}
	trigger_switch = {
		on_trigger = has_building
		local_fortification_1_off = {
			change_variable = { which = added_autonomy	value = 5 }
		}
		local_fortification_2_off = {
			change_variable = { which = added_autonomy	value = 10 }
		}
		local_fortification_3_off = {
			change_variable = { which = added_autonomy	value = 20 }
		}
	}
	trigger_switch = {
		on_trigger = has_building
		small_university = {
			change_variable = { which = added_autonomy	value = 5 }
		}
		medium_university = {
			change_variable = { which = added_autonomy	value = 10 }
		}
		big_university = {
			change_variable = { which = added_autonomy	value = 20 }
		}
	}
	trigger_switch = {
		on_trigger = has_building
		bureaucracy_1 = {
			change_variable = { which = removed_autonomy	value = 10 }
		}
		bureaucracy_2 = {
			change_variable = { which = removed_autonomy	value = 12.5 }
		}
		bureaucracy_3 = {
			change_variable = { which = removed_autonomy	value = 15 }
		}
		bureaucracy_4 = {
			change_variable = { which = removed_autonomy	value = 20 }
		}
		bureaucracy_5 = {
			change_variable = { which = removed_autonomy	value = 20 }
		}
	}
	if = {
		limit = {
			owner = { has_country_modifier = obstacle_succession }
		}
		change_variable = { which = added_autonomy	value = 5 }
	}
	if = {
		limit = {
			owner = { has_country_modifier = obstacle_gavelkind }
		}
		change_variable = { which = added_autonomy	value = 5 }
	}
	if = {
		limit = {
			owner = { has_country_modifier = obstacle_feudal_fragmentation }
		}
		change_variable = { which = added_autonomy	value = 4 }
	}
	if = {
		limit = {
			owner = { has_country_modifier = obstacle_feuding }
		}
		change_variable = { which = added_autonomy	value = 3 }
	}
	if = {
		limit = {
			has_province_modifier = fueros
		}
		change_variable = { which = added_autonomy	value = 6 }
	}
	owner = {
		trigger_switch = {
			on_trigger = government
			medieval_monarchy = {
				PREV = { change_variable = { which = added_autonomy	value = 10 } }
			}
			feudal_monarchy = {
				PREV = { change_variable = { which = added_autonomy	value = 5 } }
			}
			absolute_monarchy = {
				PREV = { change_variable = { which = removed_autonomy	value = 5 } }
			}
			constitutional_monarchy = {
				PREV = { change_variable = { which = removed_autonomy	value = 5 } }
			}
			english_monarchy = {
				PREV = { change_variable = { which = added_autonomy	value = 5 } }
			}
			noble_republic = {
				PREV = { change_variable = { which = added_autonomy	value = 5 } }
			}
			administrative_republic = {
				PREV = { change_variable = { which = removed_autonomy	value = 5 } }
			}
			constitutional_republic = {
				PREV = { change_variable = { which = added_autonomy	value = 6 } }
			}
			crowned_republic = {
				PREV = { change_variable = { which = added_autonomy	value = 2 } }
			}
			republican_dictatorship = {
				PREV = { change_variable = { which = added_autonomy	value = 5 } }
			}
			bureaucratic_despotism = {
				PREV = { change_variable = { which = removed_autonomy	value = 10 } }
			}
			peasants_republic = {
				PREV = { change_variable = { which = removed_autonomy	value = 5 } }
			}
			papal_government = {
				PREV = { change_variable = { which = added_autonomy	value = 10 } }
			}
			theocratic_government = {
				PREV = { change_variable = { which = added_autonomy	value = 5 } }
			}
			eastern_monarchy = {
				PREV = { change_variable = { which = added_autonomy	value = 10 } }
			}
			eastern_thalassocracy = {
				PREV = { change_variable = { which = added_autonomy	value = 10 } }
			}
			indian_monarchy = {
				PREV = { change_variable = { which = added_autonomy	value = 15 } }
			}
			rajput_monarchy = {
				PREV = { change_variable = { which = added_autonomy	value = 10 } }
			}
			maratha_confederacy = {
				PREV = { change_variable = { which = added_autonomy	value = 10 } }
			}
			chinese_horde_1 = {
				PREV = { change_variable = { which = added_autonomy	value = 5 } }
			}
			chinese_monarchy = {
				PREV = { change_variable = { which = removed_autonomy	value = 6 } }
			}
			chinese_monarchy_3 = {
				PREV = { change_variable = { which = removed_autonomy	value = 5 } }
			}
			chinese_monarchy_4 = {
				PREV = { change_variable = { which = added_autonomy	value = 5 } }
			}
			chinese_monarchy_5 = {
				PREV = { change_variable = { which = added_autonomy	value = 15 } }
			}
			tibetan_theocracy = {
				PREV = { change_variable = { which = added_autonomy	value = 10 } }
			}
			tribal_monarchy = {
				PREV = { change_variable = { which = added_autonomy	value = 20 } }
			}
			tribal_theocracy = {
				PREV = { change_variable = { which = added_autonomy	value = 20 } }
			}
			tribal_confederation = {
				PREV = { change_variable = { which = added_autonomy	value = 20 } }
			}
			tribal_federation = {
				PREV = { change_variable = { which = added_autonomy	value = 20 } }
			}
			tribal_amalgamation = {
				PREV = { change_variable = { which = added_autonomy	value = 100 } }
			}
			native_council_settled = {
				PREV = { change_variable = { which = added_autonomy	value = 5 } }
			}
			siberian_native_council = {
				PREV = { change_variable = { which = added_autonomy	value = 20 } }
			}
			tribal_nomads = {
				PREV = { change_variable = { which = added_autonomy	value = 25 } }
			}
			tribal_nomads_hereditary = {
				PREV = { change_variable = { which = added_autonomy	value = 25 } }
			}
			tribal_nomads_altaic = {
				PREV = { change_variable = { which = added_autonomy	value = 25 } }
			}
			tribal_nomads_steppe = {
				PREV = { change_variable = { which = added_autonomy	value = 25 } }
			}
			cossack_republic = {
				PREV = { change_variable = { which = added_autonomy	value = 15 } }
			}
			revolutionary_republic = {
				PREV = { change_variable = { which = removed_autonomy	value = 5 } }
			}
			revolutionary_republic = {
				PREV = { change_variable = { which = added_autonomy	value = 5 } }
			}
			imperial_city = {
				PREV = { change_variable = { which = added_autonomy	value = 30 } }
			}
			merchant_imperial_city = {
				PREV = { change_variable = { which = added_autonomy	value = 10 } }
			}
			ambrosian_republic = {
				PREV = { change_variable = { which = added_autonomy	value = 5 } }
			}
			iqta = {
				PREV = { change_variable = { which = added_autonomy	value = 10 } }
			}
			mamluk_government = {
				PREV = { change_variable = { which = added_autonomy	value = 10 } }
			}
			dutch_republic = {
				PREV = { change_variable = { which = added_autonomy	value = 10 } }
			}
			portuguese_monarchy = {
				PREV = { change_variable = { which = added_autonomy	value = 20 } }
			}
			colonial_government = {
				PREV = { change_variable = { which = added_autonomy	value = 10 } }
			}
			feudal_colonial_government = {
				PREV = { change_variable = { which = added_autonomy	value = 10 } }
			}
			trader_colonial_government = {
				PREV = { change_variable = { which = added_autonomy	value = 20 } }
			}
			population_colonial_government = {
				PREV = { change_variable = { which = added_autonomy	value = 5 } }
			}
			trade_company_gov = {
				PREV = { change_variable = { which = added_autonomy	value = 15 } }
			}
			signoria_monarchy = {
				PREV = { change_variable = { which = added_autonomy	value = 5 } }
			}
		}
	}
	
	if = {
		limit = {
			is_state = yes
		}
		change_variable = { which = removed_autonomy	value = 20 }
	}
	else = {
		change_variable = { which = removed_autonomy	value = 35 }
	}
	
	if = {
		limit = {
			owner = { religion = orthodox }
		}
		change_variable = { which = removed_autonomy	value = 2.5 }
	}
	if = {
		limit = {
			owner = { government = republic }
		}
		if = {
			limit = {
				owner = { republican_tradition = 95 }
			}
			change_variable = { which = removed_autonomy	value = 20 }
		}
		else_if = {
			limit = {
				owner = { republican_tradition = 90 }
			}
			change_variable = { which = removed_autonomy	value = 18 }
		}
		else_if = {
			limit = {
				owner = { republican_tradition = 80 }
			}
			change_variable = { which = removed_autonomy	value = 16 }
		}
		else_if = {
			limit = {
				owner = { republican_tradition = 70 }
			}
			change_variable = { which = removed_autonomy	value = 14 }
		}
		else_if = {
			limit = {
				owner = { republican_tradition = 60 }
			}
			change_variable = { which = removed_autonomy	value = 12 }
		}
		else_if = {
			limit = {
				owner = { republican_tradition = 50 }
			}
			change_variable = { which = removed_autonomy	value = 10 }
		}
		else_if = {
			limit = {
				owner = { republican_tradition = 40 }
			}
			change_variable = { which = removed_autonomy	value = 8 }
		}
		else_if = {
			limit = {
				owner = { republican_tradition = 30 }
			}
			change_variable = { which = removed_autonomy	value = 6 }
		}
		else_if = {
			limit = {
				owner = { republican_tradition = 20 }
			}
			change_variable = { which = removed_autonomy	value = 4 }
		}
		else_if = {
			limit = {
				owner = { republican_tradition = 10 }
			}
			change_variable = { which = removed_autonomy	value = 2 }
		}
		if = {
			limit = {
				owner = {
					faction_in_power = mr_aristocrats
				}
			}
			change_variable = { which = added_autonomy		value = 5 }
		}
	}
	else = {
		if = {
			limit = {
				owner = { legitimacy = 95 }
			}
			change_variable = { which = removed_autonomy	value = 8 }
		}
		else_if = {
			limit = {
				owner = { legitimacy = 90 }
			}
			change_variable = { which = removed_autonomy	value = 7.2 }
		}
		else_if = {
			limit = {
				owner = { legitimacy = 80 }
			}
			change_variable = { which = removed_autonomy	value = 6.4 }
		}
		else_if = {
			limit = {
				owner = { legitimacy = 70 }
			}
			change_variable = { which = removed_autonomy	value = 5.6 }
		}
		else_if = {
			limit = {
				owner = { legitimacy = 60 }
			}
			change_variable = { which = removed_autonomy	value = 4.8 }
		}
		else_if = {
			limit = {
				owner = { legitimacy = 50 }
			}
			change_variable = { which = removed_autonomy	value = 4 }
		}
		else_if = {
			limit = {
				owner = { legitimacy = 40 }
			}
			change_variable = { which = removed_autonomy	value = 3.2 }
		}
		else_if = {
			limit = {
				owner = { legitimacy = 30 }
			}
			change_variable = { which = removed_autonomy	value = 2.4 }
		}
		else_if = {
			limit = {
				owner = { legitimacy = 20 }
			}
			change_variable = { which = removed_autonomy	value = 1.6 }
		}
		else_if = {
			limit = {
				owner = { legitimacy = 10 }
			}
			change_variable = { which = removed_autonomy	value = 0.8 }
		}
	}
}
	
centralisation_autonomy_effect = {
	set_variable = { which = absolutismvar			value = 0 }
	owner = {
		PREV = { set_variable = { which = absolutismvar	which = PREV } }
	}
	multiply_variable = { which = absolutismvar		value = 0.6 } #1-60 range, matches -0.6 effect
	change_variable = { which = removed_autonomy	which = absolutismvar }
	set_variable = { which = absolutismvar			value = 0 }
}

estate_autonomy_no_init_effect = {
	if = {
		limit = {
			OR = {
				AND = {
					has_province_flag = tribals_control_province
					NOT = { check_variable = { which = tribals_ratio	value = 0.001 } }
				}
				AND = {
					has_province_flag = tribals_control_province
					check_variable = { which = tribals_ratio	value = 20 }
				}
			}
		}
		change_variable = { which = added_autonomy	value = 50 }
	}
	else = {
		change_variable = { which = added_autonomy	value = 20 }
	}
}

estate_autonomy_init_effect = {
	trigger_switch = {
		on_trigger = has_province_flag
		tribals_control_province = {
			change_variable = { which = added_autonomy	value = 50 }
		}
		greater_nobles_control_province = {
			change_variable = { which = added_autonomy	value = 25 }
			trigger_switch = {
				on_trigger = has_province_modifier
				GN_authority_to_govern_locally_1 = {
					change_variable = { which = added_autonomy	value = 5 }
				}
				GN_authority_to_govern_locally_2 = {
					change_variable = { which = added_autonomy	value = 10 }
				}
			}
		}
		lesser_nobles_control_province = {
			change_variable = { which = added_autonomy	value = 5 }
			trigger_switch = {
				on_trigger = has_province_modifier
				LN_authority_to_govern_locally_1 = {
					change_variable = { which = added_autonomy	value = 5 }
				}
				LN_authority_to_govern_locally_2 = {
					change_variable = { which = added_autonomy	value = 10 }
				}
			}
		}
		burghers_control_province = {
			change_variable = { which = added_autonomy	value = 10 }
			trigger_switch = {
				on_trigger = has_province_modifier
				BG_authority_to_govern_locally_1 = {
					change_variable = { which = added_autonomy	value = 5 }
				}
				BG_authority_to_govern_locally_2 = {
					change_variable = { which = added_autonomy	value = 10 }
				}
			}
		}
		freeholders_control_province = {
			change_variable = { which = added_autonomy	value = 20 }
		}
	}
	if = {
		limit = {
			has_province_modifier = estate_tribal_minority
		}
		change_variable = { which = added_autonomy	value = 25 }
	}
	trigger_switch = {
		on_trigger = has_province_modifier
		tribes_settled_1 = {
			change_variable = { which = removed_autonomy	value = 3.5 }
		}
		tribes_settled_2 = {
			change_variable = { which = removed_autonomy	value = 7 }
		}
		tribes_settled_3 = {
			change_variable = { which = removed_autonomy	value = 10.5 }
		}
		tribes_settled_4 = {
			change_variable = { which = removed_autonomy	value = 14 }
		}
		tribes_settled_5 = {
			change_variable = { which = removed_autonomy	value = 17.5 }
		}
		tribes_settled_6 = {
			change_variable = { which = removed_autonomy	value = 21 }
		}
		tribes_settled_7 = {
			change_variable = { which = removed_autonomy	value = 25 }
		}
	}
}

POP_autonomy_effect = {
	owner = {
		trigger_switch = {
			on_trigger = has_country_modifier
			education_level_illiterate = {
				PREV = { change_variable = { which = removed_autonomy	value = 15 } }
			}
			education_level_mediocre = {
				PREV = { change_variable = { which = removed_autonomy	value = 10 } }
			}
			education_level_poor = {
				PREV = { change_variable = { which = removed_autonomy	value = 5 } }
			}
			education_level_average = {
				PREV = { change_variable = { which = added_autonomy		value = 2.5 } }
			}
			education_level_fair = {
				PREV = { change_variable = { which = added_autonomy		value = 5 } }
			}
			education_level_good = {
				PREV = { change_variable = { which = added_autonomy		value = 7.5 } }
			}
			education_level_high = {
				PREV = { change_variable = { which = added_autonomy		value = 10 } }
			}
			education_level_exceptional = {
				PREV = { change_variable = { which = added_autonomy		value = 12.5 } }
			}
			education_level_enlightened = {
				PREV = { change_variable = { which = added_autonomy		value = 15 } }
			}
		}
	}
	owner = {
		trigger_switch = {
			on_trigger = has_country_modifier
			aristocrats_weak = {
				PREV = { change_variable = { which = added_autonomy		value = 2.5 } }
			}
			aristocrats_capable = {
				PREV = { change_variable = { which = added_autonomy		value = 2.5 } }
			}
			aristocrats_strong = {
				PREV = { change_variable = { which = added_autonomy		value = 5 } }
			}
			aristocrats_powerful = {
				PREV = { change_variable = { which = added_autonomy		value = 5 } }
			}
			aristocrats_dominant = {
				PREV = { change_variable = { which = added_autonomy		value = 5 } }
			}
		}
	}
}

no_CE_autonomy_effect = {
	change_variable = { which = added_autonomy	value = 20 }
}

CE_autonomy_effect = {
	if = { limit = { has_province_modifier = martial_law } # ensures some autonomy is added if martial law is present
		change_variable = { which = added_autonomy	value = 64 }
	}
	if = { limit = { has_province_modifier = CE_Mod_1 }
		change_variable = { which = added_autonomy	value = 1.6 }
	}
	if = { limit = { has_province_modifier = CE_Mod_2 }
		change_variable = { which = added_autonomy	value = 3.2 }
	}
	if = { limit = { has_province_modifier = CE_Mod_4 }
		change_variable = { which = added_autonomy	value = 6.4 }
	}
	if = { limit = { has_province_modifier = CE_Mod_8 }
		change_variable = { which = added_autonomy	value = 12.8 }
	}
	if = { limit = { has_province_modifier = CE_Mod_16 }
		change_variable = { which = added_autonomy	value = 25.6 }
	}
	if = { limit = { has_province_modifier = CE_Mod_32 }
		change_variable = { which = added_autonomy	value = 51.2 }
	}
	if = { limit = { has_province_modifier = CE_Mod_64 }
		change_variable = { which = added_autonomy	value = 102.4 }
	}
	if = { limit = { has_province_modifier = CE_Mod_128 }
		change_variable = { which = added_autonomy	value = 204.8 }
	}
}

sum_autonomy_effect = {
	set_variable = { which = final_autonomy				value = 0 }
	change_variable = { which = final_autonomy			which = added_autonomy }
	subtract_variable = { which = final_autonomy		which = removed_autonomy }
	set_variable = { which = added_autonomy	value = 0 }
	set_variable = { which = removed_autonomy	value = 0 }
}

starting_autonomy_effect = {
	if = {
		limit = {
			owner = { has_country_modifier = fez_political_anarchy }
		}
		change_variable = { which = final_autonomy	value = 25 }
	}
	if = { # Add autonomy for non culture provinces
		limit = {
			has_owner_culture = no
			has_owner_accepted_culture = no
			NOT = { culture_group = owner }
		}
		change_variable = { which = final_autonomy	value = 10 }
	}
	else_if = {
		limit = {
			has_owner_culture = no
			OR = {
				culture_group = owner
				has_owner_accepted_culture = yes
				province_id = 2552 # only culture for gameplay reasons
			}
		}
		change_variable = { which = final_autonomy	value = 5 }
	}
	if = { # Add autonomy for non religion provinces
		limit = {
			NOT = { religion = owner }
			NOT = { religion_group = owner }
		}
		change_variable = { which = final_autonomy	value = 10 }
	}
	else_if = {
		limit = {
			NOT = { religion = owner }
		}
		change_variable = { which = final_autonomy	value = 5 }
	}
	trigger_switch = {
		on_trigger = has_province_flag
		add_local_autonomy_15 = {
			change_variable = { which = final_autonomy	value = 15 }
		}
		add_local_autonomy_25 = {
			change_variable = { which = final_autonomy	value = 25 }
		}
		add_local_autonomy_50 = {
			change_variable = { which = final_autonomy	value = 50 }
		}
		add_local_autonomy_75 = {
			change_variable = { which = final_autonomy	value = 75 }
		}
		add_local_autonomy_100 = {
			change_variable = { which = final_autonomy	value = 100 }
		}
	}
	trigger_switch = {
		on_trigger = has_province_flag
		subtract_local_autonomy_15 = {
			subtract_variable = { which = final_autonomy	value = 15 }
		}
		subtract_local_autonomy_25 = {
			subtract_variable = { which = final_autonomy	value = 25 }
		}
		subtract_local_autonomy_50 = {
			subtract_variable = { which = final_autonomy	value = 50 }
		}
		subtract_local_autonomy_75 = {
			subtract_variable = { which = final_autonomy	value = 75 }
		}
		subtract_local_autonomy_100 = {
			subtract_variable = { which = final_autonomy	value = 100 }
		}
	}
}

final_autonomy_effect = {
	if = { #Cap autonomy at 100
		limit = {
			check_variable = { which = final_autonomy	value = 100 }
		}
		set_variable = { which = final_autonomy	value = 100 }
	}
	if = {
		limit = {
			check_variable = { which = final_autonomy	value = 64 }
		}
		subtract_variable = { which = final_autonomy	value = 64 }
		add_local_autonomy = 64
	}
	if = {
		limit = {
			check_variable = { which = final_autonomy	value = 32 }
		}
		subtract_variable = { which = final_autonomy	value = 32 }
		add_local_autonomy = 32
	}
	if = {
		limit = {
			check_variable = { which = final_autonomy	value = 16 }
		}
		subtract_variable = { which = final_autonomy	value = 16 }
		add_local_autonomy = 16
	}
	if = {
		limit = {
			check_variable = { which = final_autonomy	value = 8 }
		}
		subtract_variable = { which = final_autonomy	value = 8 }
		add_local_autonomy = 8
	}
	if = {
		limit = {
			check_variable = { which = final_autonomy	value = 4 }
		}
		subtract_variable = { which = final_autonomy	value = 4 }
		add_local_autonomy = 4
	}
	if = {
		limit = {
			check_variable = { which = final_autonomy	value = 2 }
		}
		subtract_variable = { which = final_autonomy	value = 2 }
		add_local_autonomy = 2
	}
	if = {
		limit = {
			check_variable = { which = final_autonomy	value = 1 }
		}
		subtract_variable = { which = final_autonomy	value = 1 }
		add_local_autonomy = 1
	}
	if = {
		limit = {
			check_variable = { which = final_autonomy	value = 0.75 }
		}
		subtract_variable = { which = final_autonomy	value = 0.75 }
		add_local_autonomy = 0.7
	}
	if = {
		limit = {
			check_variable = { which = final_autonomy	value = 0.5 }
		}
		subtract_variable = { which = final_autonomy	value = 0.5 }
		add_local_autonomy = 0.5
	}
	if = {
		limit = {
			check_variable = { which = final_autonomy	value = 0.25 }
		}
		subtract_variable = { which = final_autonomy	value = 0.25 }
		add_local_autonomy = 0.2
	}
	if = {
		limit = {
			check_variable = { which = final_autonomy	value = 0.1 }
		}
		subtract_variable = { which = final_autonomy	value = 0.1 }
		add_local_autonomy = 0.1
	}
			
	set_variable = { which = final_autonomy	value = 0 }
}