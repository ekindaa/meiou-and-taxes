scaled_ducat_cost_country = {
	while = { limit = { check_variable = { which = ducat_cost value = 65536 } } 	add_treasury = -65536 		subtract_variable = { which = ducat_cost value = 65536 } }
	if = { limit = { check_variable = { which = ducat_cost value = 32768 } } 	add_treasury = -32768 		subtract_variable = { which = ducat_cost value = 32768 } }
	if = { limit = { check_variable = { which = ducat_cost value = 16384 } } 	add_treasury = -16384 		subtract_variable = { which = ducat_cost value = 16384 } }
	if = { limit = { check_variable = { which = ducat_cost value = 8192 } } 	add_treasury = -8192 		subtract_variable = { which = ducat_cost value = 8192 } }
	if = { limit = { check_variable = { which = ducat_cost value = 4096 } } 	add_treasury = -4096 		subtract_variable = { which = ducat_cost value = 4096 } }
	if = { limit = { check_variable = { which = ducat_cost value = 2048 } } 	add_treasury = -2048 		subtract_variable = { which = ducat_cost value = 2048 } }
	if = { limit = { check_variable = { which = ducat_cost value = 1024 } } 	add_treasury = -1024 		subtract_variable = { which = ducat_cost value = 1024 } }
	if = { limit = { check_variable = { which = ducat_cost value = 512 } } 		add_treasury = -512 		subtract_variable = { which = ducat_cost value = 512 } }
	if = { limit = { check_variable = { which = ducat_cost value = 256 } } 		add_treasury = -256 		subtract_variable = { which = ducat_cost value = 256 } }
	if = { limit = { check_variable = { which = ducat_cost value = 128 } } 		add_treasury = -128 		subtract_variable = { which = ducat_cost value = 128 } }
	if = { limit = { check_variable = { which = ducat_cost value = 64 } } 		add_treasury = -64 			subtract_variable = { which = ducat_cost value = 64 } }
	if = { limit = { check_variable = { which = ducat_cost value = 32 } } 		add_treasury = -32 			subtract_variable = { which = ducat_cost value = 32 } }
	if = { limit = { check_variable = { which = ducat_cost value = 16 } } 		add_treasury = -16 			subtract_variable = { which = ducat_cost value = 16 } }
	if = { limit = { check_variable = { which = ducat_cost value = 8 } } 		add_treasury = -8 			subtract_variable = { which = ducat_cost value = 8 } }
	if = { limit = { check_variable = { which = ducat_cost value = 4 } } 		add_treasury = -4 			subtract_variable = { which = ducat_cost value = 4 } }
	if = { limit = { check_variable = { which = ducat_cost value = 2 } } 		add_treasury = -2 			subtract_variable = { which = ducat_cost value = 2 } }
	if = { limit = { check_variable = { which = ducat_cost value = 1 } } 		add_treasury = -1 			subtract_variable = { which = ducat_cost value = 1 } }
	set_variable = { which = ducat_cost	value = 0 }
}

scaled_ducat_gained_country = {
	while = { limit = { check_variable = { which = ducat_gain value = 65536 } } 	add_treasury = 65536 		subtract_variable = { which = ducat_gain value = 65536 } }
	if = { limit = { check_variable = { which = ducat_gain value = 32768 } } 	add_treasury = 32768 		subtract_variable = { which = ducat_gain value = 32768 } }
	if = { limit = { check_variable = { which = ducat_gain value = 16384 } } 	add_treasury = 16384 		subtract_variable = { which = ducat_gain value = 16384 } }
	if = { limit = { check_variable = { which = ducat_gain value = 8192 } } 	add_treasury = 8192 		subtract_variable = { which = ducat_gain value = 8192 } }
	if = { limit = { check_variable = { which = ducat_gain value = 4096 } } 	add_treasury = 4096 		subtract_variable = { which = ducat_gain value = 4096 } }
	if = { limit = { check_variable = { which = ducat_gain value = 2048 } } 	add_treasury = 2048 		subtract_variable = { which = ducat_gain value = 2048 } }
	if = { limit = { check_variable = { which = ducat_gain value = 1024 } } 	add_treasury = 1024 		subtract_variable = { which = ducat_gain value = 1024 } }
	if = { limit = { check_variable = { which = ducat_gain value = 512 } } 		add_treasury = 512 			subtract_variable = { which = ducat_gain value = 512 } }
	if = { limit = { check_variable = { which = ducat_gain value = 256 } } 		add_treasury = 256 			subtract_variable = { which = ducat_gain value = 256 } }
	if = { limit = { check_variable = { which = ducat_gain value = 128 } } 		add_treasury = 128 			subtract_variable = { which = ducat_gain value = 128 } }
	if = { limit = { check_variable = { which = ducat_gain value = 64 } } 		add_treasury = 64 			subtract_variable = { which = ducat_gain value = 64 } }
	if = { limit = { check_variable = { which = ducat_gain value = 32 } } 		add_treasury = 32 			subtract_variable = { which = ducat_gain value = 32 } }
	if = { limit = { check_variable = { which = ducat_gain value = 16 } } 		add_treasury = 16 			subtract_variable = { which = ducat_gain value = 16 } }
	if = { limit = { check_variable = { which = ducat_gain value = 8 } } 		add_treasury = 8 			subtract_variable = { which = ducat_gain value = 8 } }
	if = { limit = { check_variable = { which = ducat_gain value = 4 } } 		add_treasury = 4 			subtract_variable = { which = ducat_gain value = 4 } }
	if = { limit = { check_variable = { which = ducat_gain value = 2 } } 		add_treasury = 2 			subtract_variable = { which = ducat_gain value = 2 } }
	if = { limit = { check_variable = { which = ducat_gain value = 1 } } 		add_treasury = 1 			subtract_variable = { which = ducat_gain value = 1 } }
	set_variable = { which = ducat_gain	value = 0 }
}

scaled_manpower_cost_country = {
	while = { limit = { check_variable = { which = manpower_cost value = 65536 } } 	add_manpower = -65536 		subtract_variable = { which = manpower_cost value = 65536 } }
	if = { limit = { check_variable = { which = manpower_cost value = 32768 } } 	add_manpower = -32768 		subtract_variable = { which = manpower_cost value = 32768 } }
	if = { limit = { check_variable = { which = manpower_cost value = 16384 } } 	add_manpower = -16384 		subtract_variable = { which = manpower_cost value = 16384 } }
	if = { limit = { check_variable = { which = manpower_cost value = 8192 } } 		add_manpower = -8192 		subtract_variable = { which = manpower_cost value = 8192 } }
	if = { limit = { check_variable = { which = manpower_cost value = 4096 } } 		add_manpower = -4096 		subtract_variable = { which = manpower_cost value = 4096 } }
	if = { limit = { check_variable = { which = manpower_cost value = 2048 } } 		add_manpower = -2048 		subtract_variable = { which = manpower_cost value = 2048 } }
	if = { limit = { check_variable = { which = manpower_cost value = 1024 } } 		add_manpower = -1024 		subtract_variable = { which = manpower_cost value = 1024 } }
	if = { limit = { check_variable = { which = manpower_cost value = 512 } } 		add_manpower = -512 		subtract_variable = { which = manpower_cost value = 512 } }
	if = { limit = { check_variable = { which = manpower_cost value = 256 } } 		add_manpower = -256 		subtract_variable = { which = manpower_cost value = 256 } }
	if = { limit = { check_variable = { which = manpower_cost value = 128 } } 		add_manpower = -128 		subtract_variable = { which = manpower_cost value = 128 } }
	if = { limit = { check_variable = { which = manpower_cost value = 64 } } 		add_manpower = -64 			subtract_variable = { which = manpower_cost value = 64 } }
	if = { limit = { check_variable = { which = manpower_cost value = 32 } } 		add_manpower = -32 			subtract_variable = { which = manpower_cost value = 32 } }
	if = { limit = { check_variable = { which = manpower_cost value = 16 } } 		add_manpower = -16 			subtract_variable = { which = manpower_cost value = 16 } }
	if = { limit = { check_variable = { which = manpower_cost value = 8 } } 		add_manpower = -8 			subtract_variable = { which = manpower_cost value = 8 } }
	if = { limit = { check_variable = { which = manpower_cost value = 4 } } 		add_manpower = -4 			subtract_variable = { which = manpower_cost value = 4 } }
	if = { limit = { check_variable = { which = manpower_cost value = 2 } } 		add_manpower = -2 			subtract_variable = { which = manpower_cost value = 2 } }
	if = { limit = { check_variable = { which = manpower_cost value = 1 } } 		add_manpower = -1 			subtract_variable = { which = manpower_cost value = 1 } }
	set_variable = { which = manpower_cost	value = 0 }
}

scaled_manpower_gained_country = {
	while = { limit = { check_variable = { which = manpower_gain value = 65536 } } 	add_manpower = 65536 		subtract_variable = { which = manpower_gain value = 65536 } }
	if = { limit = { check_variable = { which = manpower_gain value = 32768 } } 	add_manpower = 32768 		subtract_variable = { which = manpower_gain value = 32768 } }
	if = { limit = { check_variable = { which = manpower_gain value = 16384 } } 	add_manpower = 16384 		subtract_variable = { which = manpower_gain value = 16384 } }
	if = { limit = { check_variable = { which = manpower_gain value = 8192 } } 		add_manpower = 8192 		subtract_variable = { which = manpower_gain value = 8192 } }
	if = { limit = { check_variable = { which = manpower_gain value = 4096 } } 		add_manpower = 4096 		subtract_variable = { which = manpower_gain value = 4096 } }
	if = { limit = { check_variable = { which = manpower_gain value = 2048 } } 		add_manpower = 2048 		subtract_variable = { which = manpower_gain value = 2048 } }
	if = { limit = { check_variable = { which = manpower_gain value = 1024 } } 		add_manpower = 1024 		subtract_variable = { which = manpower_gain value = 1024 } }
	if = { limit = { check_variable = { which = manpower_gain value = 512 } } 		add_manpower = 512 			subtract_variable = { which = manpower_gain value = 512 } }
	if = { limit = { check_variable = { which = manpower_gain value = 256 } } 		add_manpower = 256 			subtract_variable = { which = manpower_gain value = 256 } }
	if = { limit = { check_variable = { which = manpower_gain value = 128 } } 		add_manpower = 128 			subtract_variable = { which = manpower_gain value = 128 } }
	if = { limit = { check_variable = { which = manpower_gain value = 64 } } 		add_manpower = 64 			subtract_variable = { which = manpower_gain value = 64 } }
	if = { limit = { check_variable = { which = manpower_gain value = 32 } } 		add_manpower = 32 			subtract_variable = { which = manpower_gain value = 32 } }
	if = { limit = { check_variable = { which = manpower_gain value = 16 } } 		add_manpower = 16 			subtract_variable = { which = manpower_gain value = 16 } }
	if = { limit = { check_variable = { which = manpower_gain value = 8 } } 		add_manpower = 8 			subtract_variable = { which = manpower_gain value = 8 } }
	if = { limit = { check_variable = { which = manpower_gain value = 4 } } 		add_manpower = 4 			subtract_variable = { which = manpower_gain value = 4 } }
	if = { limit = { check_variable = { which = manpower_gain value = 2 } } 		add_manpower = 2 			subtract_variable = { which = manpower_gain value = 2 } }
	if = { limit = { check_variable = { which = manpower_gain value = 1 } } 		add_manpower = 1 			subtract_variable = { which = manpower_gain value = 1 } }
	set_variable = { which = manpower_gain	value = 0 }
}

scaled_admin_cost_country = {
	while = { limit = { check_variable = { which = admin_cost value = 1024 } } 	add_adm_power = -1024 		subtract_variable = { which = admin_cost value = 1024 } }
	if = { limit = { check_variable = { which = admin_cost value = 512 } } 		add_adm_power = -512 		subtract_variable = { which = admin_cost value = 512 } }
	if = { limit = { check_variable = { which = admin_cost value = 256 } } 		add_adm_power = -256 		subtract_variable = { which = admin_cost value = 256 } }
	if = { limit = { check_variable = { which = admin_cost value = 128 } } 		add_adm_power = -128 		subtract_variable = { which = admin_cost value = 128 } }
	if = { limit = { check_variable = { which = admin_cost value = 64 } } 		add_adm_power = -64 		subtract_variable = { which = admin_cost value = 64 } }
	if = { limit = { check_variable = { which = admin_cost value = 32 } } 		add_adm_power = -32 		subtract_variable = { which = admin_cost value = 32 } }
	if = { limit = { check_variable = { which = admin_cost value = 16 } } 		add_adm_power = -16 		subtract_variable = { which = admin_cost value = 16 } }
	if = { limit = { check_variable = { which = admin_cost value = 8 } } 		add_adm_power = -8 			subtract_variable = { which = admin_cost value = 8 } }
	if = { limit = { check_variable = { which = admin_cost value = 4 } } 		add_adm_power = -4 			subtract_variable = { which = admin_cost value = 4 } }
	if = { limit = { check_variable = { which = admin_cost value = 2 } } 		add_adm_power = -2 			subtract_variable = { which = admin_cost value = 2 } }
	if = { limit = { check_variable = { which = admin_cost value = 1 } } 		add_adm_power = -1 			subtract_variable = { which = admin_cost value = 1 } }
	set_variable = { which = admin_cost	value = 0 }
}

scaled_diplo_cost_country = {
	while = { limit = { check_variable = { which = diplo_cost value = 1024 } } 	add_dip_power = -1024 		subtract_variable = { which = diplo_cost value = 1024 } }
	if = { limit = { check_variable = { which = diplo_cost value = 512 } } 		add_dip_power = -512 		subtract_variable = { which = diplo_cost value = 512 } }
	if = { limit = { check_variable = { which = diplo_cost value = 256 } } 		add_dip_power = -256 		subtract_variable = { which = diplo_cost value = 256 } }
	if = { limit = { check_variable = { which = diplo_cost value = 128 } } 		add_dip_power = -128 		subtract_variable = { which = diplo_cost value = 128 } }
	if = { limit = { check_variable = { which = diplo_cost value = 64 } } 		add_dip_power = -64 		subtract_variable = { which = diplo_cost value = 64 } }
	if = { limit = { check_variable = { which = diplo_cost value = 32 } } 		add_dip_power = -32 		subtract_variable = { which = diplo_cost value = 32 } }
	if = { limit = { check_variable = { which = diplo_cost value = 16 } } 		add_dip_power = -16 		subtract_variable = { which = diplo_cost value = 16 } }
	if = { limit = { check_variable = { which = diplo_cost value = 8 } } 		add_dip_power = -8 			subtract_variable = { which = diplo_cost value = 8 } }
	if = { limit = { check_variable = { which = diplo_cost value = 4 } } 		add_dip_power = -4 			subtract_variable = { which = diplo_cost value = 4 } }
	if = { limit = { check_variable = { which = diplo_cost value = 2 } } 		add_dip_power = -2 			subtract_variable = { which = diplo_cost value = 2 } }
	if = { limit = { check_variable = { which = diplo_cost value = 1 } } 		add_dip_power = -1 			subtract_variable = { which = diplo_cost value = 1 } }
	set_variable = { which = diplo_cost	value = 0 }
}

scaled_milit_cost_country = {
	while = { limit = { check_variable = { which = milit_cost value = 1024 } } 	add_mil_power = -1024 		subtract_variable = { which = milit_cost value = 1024 } }
	if = { limit = { check_variable = { which = milit_cost value = 512 } } 		add_mil_power = -512 		subtract_variable = { which = milit_cost value = 512 } }
	if = { limit = { check_variable = { which = milit_cost value = 256 } } 		add_mil_power = -256 		subtract_variable = { which = milit_cost value = 256 } }
	if = { limit = { check_variable = { which = milit_cost value = 128 } } 		add_mil_power = -128 		subtract_variable = { which = milit_cost value = 128 } }
	if = { limit = { check_variable = { which = milit_cost value = 64 } } 		add_mil_power = -64 		subtract_variable = { which = milit_cost value = 64 } }
	if = { limit = { check_variable = { which = milit_cost value = 32 } } 		add_mil_power = -32 		subtract_variable = { which = milit_cost value = 32 } }
	if = { limit = { check_variable = { which = milit_cost value = 16 } } 		add_mil_power = -16 		subtract_variable = { which = milit_cost value = 16 } }
	if = { limit = { check_variable = { which = milit_cost value = 8 } } 		add_mil_power = -8 			subtract_variable = { which = milit_cost value = 8 } }
	if = { limit = { check_variable = { which = milit_cost value = 4 } } 		add_mil_power = -4 			subtract_variable = { which = milit_cost value = 4 } }
	if = { limit = { check_variable = { which = milit_cost value = 2 } } 		add_mil_power = -2 			subtract_variable = { which = milit_cost value = 2 } }
	if = { limit = { check_variable = { which = milit_cost value = 1 } } 		add_mil_power = -1 			subtract_variable = { which = milit_cost value = 1 } }
	set_variable = { which = milit_cost	value = 0 }
}

adm_available = {
	trigger_switch = {
		on_trigger = adm_power
		550 = {
			trigger_switch = {
				on_trigger = adm_power
				800 = {
					trigger_switch = {
						on_trigger = adm_power
						900 = {
							trigger_switch = {
								on_trigger = adm_power
								1000 = { set_variable = { which = adm_power	value = 1050 } }
								950 = { set_variable = { which = adm_power	value = 1000 } }
								900 = { set_variable = { which = adm_power	value = 950 } }
							}
						}
						800 = {
							trigger_switch = {
								on_trigger = adm_power
								850 = { set_variable = { which = adm_power	value = 900 } }
								800 = { set_variable = { which = adm_power	value = 850 } }
							}
						}
					}
				}
				550 = {
					trigger_switch = {
						on_trigger = adm_power
						650 = {
							trigger_switch = {
								on_trigger = adm_power
								750 = { set_variable = { which = adm_power	value = 800 } }
								700 = { set_variable = { which = adm_power	value = 750 } }
								650 = { set_variable = { which = adm_power	value = 700 } }
							}
						}
						550 = {
							trigger_switch = {
								on_trigger = adm_power
								600 = { set_variable = { which = adm_power	value = 650 } }
								550 = { set_variable = { which = adm_power	value = 600 } }
							}
						}
					}
				}
			}
		}
		50 = {
			trigger_switch = {
				on_trigger = adm_power
				300 = {
					trigger_switch = {
						on_trigger = adm_power
						400 = {
							trigger_switch = {
								on_trigger = adm_power
								500 = { set_variable = { which = adm_power	value = 550 } }
								450 = { set_variable = { which = adm_power	value = 500 } }
								400 = { set_variable = { which = adm_power	value = 450 } }
							}
						}
						300 = {
							trigger_switch = {
								on_trigger = adm_power
								350 = { set_variable = { which = adm_power	value = 400 } }
								300 = { set_variable = { which = adm_power	value = 350 } }
							}
						}
					}
				}
				50 = {
					trigger_switch = {
						on_trigger = adm_power
						150 = {
							trigger_switch = {
								on_trigger = adm_power
								250 = { set_variable = { which = adm_power	value = 300 } }
								200 = { set_variable = { which = adm_power	value = 250 } }
								150 = { set_variable = { which = adm_power	value = 200 } }
							}
						}
						50 = {
							trigger_switch = {
								on_trigger = adm_power
								100 = { set_variable = { which = adm_power	value = 150 } }
								50 = { set_variable = { which = adm_power	value = 100 } }
							}
						}
					}
				}
			}
		}
	}
}

mil_available = {
	trigger_switch = {
		on_trigger = mil_power
		550 = {
			trigger_switch = {
				on_trigger = mil_power
				800 = {
					trigger_switch = {
						on_trigger = mil_power
						900 = {
							trigger_switch = {
								on_trigger = mil_power
								1000 = { set_variable = { which = mil_power	value = 1050 } }
								950 = { set_variable = { which = mil_power	value = 1000 } }
								900 = { set_variable = { which = mil_power	value = 950 } }
							}
						}
						800 = {
							trigger_switch = {
								on_trigger = mil_power
								850 = { set_variable = { which = mil_power	value = 900 } }
								800 = { set_variable = { which = mil_power	value = 850 } }
							}
						}
					}
				}
				550 = {
					trigger_switch = {
						on_trigger = mil_power
						650 = {
							trigger_switch = {
								on_trigger = mil_power
								750 = { set_variable = { which = mil_power	value = 800 } }
								700 = { set_variable = { which = mil_power	value = 750 } }
								650 = { set_variable = { which = mil_power	value = 700 } }
							}
						}
						550 = {
							trigger_switch = {
								on_trigger = mil_power
								600 = { set_variable = { which = mil_power	value = 650 } }
								550 = { set_variable = { which = mil_power	value = 600 } }
							}
						}
					}
				}
			}
		}
		50 = {
			trigger_switch = {
				on_trigger = mil_power
				300 = {
					trigger_switch = {
						on_trigger = mil_power
						400 = {
							trigger_switch = {
								on_trigger = mil_power
								500 = { set_variable = { which = mil_power	value = 550 } }
								450 = { set_variable = { which = mil_power	value = 500 } }
								400 = { set_variable = { which = mil_power	value = 450 } }
							}
						}
						300 = {
							trigger_switch = {
								on_trigger = mil_power
								350 = { set_variable = { which = mil_power	value = 400 } }
								300 = { set_variable = { which = mil_power	value = 350 } }
							}
						}
					}
				}
				50 = {
					trigger_switch = {
						on_trigger = mil_power
						150 = {
							trigger_switch = {
								on_trigger = mil_power
								250 = { set_variable = { which = mil_power	value = 300 } }
								200 = { set_variable = { which = mil_power	value = 250 } }
								150 = { set_variable = { which = mil_power	value = 200 } }
							}
						}
						50 = {
							trigger_switch = {
								on_trigger = mil_power
								100 = { set_variable = { which = mil_power	value = 150 } }
								50 = { set_variable = { which = mil_power	value = 100 } }
							}
						}
					}
				}
			}
		}
	}
}

dip_available = {
	trigger_switch = {
		on_trigger = dip_power
		550 = {
			trigger_switch = {
				on_trigger = dip_power
				800 = {
					trigger_switch = {
						on_trigger = dip_power
						900 = {
							trigger_switch = {
								on_trigger = dip_power
								1000 = { set_variable = { which = dip_power	value = 1050 } }
								950 = { set_variable = { which = dip_power	value = 1000 } }
								900 = { set_variable = { which = dip_power	value = 950 } }
							}
						}
						800 = {
							trigger_switch = {
								on_trigger = dip_power
								850 = { set_variable = { which = dip_power	value = 900 } }
								800 = { set_variable = { which = dip_power	value = 850 } }
							}
						}
					}
				}
				550 = {
					trigger_switch = {
						on_trigger = dip_power
						650 = {
							trigger_switch = {
								on_trigger = dip_power
								750 = { set_variable = { which = dip_power	value = 800 } }
								700 = { set_variable = { which = dip_power	value = 750 } }
								650 = { set_variable = { which = dip_power	value = 700 } }
							}
						}
						550 = {
							trigger_switch = {
								on_trigger = dip_power
								600 = { set_variable = { which = dip_power	value = 650 } }
								550 = { set_variable = { which = dip_power	value = 600 } }
							}
						}
					}
				}
			}
		}
		50 = {
			trigger_switch = {
				on_trigger = dip_power
				300 = {
					trigger_switch = {
						on_trigger = dip_power
						400 = {
							trigger_switch = {
								on_trigger = dip_power
								500 = { set_variable = { which = dip_power	value = 550 } }
								450 = { set_variable = { which = dip_power	value = 500 } }
								400 = { set_variable = { which = dip_power	value = 450 } }
							}
						}
						300 = {
							trigger_switch = {
								on_trigger = dip_power
								350 = { set_variable = { which = dip_power	value = 400 } }
								300 = { set_variable = { which = dip_power	value = 350 } }
							}
						}
					}
				}
				50 = {
					trigger_switch = {
						on_trigger = dip_power
						150 = {
							trigger_switch = {
								on_trigger = dip_power
								250 = { set_variable = { which = dip_power	value = 300 } }
								200 = { set_variable = { which = dip_power	value = 250 } }
								150 = { set_variable = { which = dip_power	value = 200 } }
							}
						}
						50 = {
							trigger_switch = {
								on_trigger = dip_power
								100 = { set_variable = { which = dip_power	value = 150 } }
								50 = { set_variable = { which = dip_power	value = 100 } }
							}
						}
					}
				}
			}
		}
	}
}

stability_loss_compound_a = {
	if = {
		limit = {
			check_variable = { which = stability_loss_a 			value = 0.01 }
			NOT = { check_variable = { which = stability_loss_a		value = 24 }	}
		}
		random = {	chance = 25		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_a 			value = 24 }
			NOT = { check_variable = { which = stability_loss_a		value = 48 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_a 			value = 48 }
			NOT = { check_variable = { which = stability_loss_a		value = 72 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 25		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_a 			value = 72 }
			NOT = { check_variable = { which = stability_loss_a		value = 96 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_a 			value = 96 }
			NOT = { check_variable = { which = stability_loss_a		value = 120 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 25		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_a 			value = 120 }
			NOT = { check_variable = { which = stability_loss_a		value = 144 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_a 			value = 144 }
			NOT = { check_variable = { which = stability_loss_a		value = 168 }	}
		}
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_a 			value = 168 }
			NOT = { check_variable = { which = stability_loss_a		value = 192 }	}
		}
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_a 			value = 192 }
			NOT = { check_variable = { which = stability_loss_a		value = 216 }	}
		}
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_a 			value = 216 }
		}
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 25		 subtract_stability_1 = yes }
	}
}

stability_loss_compound_b = {
	if = {
		limit = {
			check_variable = { which = stability_loss_b 			value = 0.01 }
			NOT = { check_variable = { which = stability_loss_b		value = 24 }	}
		}
		random = {	chance = 25		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_b 			value = 24 }
			NOT = { check_variable = { which = stability_loss_b		value = 48 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_b 			value = 48 }
			NOT = { check_variable = { which = stability_loss_b		value = 72 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 25		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_b 			value = 72 }
			NOT = { check_variable = { which = stability_loss_b		value = 96 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_b 			value = 96 }
			NOT = { check_variable = { which = stability_loss_b		value = 120 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 25		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_b 			value = 120 }
			NOT = { check_variable = { which = stability_loss_b		value = 144 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_b 			value = 144 }
			NOT = { check_variable = { which = stability_loss_b		value = 168 }	}
		}
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_b 			value = 168 }
			NOT = { check_variable = { which = stability_loss_b		value = 192 }	}
		}
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_b 			value = 192 }
			NOT = { check_variable = { which = stability_loss_b		value = 216 }	}
		}
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_b 			value = 216 }
		}
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 25		 subtract_stability_1 = yes }
	}
}

stability_loss_compound_c = {
	if = {
		limit = {
			check_variable = { which = stability_loss_c 			value = 0.01 }
			NOT = { check_variable = { which = stability_loss_c		value = 24 }	}
		}
		random = {	chance = 25		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_c 			value = 24 }
			NOT = { check_variable = { which = stability_loss_c		value = 48 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_c 			value = 48 }
			NOT = { check_variable = { which = stability_loss_c		value = 72 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 25		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_c 			value = 72 }
			NOT = { check_variable = { which = stability_loss_c		value = 96 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_c 			value = 96 }
			NOT = { check_variable = { which = stability_loss_c		value = 120 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 25		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_c 			value = 120 }
			NOT = { check_variable = { which = stability_loss_c		value = 144 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_c 			value = 144 }
			NOT = { check_variable = { which = stability_loss_c		value = 168 }	}
		}
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_c 			value = 168 }
			NOT = { check_variable = { which = stability_loss_c		value = 192 }	}
		}
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_c 			value = 192 }
			NOT = { check_variable = { which = stability_loss_c		value = 216 }	}
		}
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_c 			value = 216 }
		}
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 25		 subtract_stability_1 = yes }
	}
}

stability_loss_compound_d = {
	if = {
		limit = {
			check_variable = { which = stability_loss_d 			value = 0.01 }
			NOT = { check_variable = { which = stability_loss_d		value = 24 }	}
		}
		random = {	chance = 25		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_d 			value = 24 }
			NOT = { check_variable = { which = stability_loss_d		value = 48 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_d 			value = 48 }
			NOT = { check_variable = { which = stability_loss_d		value = 72 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 25		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_d 			value = 72 }
			NOT = { check_variable = { which = stability_loss_d		value = 96 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_d 			value = 96 }
			NOT = { check_variable = { which = stability_loss_d		value = 120 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 25		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_d 			value = 120 }
			NOT = { check_variable = { which = stability_loss_d		value = 144 }	}
		}
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_d 			value = 144 }
			NOT = { check_variable = { which = stability_loss_d		value = 168 }	}
		}
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_d 			value = 168 }
			NOT = { check_variable = { which = stability_loss_d		value = 192 }	}
		}
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 50		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_d 			value = 192 }
			NOT = { check_variable = { which = stability_loss_d		value = 216 }	}
		}
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
	}
	if = {
		limit = {
			check_variable = { which = stability_loss_d 			value = 216 }
		}
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 75		 subtract_stability_1 = yes }
		random = {	chance = 25		 subtract_stability_1 = yes }
	}
}

scaled_autonomy_added_province = {
	while = { limit = { check_variable = { which = autonomy_added value = 32 } } 		add_local_autonomy = 32		subtract_variable = { which = autonomy_added value = 32 } }
	if = { limit = { check_variable = { which = autonomy_added value = 16 } } 		add_local_autonomy = 16		subtract_variable = { which = autonomy_added value = 16 } }
	if = { limit = { check_variable = { which = autonomy_added value = 8 } } 		add_local_autonomy = 8		subtract_variable = { which = autonomy_added value = 8 } }
	if = { limit = { check_variable = { which = autonomy_added value = 4 } } 		add_local_autonomy = 4		subtract_variable = { which = autonomy_added value = 4 } }
	if = { limit = { check_variable = { which = autonomy_added value = 2 } } 		add_local_autonomy = 2		subtract_variable = { which = autonomy_added value = 2 } }
	if = { limit = { check_variable = { which = autonomy_added value = 1 } } 		add_local_autonomy = 1		subtract_variable = { which = autonomy_added value = 1 } }
	set_variable = { which = autonomy_added 	value = 0 }
}