castille_shakes_down_granada = {
	
}

corruption_stop_gap = {
	
}

free_loyalty = {
	
}

recent_student_riot = { }

country_development_1 = {
	build_cost = -0.2
	
	land_morale = -0.1
	naval_morale = -0.1
	siege_ability = -0.1
	defensiveness = -0.1
	discipline = -0.05
	land_forcelimit_modifier = -0.15
	
	global_trade_power = -0.05
	
	technology_cost = 0.05
	picture = "MAPMODE_city_infrastructure_level_1"
}

country_development_2 = {
	build_cost = -0.1
	
	land_morale = -0.05
	naval_morale = -0.05
	siege_ability = -0.05
	defensiveness = -0.05
	discipline = -0.025
	land_forcelimit_modifier = -0.10
	
	global_trade_power = -0.025
	
	technology_cost = 0.025
	picture = "MAPMODE_city_infrastructure_level_2"
}

country_development_3 = {
	picture = "MAPMODE_city_infrastructure_level_3"
	defensiveness = 0.025
}

country_development_4 = {
	build_cost = 0.2
	
	land_morale = 0.025
	naval_morale = 0.025
	siege_ability = 0.025
	defensiveness = 0.025
	discipline = 0.012
	land_forcelimit_modifier = 0.10
	
	global_trade_power = 0.025
	
	technology_cost = -0.025
	picture = "MAPMODE_city_infrastructure_level_4"
}

country_development_5 = {
	build_cost = 0.4
	
	land_morale = 0.05
	naval_morale = 0.05
	siege_ability = 0.05
	defensiveness = 0.05
	discipline = 0.025
	land_forcelimit_modifier = 0.15
	
	global_trade_power = 0.05
	
	technology_cost = -0.05
	picture = "MAPMODE_city_infrastructure_level_5"
}
country_development_6 = {
	build_cost = 0.6
	
	land_morale = 0.1
	naval_morale = 0.1
	siege_ability = 0.1
	defensiveness = 0.1
	discipline = 0.05
	land_forcelimit_modifier = 0.20
	
	global_trade_power = 0.1
	
	technology_cost = -0.1
	picture = "MAPMODE_city_infrastructure_level_6"
}
country_development_7 = {
	build_cost = 0.8
	
	land_morale = 0.125
	naval_morale = 0.125
	siege_ability = 0.125
	defensiveness = 0.125
	discipline = 0.075
	land_forcelimit_modifier = 0.25
	
	global_trade_power = 0.15
	
	technology_cost = -0.125
	picture = "MAPMODE_city_infrastructure_level_8"
	
}
country_development_8 = {
	build_cost = 1
	
	land_morale = 0.15
	naval_morale = 0.15
	siege_ability = 0.15
	defensiveness = 0.15
	discipline = 0.1
	land_forcelimit_modifier = 0.30
	
	global_trade_power = 0.2
	
	technology_cost = -0.15
	
	picture = "MAPMODE_city_infrastructure_level_11"
}

education_level_illiterate = {
	technology_cost = 0.15
	idea_cost = 0.10
	army_tradition_decay = 0.015
	navy_tradition_decay = 0.015
	global_institution_spread = 0
	
	stability_cost_modifier = -0.1
	global_autonomy = -0.15
	picture = "MAPMODE_education_level_1"
}

education_level_mediocre = {
	technology_cost = 0.1
	idea_cost = 0.075
	army_tradition_decay = 0.010
	navy_tradition_decay = 0.010
	global_institution_spread = 0.10
	
	stability_cost_modifier = -0.05
	global_autonomy = -0.10
	picture = "MAPMODE_education_level_2"
}

education_level_poor = {
	technology_cost = 0.05
	idea_cost = 0.05
	army_tradition_decay = 0.005
	navy_tradition_decay = 0.005
	global_institution_spread = 0.20
	
	stability_cost_modifier = -0.025
	global_autonomy = -0.05
	picture = "MAPMODE_education_level_3"
}

education_level_average = {
	stability_cost_modifier = -0.025
	global_institution_spread = 0.30
	global_autonomy = 0.025
	picture = "MAPMODE_education_level_4"
}


education_level_fair = {
	technology_cost = -0.05
	idea_cost = -0.025
	army_tradition_decay = -0.005
	navy_tradition_decay = -0.005
	global_institution_spread = 0.40
	
	stability_cost_modifier = 0.05
	global_autonomy = 0.05
	picture = "MAPMODE_education_level_5"
}

education_level_good = {
	technology_cost = -0.1
	idea_cost = -0.05
	army_tradition_decay = -0.010
	navy_tradition_decay = -0.010
	global_institution_spread = 0.50
	advisor_pool = 1
	stability_cost_modifier = 0.10
	global_autonomy = 0.075
	picture = "MAPMODE_education_level_6"
}

education_level_high = {
	technology_cost = -0.15
	idea_cost = -0.075
	army_tradition_decay = -0.015
	navy_tradition_decay = -0.015
	global_institution_spread = 0.60
	advisor_pool = 1
	stability_cost_modifier = 0.15
	global_autonomy = 0.1
	picture = "MAPMODE_education_level_7"
}

education_level_exceptional = {
	technology_cost = -0.2
	idea_cost = -0.1
	army_tradition_decay = -0.020
	navy_tradition_decay = -0.020
	global_institution_spread = 0.70
	advisor_pool = 2
	stability_cost_modifier = 0.20
	global_autonomy = 0.125
	picture = "MAPMODE_education_level_8"
}

education_level_enlightened = {
	technology_cost = -0.25
	idea_cost = -0.125
	army_tradition_decay = -0.025
	navy_tradition_decay = -0.025
	global_institution_spread = 0.80
	advisor_pool = 2
	stability_cost_modifier = 0.25
	global_autonomy = 0.15
	picture = "MAPMODE_education_level_11"
}

some_glass_per_capita = {
	technology_cost = -0.0125
	picture = "urban_goods_glassware"
}
ample_glass_per_capita = {
	technology_cost = -0.025
	picture = "urban_goods_glassware"
}
abundant_glass_per_capita = {
	technology_cost = -0.05
	global_prov_trade_power_modifier = 0.05
	picture = "urban_goods_glassware"
}
plentiful_glass_per_capita = {
	technology_cost = -0.075
	prestige_decay = -0.005
	global_prov_trade_power_modifier = 0.1
	picture = "urban_goods_glassware"
}
copious_glass_per_capita = {
	technology_cost = -0.1
	prestige_decay = -0.01
	global_prov_trade_power_modifier = 0.15
	picture = "urban_goods_glassware"
}

some_leather_per_capita = {
	production_efficiency = 0.05
	picture = "urban_goods_leather"
}
ample_leather_per_capita = {
	production_efficiency = 0.075
	global_regiment_cost = -0.025
	picture = "urban_goods_leather"
}
abundant_leather_per_capita = {
	production_efficiency = 0.1
	global_regiment_cost = -0.05
	picture = "urban_goods_leather"
}
plentiful_leather_per_capita = {
	production_efficiency = 0.15
	global_regiment_cost = -0.075
	picture = "urban_goods_leather"
}
copious_leather_per_capita = {
	production_efficiency = 0.25
	global_regiment_cost = -0.1
	picture = "urban_goods_leather"
}

some_silk_per_capita = {
	adm_tech_cost_modifier = -0.025
	picture = "urban_goods_silk"
}
ample_silk_per_capita = {
	adm_tech_cost_modifier = -0.05
	global_prov_trade_power_modifier = 0.05
	picture = "urban_goods_silk"
}
abundant_silk_per_capita = {
	picture = "urban_goods_silk"
	adm_tech_cost_modifier = -0.1
	global_prov_trade_power_modifier = 0.1
	advisor_cost = -0.1
}
plentiful_silk_per_capita = {
	adm_tech_cost_modifier = -0.15
	global_prov_trade_power_modifier = 0.15
	advisor_cost = -0.15
	picture = "urban_goods_silk"
}
copious_silk_per_capita = {
	adm_tech_cost_modifier = -0.2
	global_prov_trade_power_modifier = 0.2
	advisor_cost = -0.2
	picture = "urban_goods_silk"
}

some_steel_per_capita = {
	mil_tech_cost_modifier = -0.025
	picture = "urban_goods_steel"
}
ample_steel_per_capita = {
	mil_tech_cost_modifier = -0.05
	cavalry_power = 0.025
	artillery_power = 0.025
	infantry_power = 0.025
	picture = "urban_goods_steel"
}
abundant_steel_per_capita = {
	mil_tech_cost_modifier = -0.1
	cavalry_power = 0.05
	artillery_power = 0.05
	infantry_power = 0.05
	picture = "urban_goods_steel"
}
plentiful_steel_per_capita = {
	mil_tech_cost_modifier = -0.15
	cavalry_power = 0.1
	artillery_power = 0.1
	infantry_power = 0.1
	picture = "urban_goods_steel"
}
copious_steel_per_capita = {
	mil_tech_cost_modifier = -0.2
	cavalry_power = 0.15
	artillery_power = 0.15
	infantry_power = 0.15
	picture = "urban_goods_steel"
}

some_naval_supplies_per_capita = {
	naval_forcelimit_modifier = 0.01
	ship_durability = 0.01
	picture = "urban_goods_naval_supplies"
}
ample_naval_supplies_per_capita = {
	naval_forcelimit_modifier = 0.025
	ship_durability = 0.02
	#range = 0.1
	picture = "urban_goods_naval_supplies"
}
abundant_naval_supplies_per_capita = {
	naval_forcelimit_modifier = 0.05
	ship_durability = 0.03
	#range = 0.15
	#colonists = 1
	picture = "urban_goods_naval_supplies"
}
plentiful_naval_supplies_per_capita = {
	naval_forcelimit_modifier = 0.075
	ship_durability = 0.04
	navy_tradition_decay = -0.02
	#range = 0.2
	#colonists = 1
	picture = "urban_goods_naval_supplies"
}
copious_naval_supplies_per_capita = {
	naval_forcelimit_modifier = 0.10
	ship_durability = 0.05
	navy_tradition_decay = -0.04
	#range = 0.25
	#colonists = 2
	picture = "urban_goods_naval_supplies"
}

some_paper_per_capita = {
	idea_cost = -0.05
	picture = "urban_goods_paper"
}
ample_paper_per_capita = {
	idea_cost = -0.075
	advisor_cost = -0.05
	picture = "urban_goods_paper"
}
abundant_paper_per_capita = {
	idea_cost = -0.1
	advisor_cost = -0.1
	advisor_pool = 1
	picture = "urban_goods_paper"
}
plentiful_paper_per_capita = {
	idea_cost = -0.15
	advisor_cost = -0.2
	advisor_pool = 1
	picture = "urban_goods_paper"
}
copious_paper_per_capita = {
	idea_cost = -0.2
	advisor_cost = -0.3
	advisor_pool = 2
	picture = "urban_goods_paper"
}

some_carpet_per_capita = {
	prestige_decay = -0.0025
	global_tax_modifier = 0.025
	picture = "urban_goods_carpet"
}
ample_carpet_per_capita = {
	prestige_decay = -0.005
	global_tax_modifier = 0.05
	picture = "urban_goods_carpet"
}
abundant_carpet_per_capita = {
	prestige_decay = -0.01
	global_tax_modifier = 0.10
	picture = "urban_goods_carpet"
}
plentiful_carpet_per_capita = {
	prestige_decay = -0.0125
	global_tax_modifier = 0.15
	picture = "urban_goods_carpet"
}
copious_carpet_per_capita = {
	prestige_decay = -0.015
	global_tax_modifier = 0.25
	picture = "urban_goods_carpet"
}

some_chinaware_per_capita = {
	diplomatic_reputation = 1
	picture = "urban_goods_chinaware"
}
ample_chinaware_per_capita = {
	diplomatic_reputation = 2
	diplomats = 1
	picture = "urban_goods_chinaware"
}
abundant_chinaware_per_capita = {
	diplomatic_reputation = 3
	diplomatic_upkeep = 1
	diplomats = 1
	picture = "urban_goods_chinaware"
}
plentiful_chinaware_per_capita = {
	diplomatic_reputation = 4
	diplomatic_upkeep = 1
	diplomats = 1
	picture = "urban_goods_chinaware"
}
copious_chinaware_per_capita = {
	diplomatic_reputation = 5
	diplomatic_upkeep = 2
	diplomats = 2
	picture = "urban_goods_chinaware"
}

some_jewelry_per_capita = {
	global_tax_modifier = 0.05
	picture = "urban_goods_jewelry"
}
ample_jewelry_per_capita = {
	global_tax_modifier = 0.1
	legitimacy = 0.15
	republican_tradition = 0.15
	picture = "urban_goods_jewelry"
}
abundant_jewelry_per_capita = {
	global_tax_modifier = 0.15
	inflation_reduction = 0.05
	legitimacy = 0.3
	republican_tradition = 0.30
	picture = "urban_goods_jewelry"
}
plentiful_jewelry_per_capita = {
	global_tax_modifier = 0.2
	inflation_reduction = 0.075
	legitimacy = 0.5
	republican_tradition = 0.50
	picture = "urban_goods_jewelry"
}
copious_jewelry_per_capita = {
	global_tax_modifier = 0.3
	inflation_reduction = 0.1
	legitimacy = 0.75
	republican_tradition = 0.75
	picture = "urban_goods_jewelry"
}

some_luxury_cloth_per_capita = {
	dip_tech_cost_modifier = -0.025
	picture = "urban_goods_luxury_cloth"
}
ample_luxury_cloth_per_capita = {
	dip_tech_cost_modifier = -0.05
	global_prov_trade_power_modifier = 0.05
	picture = "urban_goods_luxury_cloth"
}
abundant_luxury_cloth_per_capita = {
	dip_tech_cost_modifier = -0.1
	global_prov_trade_power_modifier = 0.1
	diplomatic_upkeep = 1
	picture = "urban_goods_luxury_cloth"
}
plentiful_luxury_cloth_per_capita = {
	dip_tech_cost_modifier = -0.15
	global_prov_trade_power_modifier = 0.15
	diplomatic_upkeep = 1
	picture = "urban_goods_luxury_cloth"
}
copious_luxury_cloth_per_capita = {
	dip_tech_cost_modifier = -0.2
	global_prov_trade_power_modifier = 0.2
	diplomatic_upkeep = 2
	picture = "urban_goods_luxury_cloth"
}

timber_deficit_very_large = {
	global_ship_cost = 0.40
	global_ship_recruit_speed = 0.50
	global_ship_repair = -0.50
	ship_durability = -0.1
	picture = "lumber_bad"
}
timber_deficit_large = {
	global_ship_cost = 0.30
	global_ship_recruit_speed = 0.40
	global_ship_repair = -0.40
	ship_durability = -0.075
	picture = "lumber_bad"
}
timber_deficit_medium = {
	global_ship_cost = 0.20
	global_ship_recruit_speed = 0.25
	global_ship_repair = -0.25
	ship_durability = -0.05
	picture = "lumber_bad"
}
timber_deficit_small = {
	global_ship_cost = 0.10
	global_ship_recruit_speed = 0.10
	global_ship_repair = -0.10
	ship_durability = -0.025
	picture = "lumber_bad"
}
timber_stock_small = {
	naval_maintenance_modifier = -0.05
	global_ship_recruit_speed = -0.05
	ship_durability = 0.025
	picture = "lumber_good"
}
timber_stock_medium = {
	global_ship_cost = -0.10
	naval_maintenance_modifier = -0.10
	global_ship_recruit_speed = -0.10
	ship_durability = 0.05
	picture = "lumber_good"
}
timber_buying_1 = {
	global_tax_income = -1
	naval_maintenance_modifier = 0.025
	picture = "lumber_buyer_1"
}
timber_buying_2 = {
	global_tax_income = -2.5
	naval_maintenance_modifier = 0.05
	picture = "lumber_buyer_2"
}
timber_buying_3 = {
	global_tax_income = -4
	naval_maintenance_modifier = 0.075
	picture = "lumber_buyer_3"
}
timber_buying_4 = {
	global_tax_income = -6
	naval_maintenance_modifier = 0.1
	merchants = -1
	picture = "lumber_buyer_4"
}
timber_buying_5 = {
	global_tax_income = -9
	naval_maintenance_modifier = 0.125
	merchants = -1
	picture = "lumber_buyer_5"
}
timber_buying_6 = {
	global_tax_income = -12
	naval_maintenance_modifier = 0.15
	merchants = -1
	picture = "lumber_buyer_5"
}
timber_buying_7 = {
	global_tax_income = -16
	naval_maintenance_modifier = 0.20
	merchants = -1
	picture = "lumber_buyer_5"
}
timber_buying_8 = {
	global_tax_income = -20
	naval_maintenance_modifier = 0.25
	merchants = -1
	picture = "lumber_buyer_5"
}
timber_seller_1 = {
	global_tax_income = 1
	global_ship_trade_power = 0.010
	picture = "lumber_seller_1"
}
timber_seller_2 = {
	global_tax_income = 2.5
	global_ship_trade_power = 0.025
	picture = "lumber_seller_2"
}
timber_seller_3 = {
	global_tax_income = 4
	global_ship_trade_power = 0.050
	picture = "lumber_seller_3"
}
timber_seller_4 = {
	global_tax_income = 6
	global_ship_trade_power = 0.075
	picture = "lumber_seller_4"
}
timber_seller_5 = {
	global_tax_income = 9
	global_ship_trade_power = 0.10
	picture = "lumber_seller_4"
}
timber_seller_6 = {
	global_tax_income = 12
	global_ship_trade_power = 0.125
	picture = "lumber_seller_4"
}
timber_seller_7 = {
	global_tax_income = 16
	global_ship_trade_power = 0.15
	picture = "lumber_seller_4"
}
timber_seller_8 = {
	global_tax_income = 20
	global_ship_trade_power = 0.20
	picture = "lumber_seller_4"
}
state_forest = {
	trade_goods_size_modifier = -0.20
	global_tax_income = -3
	picture = "state_forest"
}
montello_state_forest = {
	trade_goods_size_modifier = -0.15
	global_tax_income = -1.5
	picture = "state_forest"
}
pinhal_de_leiria = {
	trade_goods_size_modifier = -0.15
	global_tax_income = -1.5
	picture = "state_forest"
}
foret_de_troncais = {
	trade_goods_size_modifier = -0.15
	global_tax_income = -3
	picture = "state_forest"
}
rhein_timber = {
	global_tax_income = -5
	picture = "state_forest"
}
deforestation = {
	trade_goods_size_modifier = -0.25
	picture = "state_forest"
}
forestry_act = {
	global_tax_income = -3
	global_tax_modifier = -0.05
	global_trade_goods_size_modifier = -0.025
	global_unrest = 0.5
	picture = "state_forest"
}
forestry_act_big = {
	global_tax_income = -8
	global_tax_modifier = -0.075
	global_trade_goods_size_modifier = -0.05
	global_unrest = 1
	picture = "state_forest"
}
forestry_code = {
	global_tax_income = -15
	global_tax_modifier = -0.10
	global_trade_goods_size_modifier = -0.10
	global_unrest = 1.5
	picture = "state_forest"
}

AI_policy_decisions_checked = {
	
}

AI_centralization_policy = {
	yearly_absolutism = 0.4
}

stability_policy_admin_1 = {
	stability_cost_modifier = -0.25
	global_tax_modifier = -0.05
	production_efficiency = -0.10
}

stability_policy_admin_2 = {
	stability_cost_modifier = -0.2
	global_tax_modifier = -0.05
	production_efficiency = -0.10
}

stability_policy_admin_3 = {
	stability_cost_modifier = -0.15
	global_tax_modifier = -0.05
	production_efficiency = -0.10
}

stability_policy_diplo_1 = {
	stability_cost_modifier = -0.25
	diplomatic_reputation = -2
#	diplomats = -1
	improve_relation_modifier = -0.15
}

stability_policy_diplo_2 = {
	stability_cost_modifier = -0.20
	diplomatic_reputation = -2
#	diplomats = -1
	improve_relation_modifier = -0.15
}

stability_policy_diplo_3 = {
	stability_cost_modifier = -0.15
	diplomatic_reputation = -2
#	diplomats = -1
	improve_relation_modifier = -0.15
}

stability_policy_milit_1 = {
	stability_cost_modifier = -0.25
	global_manpower_modifier = -0.10
	global_tax_modifier = -0.025
	production_efficiency = -0.05
}

stability_policy_milit_2 = {
	stability_cost_modifier = -0.20
	global_manpower_modifier = -0.10
	global_tax_modifier = -0.025
	production_efficiency = -0.05
}

stability_policy_milit_3 = {
	stability_cost_modifier = -0.15
	global_manpower_modifier = -0.10
	global_tax_modifier = -0.025
	production_efficiency = -0.05
}

road_maintenance_5 = {
	state_maintenance_modifier = 0.05
}

road_maintenance_10 = {
	state_maintenance_modifier = 0.10
}

road_maintenance_20 = {
	state_maintenance_modifier = 0.20
}

road_maintenance_30 = {
	state_maintenance_modifier = 0.30
}

road_maintenance_40 = {
	state_maintenance_modifier = 0.40
}

road_maintenance_50 = {
	state_maintenance_modifier = 0.50
}

road_maintenance_75 = {
	state_maintenance_modifier = 0.75
}

road_maintenance_100 = {
	state_maintenance_modifier = 1
}

eventbuild_pause = {
	
}
weightbuild_pause = {
	
}

capital_maintenance_1 = { global_tax_income = 5 }
capital_maintenance_2 = { global_tax_income = 10 }
capital_maintenance_3 = { global_tax_income = 15 }
capital_maintenance_4 = { global_tax_income = 20 }
capital_maintenance_5 = { global_tax_income = 25 }
capital_maintenance_6 = { global_tax_income = 30 }
capital_maintenance_7 = { global_tax_income = 35 }
capital_maintenance_8 = { global_tax_income = 40 }
capital_maintenance_9 = { global_tax_income = 45 }
capital_maintenance_10 = { global_tax_income = 50 }
capital_maintenance_11 = { global_tax_income = 55 }
capital_maintenance_12 = { global_tax_income = 60 }
capital_maintenance_13 = { global_tax_income = 65 }
capital_maintenance_14 = { global_tax_income = 70 }
capital_maintenance_15 = { global_tax_income = 75 }
capital_maintenance_16 = { global_tax_income = 80 }
capital_maintenance_17 = { global_tax_income = 85 }
capital_maintenance_18 = { global_tax_income = 90 }
capital_maintenance_19 = { global_tax_income = 95 }
capital_maintenance_20 = { global_tax_income = 100 }
capital_maintenance_21 = { global_tax_income = 120 }
capital_maintenance_22 = { global_tax_income = 140 }
capital_maintenance_23 = { global_tax_income = 160 }
capital_maintenance_24 = { global_tax_income = 180 }
capital_maintenance_25 = { global_tax_income = 200 }
capital_maintenance_26 = { global_tax_income = 220 }
capital_maintenance_27 = { global_tax_income = 240 }
capital_maintenance_28 = { global_tax_income = 260 }
capital_maintenance_29 = { global_tax_income = 280 }
capital_maintenance_30 = { global_tax_income = 300 }
capital_maintenance_31 = { global_tax_income = 320 }
capital_maintenance_32 = { global_tax_income = 340 }
capital_maintenance_33 = { global_tax_income = 360 }
capital_maintenance_34 = { global_tax_income = 380 }
capital_maintenance_35 = { global_tax_income = 400 }
capital_maintenance_36 = { global_tax_income = 420 }
capital_maintenance_37 = { global_tax_income = 440 }
capital_maintenance_38 = { global_tax_income = 460 }
capital_maintenance_39 = { global_tax_income = 480 }
capital_maintenance_40 = { global_tax_income = 500 }
capital_maintenance_41 = { global_tax_income = 600 }
capital_maintenance_42 = { global_tax_income = 700 }
capital_maintenance_43 = { global_tax_income = 800 }
capital_maintenance_44 = { global_tax_income = 900 }
capital_maintenance_45 = { global_tax_income = 1000 }
capital_maintenance_46 = { global_tax_income = 1100 }
capital_maintenance_47 = { global_tax_income = 1200 }
capital_maintenance_48 = { global_tax_income = 1300 }
capital_maintenance_49 = { global_tax_income = 1400 }
capital_maintenance_50 = { global_tax_income = 1500 }
capital_maintenance_51 = { global_tax_income = 1600 }
capital_maintenance_52 = { global_tax_income = 1700 }
capital_maintenance_53 = { global_tax_income = 1800 }
capital_maintenance_54 = { global_tax_income = 1900 }
capital_maintenance_55 = { global_tax_income = 2000 }
capital_maintenance_56 = { global_tax_income = 2100 }
capital_maintenance_57 = { global_tax_income = 2200 }
capital_maintenance_58 = { global_tax_income = 2300 }
capital_maintenance_59 = { global_tax_income = 2400 }
capital_maintenance_60 = { global_tax_income = 2500 }
capital_maintenance_61 = { global_tax_income = 2500 }

capital_num_1 = { max_states = -1 }
capital_num_2 = { max_states = -2 }
capital_num_3 = { max_states = -3 }
capital_num_4 = { max_states = -4 }
capital_num_5 = { max_states = -5 }
capital_num_6 = { max_states = -6 }
capital_num_7 = { max_states = -7 }
capital_num_8 = { max_states = -8 }
capital_num_9 = { max_states = -9 }
capital_num_10 = { max_states = -10 }
capital_num_11 = { max_states = -11 }
capital_num_12 = { max_states = -12 }
capital_num_13 = { max_states = -13 }
capital_num_14 = { max_states = -14 }
capital_num_15 = { max_states = -15 }
capital_num_16 = { max_states = -16 }
capital_num_17 = { max_states = -17 }
capital_num_18 = { max_states = -18 }
capital_num_19 = { max_states = -19 }
capital_num_20 = { max_states = -20 }

stability_murder = {
	stability_cost_modifier = 0.50
}

big_country_ai_starting_constraint = { ### This prevents the AI from violently overbuilding at game start as a big country
	land_forcelimit_modifier = -0.5
	naval_forcelimit_modifier = -0.5
}