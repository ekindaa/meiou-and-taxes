namespace = hre_universities

######################################################
### HRE / European universities events, by Dezuman ###
######################################################


# TODO: For some time period from game start HRE princes (and other catholic nations) without a university in their region may ask pope to charter one. 
# This may be built in a nearby province if asker has no good city (a la Count Palatinate founding Koln university)
# Additionally during western Schism the Roman Pope should issue bulls to found German universities to educate students that can't go to Avignionist Paris university
# Historic examples: Koln, several in scotland apparently, probably all the ones already handled by static events (krakow, vienna, pecs), 
# Erfurt (Actually by Avignon pope because history is never clean and simple, but it seems to be the only one), Heidelberg. 

# TODO: If Prague university is taken by Hussites a new university is funded close by (Saxony?) by the fleeing germans. Tie into hussite event chain?
# Historic examples: Leipzig





###############################
### Historical universities ###
###############################

# These were founded only years after Kraków Academy, which is already added by a polish flavor event


# University of Vienna
country_event = {
	id = hre_universities.001
	title = "hre_universities.001.t"
	desc = "hre_universities.001.d"
	picture = UNIVERSITY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = HAB
		owns = 83 # Ostmark
		83 = { NOT = { has_building = small_university } }
		is_year = 1361 # Historic date is 1365, but we randomize a bit
		NOT = { is_year = 1420 }
	}
	
	mean_time_to_happen = { years = 4 }
	
	option = {
		name = "hre_universities.001.EVTOPTA" # Sponsor the university
		ai_chance = { factor = 100 }
		if = {
			limit = {
				ai = yes
			}
			add_years_of_income = -0.10
			add_prestige = 5
			83 = { add_building = small_university }
		}
		if = {
			limit = {
				ai = no
			}
			add_years_of_income = -0.25
			add_prestige = 5
			83 = { add_building = small_university }
		}
	}
	option = {
		name = "hre_universities.001.EVTOPTB" # Do nothing
		ai_chance = { factor = 0 }
		add_prestige = -5
	}
}

# University of Pecs
country_event = {
	id = hre_universities.002
	title = "hre_universities.002.t"
	desc = "hre_universities.002.d"
	picture = UNIVERSITY_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = HUN
		owns = 153 # Pecs
		153 = { NOT = { has_building = small_university } }
		is_year = 1363 # Historic date is 1367, but we randomize a bit
		NOT = { is_year = 1420 }
	}
	
	mean_time_to_happen = { years = 4 }
	
	option = {
		name = "hre_universities.002.EVTOPTA" # Sponsor the university
		ai_chance = { factor = 100 }
		if = {
			limit = {
				ai = yes
			}
			add_years_of_income = -0.10
			add_prestige = 5
			153 = { add_building = small_university }
		}
		if = {
			limit = {
				ai = no
			}
			add_years_of_income = -0.25
			add_prestige = 5
			153 = { add_building = small_university }
		}
	}
	option = {
		name = "hre_universities.002.EVTOPTB" # Do nothing
		ai_chance = { factor = 0 }
		add_prestige = -5
	}
}
