# Renaissance Europe


# Start renaissance in countries that should have it
country_event = {
	id = hee_renaissance.1
	title = hee_renaissance.1.t
	desc = hee_renaissance.1.d
	picture = IMPORTANT_STATUE_eventPicture
	
	trigger = {
		culture_group = latin
		NOT = { has_country_modifier = hee_renaissance }
		NOT = { is_year = 1610 }
		NOT = {
			has_country_modifier = hee_had_renaissance
		}
	}
	
	mean_time_to_happen = { months = 1 }
	
	option = {
		name = hee_renaissance.1.a
		add_country_modifier = {
			name = hee_renaissance
			duration = -1
			hidden = yes
		}
		custom_tooltip = hee_renaissance.1.tt
	}
}

#Renaissance spreads
country_event = {
	id = hee_renaissance.2
	title = hee_renaissance.2.t
	desc = hee_renaissance.2.d
	picture = IMPORTANT_STATUE_eventPicture
	
	trigger = {
		NOT = { is_year = 1590 }
		NOT = { has_country_modifier = hee_renaissance }
		OR = {
			technology_group = western
			technology_group = eastern
		}
		NOT = { has_country_modifier = hee_had_renaissance }
	}
	
	mean_time_to_happen = {
		years = 150
		modifier = {
			factor = 0.2
			any_neighbor_country = { has_country_modifier = hee_renaissance }
		}
		modifier = {
			factor = 0.75
			ADM = 4
		}
		modifier = {
			factor = 0.8
			OR = {
				primary_culture = dutch
				primary_culture = hungarian
				primary_culture = flemish
				primary_culture = brabantian
			}
		}
	}
	
	option = {
		name = hee_renaissance.2.a
		add_country_modifier = {
			name = hee_renaissance
			duration = -1
			hidden = yes
		}
		custom_tooltip = hee_renaissance.1.tt
	}
}

# End of renaissance influences
country_event = {
	id = hee_renaissance.3
	title = hee_renaissance.3.t
	desc = hee_renaissance.3.d
	picture = BIG_BOOK_eventPicture
	
	trigger = {
		has_country_modifier = hee_renaissance
		is_year = 1580
	}
	
	mean_time_to_happen = {
		years = 30
		modifier = {
			factor = 0.5
			NOT = {
				any_neighbor_country = {
					has_country_modifier = hee_renaissance
				}
			}
		}
		modifier = {
			factor = 0.2
			is_year = 1610
		}
	}
	
	immediate = {
		hidden_effect = {
			add_country_modifier = {
				name = hee_had_renaissance
				duration = -1
				hidden = yes
			}
		}
	}
	
	option = {
		name = hee_renaissance.3.a
		remove_country_modifier = hee_renaissance
	}
}

# Renaissance Building Boom
province_event = {
	id = hee_renaissance.4
	title = hee_renaissance.4.t
	desc = hee_renaissance.4.d
	picture = GREAT_BUILDING_eventPicture
	
	trigger = {
		owner = { has_country_modifier = hee_renaissance }
		base_production = 2
		owner = { is_at_war = no }
		NOT = {
			owner = {
				has_country_modifier = hee_renaissance_architecture_timer
			}
		}
	}
	
	mean_time_to_happen = { years = 55 }
	
	option = {
		name = hee_renaissance.4.a
		ROOT = {
			add_province_modifier = {
				name = hee_renaissance_architecture
				duration = 3650
			}
		}
		owner = {
			add_country_modifier = {
				name = hee_renaissance_architecture_timer
				duration = 3650
				hidden = yes
			}
		}
	}
}

# The Classics
country_event = {
	id = hee_renaissance.5
	title = hee_renaissance.5.t
	desc = hee_renaissance.5.d
	picture = GREAT_BUILDING_eventPicture
	
	trigger = {
		has_country_modifier = hee_renaissance
	}
	
	mean_time_to_happen = { years = 50 }
	
	option = {
		name = hee_renaissance.5.a
		add_adm_power = 100
		random = {
			chance = 40
			subtract_stability_1 = yes
		}
	}
}

# Renaissance writer
country_event = {
	id = hee_renaissance.8
	title = hee_renaissance.8.t
	desc = hee_renaissance.8.d
	picture = ECONOMY_eventPicture
	
	trigger = {
		has_country_modifier = hee_renaissance
	}
	
	mean_time_to_happen = { years = 33 }
	
	option = {
		name = hee_renaissance.8.a
		add_prestige = 10
	}
}

# Renaissance polymath
country_event = {
	id = hee_renaissance.9
	title = hee_renaissance.9.t
	desc = hee_renaissance.9.d
	picture = ADVISOR_eventPicture
	
	trigger = {
		has_country_modifier = hee_renaissance
	}
	
	mean_time_to_happen = { years = 150 }
	
	option = {
		name = hee_renaissance.9.a
		add_treasury = -30
		add_country_modifier = {
			name = hee_polymath_adm
			duration = 1825
		}
	}
	option = {
		name = hee_renaissance.9.b
		add_treasury = -30
		add_country_modifier = {
			name = hee_polymath_dip
			duration = 1825
		}
	}
	option = {
		name = hee_renaissance.9.c
		add_treasury = -30
		add_country_modifier = {
			name = hee_polymath_mil
			duration = 1825
		}
	}
	option = {
		name = hee_renaissance.9.e
		add_treasury = -30
		add_country_modifier = {
			name = hee_polymath_ideas
			duration = 1825
		}
	}
	option = {
		name = hee_renaissance.9.f
		add_treasury = 50
	}
}

# Renaissance writer produces theological treatise
country_event = {
	id = hee_renaissance.10
	title = hee_renaissance.10.t
	desc = hee_renaissance.10.d
	picture = RELIGION_eventPicture
	
	trigger = {
		has_country_modifier = hee_renaissance
		religion = catholic
		NOT = {
			tag = PAP
		}
		ai = no
	}
	
	mean_time_to_happen = {
		years = 50
		modifier = {
			factor = 0.75
			has_idea_group = theology_ideas
		}
	}
	
	option = {
		name = hee_renaissance.10.a
		add_papal_influence = 15
	}
	option = {
		name = hee_renaissance.10.b
		add_papal_influence = -10
		add_prestige = 5
	}
}

# Claim discovered to be false!
country_event = {
	id = hee_renaissance.11
	title = hee_renaissance.11.t
	desc = hee_renaissance.11.d
	picture = RELIGION_eventPicture
	
	trigger = {
		has_country_modifier = hee_renaissance
		NOT = { has_country_modifier = hee_falsified_timer }
		any_province = {
			is_claim = ROOT
			NOT = { owned_by = ROOT }
		}
	}
	
	mean_time_to_happen = { years = 108 }
	
	immediate = {
		add_country_modifier = {
			name = hee_falsified_timer
			duration = 18250
			hidden = yes
		}
	}
	
	option = {
		name = hee_renaissance.11.a
		add_treasury = -30
	}
	option = {
		name = hee_renaissance.11.b
		ai_chance = {
			factor = 0
		}
		random_province = {
			limit = {
				is_claim = ROOT
				NOT = { owned_by = ROOT }
			}
			remove_claim = ROOT
		}
	}
	option = {
		name = hee_renaissance.11.c
		add_country_modifier = {
			name = hee_ignore_falsification	#Extra AE
			duration = 3650
		}
		# random_province = {
		# limit = {
		# is_neighbor_of = ROOT
		# NOT = { is_claim = ROOT }
		# }
		# add_claim = ROOT
		# }
		# random_province = {
		# limit = {
		# NOT = { is_claim = ROOT }
		# is_neighbor_of = ROOT
		# }
		# add_claim = ROOT
		# }
	}
}


# Papal Legate Concedes to Humanist
country_event = {
	id = hee_renaissance.12
	title = hee_renaissance.12.t
	desc = hee_renaissance.12.d
	picture = RELIGION_eventPicture
	
	trigger = {
		ai = no
		has_country_modifier = hee_renaissance
		NOT = {
			is_religion_enabled = protestant
		}
		religion = catholic
	}
	
	mean_time_to_happen = {
		years = 83
		modifier = {
			factor = 0.8
			has_idea_group = humanist_ideas
		}
		modifier = {
			factor = 0.8
			reverse_has_opinion = {
				who = PAP
				value = 100
			}
		}
	}
	
	option = {
		name = hee_renaissance.12.a
		add_prestige = 10
		#		add_reform_desire = -0.01
	}
	
	option = {
		name = hee_renaissance.12.b
		define_advisor = {
			type = philosopher
			skill = 2
			discount = yes
		}
	}
}

#Construction Running Over Budget
country_event = {
	id = hee_renaissance.13
	title = hee_renaissance.13.t
	desc = hee_renaissance.13.d
	picture = OVEREXTENSION_eventPicture
	
	trigger = {
		has_country_modifier = hee_renaissance
		treasury = 10
		NOT = {
			num_of_loans = 4
		}
	}
	
	mean_time_to_happen = { years = 100 }
	
	option = {
		name = hee_renaissance.13.a #Increase the budget
		add_inflation = 0.5
		add_treasury = -20
	}
	
	option = {
		name = hee_renaissance.13.b #Reorganise the project
		add_adm_power = -20
	}
}
