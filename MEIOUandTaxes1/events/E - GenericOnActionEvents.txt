
# Integrating FROMCOUNTRY
country_event = {
	id = generic_onaction.610
	title = "EVTNAME610"
	desc = "EVTDESC610"
	picture = DEBATE_REPUBLICAN_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "EVTOPTB610A"
		add_country_modifier = {
			name = "annexers_legacy"
			duration = 3650
		}
		if = {
			limit = {
				any_country = {
					is_subject_of = ROOT
					has_country_flag = subject_s_colonial_nation
				}
			}
			every_subject_country = {
				limit = {
					has_country_flag = subject_s_colonial_nation
				}
				clr_country_flag = subject_s_colonial_nation
				add_liberty_desire = 10
				if = {
					limit = {
						NOT = { religion_group = ROOT }
					}
					add_liberty_desire = 5
				}
				if = {
					limit = {
						NOT = { religion = ROOT }
					}
					add_liberty_desire = 5
				}
				if = {
					limit = {
						NOT = { culture_group = ROOT }
					}
					add_liberty_desire = 5
				}
				if = {
					limit = {
						NOT = { primary_culture = ROOT }
					}
					add_liberty_desire = 5
				}
				if = {
					limit = {
						NOT = { has_opinion = { who = ROOT value = 100 } }
					}
					add_liberty_desire = 1
				}
				if = {
					limit = {
						NOT = { has_opinion = { who = ROOT value = 50 } }
					}
					add_liberty_desire = 1
				}
				if = {
					limit = {
						NOT = { has_opinion = { who = ROOT value = 0 } }
					}
					add_liberty_desire = 1
				}
				if = {
					limit = {
						NOT = { has_opinion = { who = ROOT value = -50 } }
					}
					add_liberty_desire = 1
				}
				if = {
					limit = {
						NOT = { has_opinion = { who = ROOT value = -100 } }
					}
					add_liberty_desire = 1
				}
				if = {
					limit = {
						NOT = { has_opinion = { who = ROOT value = -150 } }
					}
					add_liberty_desire = 1
				}
				if = {
					limit = {
						is_colonial_nation_of = ROOT
					}
					add_liberty_desire = 10
				}
			}
		}
	}
}

# Chance of being triggered when an heir with a weak claim succeeds to the throne
country_event = {
	id = generic_onaction.611
	title = "EVTNAME611"
	desc = "EVTDESC611"
	picture = MILITARY_CAMP_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		num_of_cities = 3
		government = monarchy
	}
	
	option = {
		name = "EVTOPTB611A"
		random_owned_province = {
			limit = {
				OR = {
					is_core = ROOT
					is_capital = yes
				}
			}
			add_province_modifier = {
				name = "pretender_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
	}
}
