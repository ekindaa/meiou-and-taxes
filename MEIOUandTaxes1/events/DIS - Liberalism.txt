# Aspiration for liberty
country_event = {
	id = aspiration_for_liberty.1
	title = "EVTNAME1061"
	desc = "EVTDESC1061"
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes
	#	mean_time_to_happen = {
	#		days = 1
	#	}
	
	
	immediate = {
		set_country_flag = had_liberalism
		subtract_stability_6 = yes
		capital_scope = {
			add_province_modifier = {
				name = "revolutionaries_organizing"
				duration = 3650
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 10 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_10
		}
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = revolutionaries_organizing }
			}
			add_province_modifier = {
				name = "revolutionaries_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
		random_owned_province = {
			limit = {
				NOT = { has_province_modifier = revolutionaries_organizing }
			}
			add_province_modifier = {
				name = "revolutionaries_organizing"
				duration = 1825
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 5 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_5
		}
	}
	
	option = {
		name = "EVTOPTA1061"
		add_prestige = -5
	}
}

# Governmental incompetence
country_event = {
	id = aspiration_for_liberty.2
	title = "EVTNAME1062"
	desc = "EVTDESC1062"
	picture = ANGRY_MOB_eventPicture
	
	
	is_triggered_only = yes
	#	mean_time_to_happen = {
	#		days = 1
	#	}
	
	option = {
		name = "EVTOPTA1062" # There is nothing to be concerned about.
		ai_chance = { factor = 50 }
		every_owned_province = {
			limit = {
				likely_rebels = revolutionary_rebels
				is_overseas = no
			}
			hidden_effect = {
				set_variable = { which = added_unrest value = 1 }
				add_base_unrest = yes
			}
			custom_tooltip = added_unrest_1
		}
	}
	option = {
		name = "EVTOPTB1062" # Prevent them from advancing further.
		ai_chance = { factor = 10 }
		add_years_of_income = -1.0
		
		every_owned_province = {
			limit = {
				is_capital = no
				likely_rebels = revolutionary_rebels
				is_overseas = no
			}
			add_local_autonomy = 15
		}
	}
}

# Battles on foreign ground
country_event = {
	id = aspiration_for_liberty.3
	title = "EVTNAME1063"
	desc = "EVTDESC1063"
	picture = BATTLE_eventPicture
	
	trigger = {
		is_at_war = yes
		num_of_allies = 1
		OR = {
			num_of_loans = 5
			war_exhaustion = 5
		}
		
		NOT = {  has_country_modifier = "decreased_morale" }
		NOT = {  has_country_modifier = "reduceed_war_expenditures" }
	}
	
	is_triggered_only = yes
	#	mean_time_to_happen = {
	#		days = 1
	#	}
	
	option = {
		name = "EVTOPTA1063" # The treasury isn't even close to empty.
		random_ally = { add_opinion = { who = ROOT modifier = opinion_munificent_ally } }
		add_country_modifier = {
			name = "decreased_morale"
			duration = -1
			desc = "END_OF_LIBERALISM"
		}
	}
	option = {
		name = "EVTOPTB1063" # Cut down on war expenditures.	
		add_country_modifier = {
			name = reduceed_war_expenditures
			duration = -1
			desc = "END_OF_LIBERALISM"
		}
		random_ally = {
			add_opinion = { who = ROOT modifier = opinion_reduced_war_expenditures }
		}
	}
}

# Order is restored
country_event = {
	id = aspiration_for_liberty.4
	title = "EVTNAME1064"
	desc = "EVTDESC1064"
	picture = CITY_VIEW_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "EVTOPTA1064"
		add_stability_1 = yes
		clr_country_flag = liberalism
		set_country_flag = liberal_constitution
		remove_country_modifier = reduceed_war_expenditures
		remove_country_modifier = decreased_morale
	}
}